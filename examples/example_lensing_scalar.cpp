/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./examples/example_lensing_scalar.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <powspec.h>
#include <fitshandle.h>
#include <xcomplex.h>
#include <healpix_map.h>
#include <alm.h>

#include "lensing_operator_flints.hpp"

using namespace CMB;

int main()
{
  PowSpec cmbspec;
  Healpix_Map<double> input_CMB, lensed_CMB;
  Alm<xcomplex<double> > lensing_potential_alms;
  LensingOperatorFlints_Scalar<double,double> *lo;

  /* initialization of input_CMB, lensing_potential_alms and cmbspec. */
  /* ... */

  /* Initialization of the lensing operator */
  lo = new LensingOperatorFlints_Scalar<double,double>(input_CMB.Nside());
  /* We need the CMB spectrum */
  lo->update_CMB_spectrum(cmbspec.tt());
  /* We specify the lensing potential */
  lo->setLensingField(lensing_potential);
  /* We compute the lensed map */
  lo->applyLensing<false>(input_CMB, lensed_CMB);

  /* Destruction */
  delete lo;

  return 0;
}
