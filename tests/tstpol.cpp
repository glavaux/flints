/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/tstpol.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <omp.h>
// STANDARD
#include <cstring>
#include <cassert>
#include <vector>
#include <fstream>
#include <cmath>
#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>
#include <time.h>

// HEALPIX
#include <vec3.h>
#include <planck_rng.h>
#include <xcomplex.h>
#include <arr.h>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <alm_healpix_tools.h>
#include <alm_map_tools.h>
#include <alm.h>
#include <powspec.h>
#include <powspec_fitsio.h>
#include <alm_powspec_tools.h>
#include <fitshandle.h>

#include "extra_map_tools.hpp"
#include "polinterpol.hpp"
#include "qbinterpol.hpp"
#include "blens.hpp"

using namespace std;
using namespace CMB;

typedef CMB::QuickPolInterpolateMap<float,double> polType;

typedef CMB::QuickInterpolateMap<float,double> tempType;

  template<typename T>
  Healpix_Map<T> operator/(const Healpix_Map<T>& m1, const Healpix_Map<T>& m2)
  {
    Healpix_Map<T> m(m1.Nside(), m1.Scheme(), SET_NSIDE);
    
    for (long i = 0; i < m.Npix(); i++)
      m[i] = m1[i]/m2[i];
    return m;
  }

int main()
{
  int lmax = 3000;
  int nside = 64;
  int seed;
  Alm<xcomplex<float> > almT(lmax,lmax), almG(lmax,lmax),
    almC(lmax,lmax), alm_phi(lmax, lmax);
  Healpix_Map<float>
    mapT(nside, RING, SET_NSIDE), mapQ(nside, RING, SET_NSIDE),
    mapU(nside, RING, SET_NSIDE), dtheta_map(nside, RING, SET_NSIDE),
    dphi_map(nside, RING, SET_NSIDE);
  //  Healpix_Map<float>
  //    mapT2(4*nside, RING, SET_NSIDE), mapQ2(4*nside, RING, SET_NSIDE),
  //    mapU2(4*nside, RING, SET_NSIDE);
  PowSpec powspec(1, lmax), powspec_phi(1, lmax);
  Healpix_Map<xcomplex<float> > polarisation(nside, RING, SET_NSIDE);
  double clock_start, clock_end;
  
  seed = time(0);
 
  planck_rng rng(seed);

  read_powspec_from_fits("spectrum.fits", powspec, 4, lmax);
  read_powspec_from_fits("spectrum_phi.fits", powspec_phi, 1, lmax); 

  polType p(powspec.gg(),
	    mapT.max_pixrad(), 2, nside, 1e-2);


 create_alm(powspec_phi, alm_phi, rng);
  create_alm_pol(powspec, almT, almG, almC, rng); 
 
  cout << "Generating base pol map" << endl;
  alm2map_pol(almT,almG,almC,mapT,mapQ,mapU);
  //  cout << "Generating high res pol map" << endl;
  //  alm2map_pol(almT,almG,almC,mapT2,mapQ2,mapU2);

  cout << "Generating deflection map at base resolution..." << endl;
  {
    Healpix_Map<float> phimap(nside, RING, SET_NSIDE);
    alm2map_der1(alm_phi, phimap, dtheta_map, dphi_map);
  }

  for (int i = 0; i < polarisation.Npix(); i++)
    polarisation[i] = xcomplex<float>(mapQ[i],mapU[i]);

  tempType pI(powspec.tt(),
	      1, mapT.Nside());
  
  Healpix_Map<float> errmap(nside, RING, SET_NSIDE);
  Healpix_Map<float>
    interpolatedQ(nside, RING, SET_NSIDE),
    interpolatedU(nside, RING, SET_NSIDE),
    interpolatedT(nside, RING, SET_NSIDE);

  cout << "Interpolating " << errmap.Npix() << " pixels..." << endl;
  clock_start = omp_get_wtime();
#pragma omp parallel for schedule(dynamic,1000000)
   for (long i = 0; i < mapT.Npix(); i++)
    {
      pointing ptg;
      int npix;
      xcomplex<float> P;
      float var;

      long j = i;
	
      computeMovedDirection(dtheta_map[i],
			    dphi_map[i],
			    polarisation.pix2ang(j), ptg);
      
      P = p.interpolate(polarisation, ptg, &npix, &errmap[i]);    
      interpolatedT[i] = pI.interpolate(mapT, ptg, 0, 0);
      interpolatedQ[i] = (P.real());
      interpolatedU[i] = (P.imag());      
    }
  clock_end = omp_get_wtime();
  cout << "Time for a map = " << (clock_end-clock_start) << endl;

  arr<double> weight(2*nside);
  PowSpec outspec_T(1,lmax), outspec_G(1,lmax), outspec_C(1,lmax);
  PowSpec inspec_T(1,lmax), inspec_G(1,lmax), inspec_C(1,lmax);
  Alm<xcomplex<float> > out_almT(lmax,lmax), out_almG(lmax, lmax),
    out_almC(lmax, lmax);

  weight.fill(1);
  map2alm_pol(
	      interpolatedT, interpolatedQ, interpolatedU,
	      out_almT, out_almG, out_almC, weight);
  extract_powspec(out_almT, outspec_T);
  extract_powspec(out_almG, outspec_G);
  extract_powspec(out_almC, outspec_C);
  extract_powspec(almT, inspec_T);
  extract_powspec(almG, inspec_G);
  extract_powspec(almC, inspec_C);
  
  const char *fname_list[] = {
    "cl_T_interpolated.fits",
    "cl_G_interpolated.fits",
    "cl_C_interpolated.fits",
    "cl_T_ref.fits",
    "cl_G_ref.fits",
    "cl_C_ref.fits"
  };
  PowSpec *specs[] = {
    &outspec_T, &outspec_G, &outspec_C,
    &inspec_T, &inspec_G, &inspec_C
  };
  for (int i = 0; i < 6; i++)
    {
      fitshandle fits;
      fits.create(fname_list[i]);
      write_powspec_to_fits(fits, *(specs[i]), 1);
    }

#if 0
  {
    fitshandle out;
    
    out.create("!inter_Q.fits");    
    write_Healpix_map_to_fits(out, interpolatedQ, FITSUTIL<float>::DTYPE);
  }
  {
    fitshandle out;

    out.create("!inter_U.fits");    
    write_Healpix_map_to_fits(out, interpolatedU, FITSUTIL<float>::DTYPE);
  }
#endif
  {
    fitshandle out;

    out.create("!errmap.fits");    
    write_Healpix_map_to_fits(out, errmap, FITSUTIL<float>::DTYPE);
  }
#if 0
  {
    fitshandle out;
    out.create("!errcheck.fits");
    write_Healpix_map_to_fits(out, interpolatedQ/errmap, FITSUTIL<float>::DTYPE);
  }
#endif
}
