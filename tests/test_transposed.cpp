/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/test_transposed.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <omp.h>
// STANDARD
#include <cstring>
#include <cassert>
#include <vector>
#include <fstream>
#include <cmath>
#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>
#include <time.h>

// HEALPIX
#include <vec3.h>
#include <planck_rng.h>
#include <xcomplex.h>
#include <arr.h>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <alm_healpix_tools.h>
#include <alm_map_tools.h>
#include <alm.h>
#include <powspec.h>
#include <powspec_fitsio.h>
#include <alm_powspec_tools.h>
#include <fitshandle.h>

// LOCAL
#include "extra_map_tools.hpp"
#include "blens.hpp"
#include "qbinterpol.hpp"

#define BOOST 1
#define PRECISION 1

using namespace CMB;
using namespace std;

int main()
{
  Healpix_Map<float> cmb_map, high_cmb_map,
    dtheta_map, dphi_map;
  int lmax = 400;
  int nside = 256;
  PowSpec powspec(1, lmax), powspec_phi(1,lmax);
  int seed;
  Alm<xcomplex<float> > alm(lmax,lmax), alm_phi(lmax,lmax);
  double clock_start, clock_end;

  seed = 1000;//time(0);
  planck_rng rng(seed);

  read_powspec_from_fits("spectrum.fits", powspec, 1, lmax);
  read_powspec_from_fits("spectrum_phi.fits", powspec_phi, 1, lmax);
  create_alm(powspec, alm, rng);
  create_alm(powspec_phi, alm_phi, rng);
  cout << "Generated alms..." << endl;

  cmb_map.SetNside(nside, RING);
  dtheta_map.SetNside(nside/BOOST, RING);
  dphi_map.SetNside(nside/BOOST, RING);

  cout << "Generating maps..." << endl;
  alm2map(alm, cmb_map);
  cout << "Generating deflection map..." << endl;
  {
    Healpix_Map<float> phimap(nside/BOOST, RING, SET_NSIDE);
    alm2map_der1(alm_phi, phimap, dtheta_map, dphi_map);
  }

  Healpix_Map<float> intermap(cmb_map.Nside(), RING, SET_NSIDE);
  Healpix_Map<float> trans_intermap(cmb_map.Nside(), RING, SET_NSIDE);
  clock_start = omp_get_wtime();
  QuickInterpolateMap<float,double> interpolate(powspec.tt(), 
						1, cmb_map.Nside(),0);

  trans_intermap.fill(0);

  long N = intermap.Npix();

  cout << "Interpolating " << N << " pixels..." << endl;
  clock_start = omp_get_wtime();
  
#pragma omp parallel
  {
#pragma omp for schedule(dynamic,1000000) 
    for (long i = 0; i < N; i++)
      {
	pointing ptg;
	float pixel;

	if ((i%1000000) == 0)
	  cout << i << endl;

	long j = i*BOOST*BOOST;

	computeMovedDirection(dtheta_map[i],
			      dphi_map[i],
			      cmb_map.pix2ang(i), ptg);

	int npix;
	float err;
	intermap[i] = interpolate.interpolate(cmb_map, ptg,  &npix, &err);
     }
  }

  
#pragma omp parallel
  {
#pragma omp for schedule(dynamic,1000000) 
    for (long i = 0; i < N; i++)
      {
	pointing ptg;
	float pixel;

	if ((i%1000000) == 0)
	  cout << i << endl;

	long j = i*BOOST*BOOST;

	computeMovedDirection(dtheta_map[i],
			      dphi_map[i],
			      cmb_map.pix2ang(i), ptg);
	interpolate.interpolateTransposed(intermap[i], trans_intermap, ptg);
      }
  }
	
  double chi2_0 = 0, chi2_1 = 0;
  for (int i = 0; i < intermap.Npix(); i++)
    {
      chi2_0 += intermap[i]*intermap[i];
      chi2_1 += cmb_map[i]*trans_intermap[i];
    }

  cout << "Chi2 = [ " << chi2_0 << " , " << chi2_1 << " ] " << endl;

  return 0;
}

