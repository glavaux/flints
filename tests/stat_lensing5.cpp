/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/stat_lensing5.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <omp.h>
// STANDARD
#include <cstring>
#include <cassert>
#include <vector>
#include <fstream>
#include <cmath>
#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>
#include <time.h>

// HEALPIX
#include <vec3.h>
#include <planck_rng.h>
#include <xcomplex.h>
#include <arr.h>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <alm_healpix_tools.h>
#include <alm_map_tools.h>
#include <alm.h>
#include <powspec.h>
#include <powspec_fitsio.h>
#include <alm_powspec_tools.h>
#include <fitshandle.h>

#include <CosmoTool/miniargs.hpp>

// LOCAL
#include "extra_map_tools.hpp"
#include "blens.hpp"
#include "qbinterpol.hpp"

#define BOOST 1
#define PRECISION 1

using namespace CMB;
using namespace std;
using namespace CosmoTool;

int main(int argc, char **argv)
{
  Healpix_Map<float> cmb_map, high_cmb_map,
    dtheta_map, dphi_map;
  int lmax = 3000;
  int nside = 2048;
  double regulation = 1e-9;
   int nLevel;

  MiniArgDesc desc[] = {
    { "NSIDE", &nside, MINIARG_INT },
    { "LEVEL", &nLevel, MINIARG_INT },
    { "REGULATION", &regulation, MINIARG_DOUBLE },
    { 0,0,MINIARG_NULL }
  };

  if (!parseMiniArgs(argc, argv, desc))
    return 1;

  PowSpec powspec(1, lmax), powspec_phi(1,lmax);
  int seed;
  Alm<xcomplex<float> > alm(lmax,lmax), alm_phi(lmax,lmax);
  double clock_start, clock_end;

  seed = 1000;//time(0);
  planck_rng rng(seed);

  read_powspec_from_fits("spectrum.fits", powspec, 1, lmax);
  create_alm(powspec, alm, rng);
  cout << "Generated alms..." << endl;

  cmb_map.SetNside(nside, RING);
  high_cmb_map.SetNside(nside*2, RING);
  dtheta_map.SetNside(nside/BOOST, RING);
  dphi_map.SetNside(nside/BOOST, RING);

  cout << "Generating maps..." << endl;
  clock_start = omp_get_wtime();
  alm2map(alm, cmb_map);
  clock_end = omp_get_wtime();
  cout << "Time for stdmap = " << (clock_end-clock_start) << endl;

  cout << "Map RMS is " << cmb_map.rms() << endl;
  cout << "Power spectrum: N_Cls = " << powspec.tt().size() << endl;

  Healpix_Map<float> intermap(2*cmb_map.Nside(), RING, SET_NSIDE);
  Healpix_Map<float> errmap(2*cmb_map.Nside(), RING, SET_NSIDE);
  clock_start = omp_get_wtime();
  QuickInterpolateMap<float,double> interpolate(powspec.tt(), 
						nLevel, cmb_map.Nside(),regulation);
  clock_end = omp_get_wtime();
  cout << "Time to initialize interpolator = " << clock_end-clock_start << endl;
 
  long N = intermap.Npix();
  double rad = cmb_map.max_pixrad();
  struct rusage usage;

  cout << "Interpolating " << N << " pixels..." << endl;
  clock_start = omp_get_wtime();
  
#pragma omp parallel
  {
#pragma omp for schedule(dynamic,1000000) 
    for (long i = 0; i < N; i++)
      {
	pointing ptg;
	float pixel;

	if ((i%1000000) == 0)
	  cout << i << endl;

	long j = i*BOOST*BOOST;
	int npix;

        ptg = intermap.pix2ang(i);

	intermap[i] = interpolate.interpolate(cmb_map, ptg,  &npix, &errmap[i]);
     }
  }

  clock_end = omp_get_wtime();
  cout << "Time for a map = " << (clock_end-clock_start) << endl;

  {
    fitshandle out;

    out.create("!errmap.fits");    
    write_Healpix_map_to_fits(out, errmap, FITSUTIL<float>::DTYPE);
  }
  return 0;
}

