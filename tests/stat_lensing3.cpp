/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/stat_lensing3.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <omp.h>
// STANDARD
#include <cstring>
#include <cassert>
#include <vector>
#include <fstream>
#include <cmath>
#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>
#include <time.h>

// HEALPIX
#include <vec3.h>
#include <planck_rng.h>
#include <xcomplex.h>
#include <arr.h>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <alm_healpix_tools.h>
#include <alm_map_tools.h>
#include <alm.h>
#include <powspec.h>
#include <powspec_fitsio.h>
#include <alm_powspec_tools.h>
#include <fitshandle.h>

// LOCAL
#include "extra_map_tools.hpp"
#include "blens.hpp"
#include "qbinterpol.hpp"

#define BOOST 1
#define PRECISION 1

using namespace CMB;
using namespace std;

int main()
{
  Healpix_Map<float> cmb_map, high_cmb_map,
    dtheta_map, dphi_map;
  int lmax = 3000;
  int nside = 1024;
  PowSpec powspec(1, lmax), powspec_phi(1,lmax);
  int seed;
  Alm<xcomplex<float> > alm(lmax,lmax), alm_phi(lmax,lmax);
  double clock_start, clock_end;

  seed = 1000;//time(0);
  planck_rng rng(seed);

  read_powspec_from_fits("spectrum.fits", powspec, 1, lmax);
  read_powspec_from_fits("spectrum_phi.fits", powspec_phi, 1, lmax);
  create_alm(powspec, alm, rng);
  create_alm(powspec_phi, alm_phi, rng);
  cout << "Generated alms..." << endl;

  cmb_map.SetNside(nside, RING);
  high_cmb_map.SetNside(nside*2, RING);
  dtheta_map.SetNside(nside/BOOST, RING);
  dphi_map.SetNside(nside/BOOST, RING);

  cout << "Generating maps..." << endl;
  clock_start = omp_get_wtime();
  alm2map(alm, cmb_map);
  clock_end = omp_get_wtime();
  cout << "Time for stdmap = " << (clock_end-clock_start) << endl;

  clock_start = omp_get_wtime();
 // alm2map(alm, high_cmb_map);
  clock_end = omp_get_wtime();
  cout << "Time for highmap = " <<(clock_end-clock_start) << endl;

  cout << "Generating deflection map..." << endl;
  {
        Healpix_Map<float> phimap(nside/BOOST, RING, SET_NSIDE);
	    alm2map_der1(alm_phi, phimap, dtheta_map, dphi_map);
  }

  cout << "Map RMS is " << cmb_map.rms() << endl;
  cout << "Power spectrum: N_Cls = " << powspec.tt().size() << endl;

  //  Healpix_Map<float> errmap(cmb_map.Nside(), RING, SET_NSIDE);
  //  Healpix_Map<float> numpix(cmb_map.Nside(), RING, SET_NSIDE);
  Healpix_Map<float> intermap(cmb_map.Nside(), RING, SET_NSIDE);
  Healpix_Map<float> errmap(cmb_map.Nside(), RING, SET_NSIDE);
  clock_start = omp_get_wtime();
  QuickInterpolateMap<float,double> interpolate(powspec.tt(), 
						 1, cmb_map.Nside(),0);
  clock_end = omp_get_wtime();
  cout << "Time to initialize interpolator = " << clock_end-clock_start << endl;
 
  long N = intermap.Npix();
  double rad = cmb_map.max_pixrad();
  struct rusage usage;

  cout << "Interpolating " << N << " pixels..." << endl;
  clock_start = omp_get_wtime();
  
#pragma omp parallel
  {
#pragma omp for schedule(dynamic,1000000) 
    for (long i = 0; i < N; i++)
      {
	pointing ptg;
	float pixel;

	if ((i%1000000) == 0)
	  cout << i << endl;

	long j = i*BOOST*BOOST;

#if 1
	computeMovedDirection(dtheta_map[i],
			      dphi_map[i],
			      cmb_map.pix2ang(i), ptg);
#endif

	int npix;
	intermap[i] = interpolate.interpolate(cmb_map, ptg,  &npix, &errmap[i]);
     }
  }

  clock_end = omp_get_wtime();
  cout << "Time for a map = " << (clock_end-clock_start) << endl;

#if 1
  {
    fitshandle out;

    out.create("!interpolated.fits");    
    write_Healpix_map_to_fits(out, intermap, FITSUTIL<float>::DTYPE);
  }
#endif
  {
    fitshandle out;
    out.create("!ref.fits");    
    write_Healpix_map_to_fits(out, cmb_map, FITSUTIL<float>::DTYPE);
  }
  {
    fitshandle out;
    out.create("!high.fits");    
    write_Healpix_map_to_fits(out, high_cmb_map, FITSUTIL<float>::DTYPE);
  }

  {
    fitshandle out;

    out.create("!errmap.fits");    
    write_Healpix_map_to_fits(out, errmap, FITSUTIL<float>::DTYPE);
  }
#if 1
  arr<double> weight(2*nside);
  PowSpec outspec(1,lmax);
  Alm<xcomplex<float> > alm_spec(lmax,lmax);
  weight.fill(1);

  map2alm(intermap, alm_spec, weight);
  extract_powspec(alm_spec, outspec);
  {
    fitshandle fits;
    fits.create("!test_cl_interpolated.fits");
    write_powspec_to_fits(fits, outspec, 1);
  }

  extract_powspec(alm, outspec);
  {
    fitshandle fits;
    fits.create("!test_cl_ref.fits");
    write_powspec_to_fits(fits, outspec, 1);
  }
#endif
  return 0;
}

