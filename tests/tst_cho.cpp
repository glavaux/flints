/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/tst_cho.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <ctime>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <arr.h>
#include "cholesky.hpp"
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>

#define N 37

using namespace std;

int main()
{
  arr<double> Mat(N*N), MatSave(N*N);
  arr<double> LMat(N*N);

  ofstream lf("lmat.txt");
  

  int seed = 1256488808;//time(0);
  srand48(seed);
  cout << "Seed = " << seed << endl;

  for (int i = 0 ; i < N; i++)
    {
      for (int j = 0; j < N; j++)
	{
	  if (i <j)
	    LMat[i*N+j] = 0;
	  else
	    LMat[i*N+j] = drand48();

	  lf << scientific << setprecision(20) << LMat[i*N+j] << endl;
	}
    }

  ofstream mf("matrix.txt");
  
  for (int i = 0 ; i < N; i++)
    {
      for (int j = 0; j < N; j++)
	{
	  if (i > j)
	    {
	      Mat[i*N+j] = Mat[j*N+i];
	      mf << scientific << setprecision(20) << Mat[i*N+j] << endl;
	      continue;
	    }
	  double s = 0;
	  for (int k = 0; k < N; k++)
	    {
	      s += LMat[i*N+k]*LMat[j*N+k];
	    }
	  Mat[i*N+j] = s;
	  mf << scientific << setprecision(20) << Mat[i*N+j] << endl;
	}
    }
  MatSave=Mat;

  int err = CMB::do_cholesky_decomposition(Mat, N);
  assert(err == 0);

  ofstream cf("cho.txt");
  for (int i = 0 ; i < N; i++)
    {
      for (int j = 0; j < N; j++)
	{
	  cf << scientific << setprecision(20) << Mat[i*N+j] << endl;
	}
    }


  ofstream vf("vec.txt");
  arr<double> col(N), colsave(N);

  for (int i = 0; i < N;i++)
    col[i] = (i==0)?1 : 0;

  colsave = col;

  CMB::cholesky_solve_0(Mat, col);
  
  for (int i = 0; i < N; i++)
    vf
      << scientific << setprecision(20) << colsave[i] << " "
      << scientific << setprecision(20) << col[i] << endl; 

  {
    arr<double> basis_v(N), Correlation_vector(N);
    gsl_matrix_view M = gsl_matrix_view_array(&MatSave[0], N, N);
    gsl_vector_view basis = gsl_vector_view_array(&basis_v[0], N);
    gsl_vector_view solution = gsl_vector_view_array(&Correlation_vector[0], N);

    gsl_vector_set_basis(&basis.vector, 0);

    for (int i = 0; i < N; i++)
       for (int j = 0; j < i; j++)
          gsl_matrix_set(&M.matrix, i, j, gsl_matrix_get(&M.matrix, j, i));


    err = gsl_linalg_cholesky_decomp(&M.matrix);
    ofstream cf("cho2.txt");
    for (int i = 0 ; i < N; i++)
      {
	for (int j = 0; j < N; j++)
	  {
	    cf << scientific << setprecision(20) << MatSave[i*N+j] << endl;
	  }
      }
    gsl_linalg_cholesky_solve(&M.matrix, &basis.vector, &solution.vector);

    ofstream vf2("vec2.txt");
    for (int i= 0; i < N; i++)
      {
	vf2 << scientific << setprecision(20) << Correlation_vector[i] << endl;
      }


    ofstream df("delta.txt");
    for (int i = 0; i < N; i++)
      {
	df << scientific << setprecision(20) << (Correlation_vector[i]-col[i])/col[i] << endl;
      }
  }

  return 0;
}
