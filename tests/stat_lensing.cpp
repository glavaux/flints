/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/stat_lensing.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

// STANDARD
#include <cstring>
#include <cassert>
#include <vector>
#include <fstream>
#include <cmath>
#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>
#include <time.h>

// HEALPIX
#include <vec3.h>
#include <planck_rng.h>
#include <xcomplex.h>
#include <arr.h>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <alm_healpix_tools.h>
#include <alm.h>
#include <powspec.h>
#include <powspec_fitsio.h>
#include <alm_powspec_tools.h>
#include <fitshandle.h>

// LOCAL
#include "extra_map_tools.hpp"
#include "blens.hpp"
#include "binterpol.hpp"

using namespace CMB;
using namespace std;

int main()
{
  Healpix_Map<float> cmb_map, 
    dtheta_map, dphi_map;
  int lmax = 3000;
  int nside = 2048;
  PowSpec powspec(1, lmax), powspec_phi(1,lmax);
  int seed;
  Alm<xcomplex<float> > alm(lmax,lmax), alm_phi(lmax,lmax);

  seed = 1000;//time(0);
  planck_rng rng(seed);

  read_powspec_from_fits("spectrum.fits", powspec, 1, lmax);
  read_powspec_from_fits("spectrum_phi.fits", powspec_phi, 1, lmax);
  create_alm(powspec, alm, rng);
  create_alm(powspec_phi, alm_phi, rng);
  cout << "Generated alms..." << endl;

  cmb_map.SetNside(nside, RING);
  dtheta_map.SetNside(nside, RING);
  dphi_map.SetNside(nside, RING);

  cout << "Generating maps..." << endl;
  alm2map(alm, cmb_map);
  cout << "Generating deflection map..." << endl;
  {
    Healpix_Map<float> phimap(nside, RING, SET_NSIDE);
    alm2map_der1(alm_phi, phimap, dtheta_map, dphi_map);
  }

  cout << "Map RMS is " << cmb_map.rms() << endl;
  cout << "Power spectrum: N_Cls = " << powspec.tt().size() << endl;

  //  Healpix_Map<float> errmap(cmb_map.Nside(), RING, SET_NSIDE);
  //  Healpix_Map<float> numpix(cmb_map.Nside(), RING, SET_NSIDE);
  Healpix_Map<float> intermap(cmb_map.Nside(), RING, SET_NSIDE);
  Healpix_Map<float> errmap(cmb_map.Nside(), RING, SET_NSIDE);
  Healpix_Map<float> stdmap(cmb_map.Nside(), RING, SET_NSIDE);
  InterpolateMap<float,double> interpolate(powspec.tt(), 
				     20*cmb_map.max_pixrad(), 
				     500000, cmb_map.Nside());
 
  long N = intermap.Npix();
  double rad = cmb_map.max_pixrad();
  struct rusage usage;

  cout << "Interpolating..." << endl;
  clock_t clock_start = clock();

#pragma omp parallel
  {
#pragma omp for schedule(dynamic,10) 
    for (long i = 0; i < N; i++)
      {
	pointing ptg;
	
	computeMovedDirection(dtheta_map[i],
			      dphi_map[i],
			      cmb_map.pix2ang(i), ptg);
	int npix;
	intermap[i] = interpolate.interpolate9(cmb_map, ptg,  &npix, &errmap[i]);
	stdmap[i] = cmb_map.interpolated_value(ptg);
	assert(!isnan(intermap[i]));
      }
  }

  clock_t clock_end = clock();
  getrusage(RUSAGE_SELF, &usage);

  cout << "Data size = " << usage.ru_idrss << endl;

  {
    cout << "Time for a map = " << double(clock_end-clock_start)/CLOCKS_PER_SEC << endl;
  }

#if 1
  {
    fitshandle out;

    out.create("!interpolated.fits");    
    write_Healpix_map_to_fits(out, intermap, FITSUTIL<float>::DTYPE);
  }
  {
    fitshandle out;
    out.create("!error.fits");    
    write_Healpix_map_to_fits(out, errmap, FITSUTIL<float>::DTYPE);
  }
  {
    fitshandle out;
    out.create("!std.fits");    
    write_Healpix_map_to_fits(out, stdmap, FITSUTIL<float>::DTYPE);
  }
  {
    fitshandle out;
    out.create("!dtheta_map.fits");    
    write_Healpix_map_to_fits(out, dtheta_map, FITSUTIL<float>::DTYPE);
  }
  {
    fitshandle out;
    out.create("!dphi_map.fits");    
    write_Healpix_map_to_fits(out, dphi_map, FITSUTIL<float>::DTYPE);
  }
#endif

  arr<double> weight(2*nside);
  PowSpec outspec(1,lmax);
  weight.fill(1);

  map2alm(intermap, alm, weight);
  extract_powspec(alm, outspec);
  {
    fitshandle fits;
    fits.create("!test_cl.fits");
    write_powspec_to_fits(fits, outspec, 1);
  }
  return 0;
}

