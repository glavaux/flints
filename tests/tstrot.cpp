/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/tstrot.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <omp.h>
// STANDARD
#include <cstring>
#include <cassert>
#include <vector>
#include <fstream>
#include <cmath>
#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>
#include <time.h>

// HEALPIX
#include <vec3.h>
#include <planck_rng.h>
#include <xcomplex.h>
#include <arr.h>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <alm_healpix_tools.h>
#include <alm_map_tools.h>
#include <alm.h>
#include <powspec.h>
#include <powspec_fitsio.h>
#include <alm_powspec_tools.h>
#include <fitshandle.h>

#include "polinterpol.hpp"

using namespace std;

typedef CMB::QuickPolInterpolateMap<float,double> polType;

int main()
{
  int lmax = 3000;
  int nside = 128;
  int seed;
  Alm<xcomplex<float> > almT(lmax,lmax), almG(lmax,lmax),
    almC(lmax,lmax);
  Healpix_Map<float>
    mapT(nside, RING, SET_NSIDE), mapQ(nside, RING, SET_NSIDE),
    mapU(nside, RING, SET_NSIDE);
  Healpix_Map<float>
    mapT2(2*nside, RING, SET_NSIDE), mapQ2(2*nside, RING, SET_NSIDE),
    mapU2(2*nside, RING, SET_NSIDE);
  PowSpec powspec(1, lmax);
  Healpix_Map<xcomplex<float> > polarisation(nside, RING, SET_NSIDE);
  
  seed = 1000;//time(0);
 
  planck_rng rng(seed);

  read_powspec_from_fits("spectrum.fits", powspec, 4, lmax);

  polType p(powspec.gg(),
	    mapT.max_pixrad(), 1, nside);
  
  double alpha,beta,gamma;
  double theta, thetap, phi, phip;

  theta = M_PI;
  phi = 0;
  ofstream f("out.txt");
  for (int i = 0; i < 100; i++)
    {  
      thetap = M_PI/4;
      phip = (i*2*M_PI/100);

      p.computeRotation(theta, phi,
		        thetap, phip,
			alpha, beta, gamma);

      f << thetap << " " << phip << " " << alpha << " " << beta << " " << gamma << endl;
    }
  return 0;
};
