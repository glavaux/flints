/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/diff_map.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <fitshandle.h>
#include "extra_map_tools.hpp"

using namespace CMB;

int main()
{
   Healpix_Map<double> map1, map2;

   read_Healpix_map_from_fits("map1.fits", map1, 1, 2);
   read_Healpix_map_from_fits("map2.fits", map2, 1, 2);

  {
    fitshandle out;

    out.create("!delta_m1_m2.fits");    
    write_Healpix_map_to_fits(out, map1-map2, FITSUTIL<double>::DTYPE);
  }

  return 0;
}
