/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/tst_inv.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <cstdlib>
#include <iostream>
#include "inv.hpp"

using namespace std;

struct MyMatrix
{
  double M[16];

  const double& operator[](int i) const
  {
    return M[i];
  }

  double& operator[](int i)
  {
    return M[i];
  }
};

int main()
{
  MyMatrix A, B, C;

  srand48(1021);
  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++)
      {
	A[4*i + j] = drand48();
	cout << A[4*i+j] << endl;
      }

  for (int q = 0; q < 10000000; q++)
    inv4x4<MyMatrix,double>(A,B);

  prod<MyMatrix,double>(A,B,C);

  for (int i = 0; i < 4; i++)
    {
      for (int j = 0; j < 4; j++)
	{
	  cout << C[4*i + j] << " ";
	}
      cout << endl;
    }

  return 0;
}
