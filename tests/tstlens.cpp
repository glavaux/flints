/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/tstlens.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <iostream>
#include <pointing.h>
#include "blens.hpp"
#include <cmath>

using namespace CMB;
using namespace std;

double mymod(double a)
{
   while (a > M_PI) a -= 2*M_PI;
   while (a < -M_PI) a += 2*M_PI;
   return a;
}

int main()
{
  for (int i = 0 ; i < 100; i++)
    {
       double theta, phi;
       pointing n, new_n, new_n2;

       theta = (drand48()-0.5)*1e-5; 
       phi = (drand48()-0.5)*1e-5;
       n = pointing(drand48()*M_PI,drand48()*2*M_PI);

       computeMovedDirectionClassic(theta, phi, n, new_n);
       computeMovedDirectionNew(theta, phi, n, new_n2);
       cout << new_n.theta-n.theta << " " << new_n.phi-n.phi << " " << mymod(new_n2.theta-n.theta) << " " << mymod(new_n2.phi-n.phi) << endl;
    }
  return 0;
}
