/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/tst_xyf.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <iostream>
#include <cmath>
#include <cstring>
#include <cassert>
#include <vector>
#include <vec3.h>
#include <arr.h>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <fitshandle.h>

using namespace std;

int main()
{
  Healpix_Map<double> map(32, NEST, SET_NSIDE);
  fix_arr<int,8> ngb;

  for (int q = 0; q < 48; q++)
    {
      int pix = q*32*8;
  
      //  for (int i = 0; i < 10000 && pix >= 0; i++)
      //    {
      //      map[pix] = 1;
      map.neighbors(pix, ngb);
      
      for (int i = 0; i < 8; i++)
	if (ngb[i] >= 0)
	  map[ngb[i]] = i+1;
      //pix = ngb[0];
      //    }
    }
  
  {
    fitshandle out;

    out.create("!tst.fits");    
    write_Healpix_map_to_fits(out, map, FITSUTIL<double>::DTYPE);
  }

  return 0;
}
