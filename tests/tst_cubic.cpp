/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/tst_cubic.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <cassert>
#include <cmath>
#include "cubic.hpp"

using namespace std;
using namespace CMB;

int main()
{
  arr<double> x;
  arr<double> p;

  x.alloc(101);
  p.alloc(101);

  for (int i = 0; i < 101; i++)
    {
      x[i] = M_PI*i/100;
      p[i] = sin(x[i]);
    }  

  CubicSpline<double> spline(x, p);

  for (int i = 0; i < 1000; i++)
    {
      double x = M_PI*i/1000;
      cout << x << " " << spline.compute(x) << " " << sin(x) << endl;
    }  

  return 0;
}
