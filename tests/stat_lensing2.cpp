/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/stat_lensing2.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

// STANDARD
#include <cstring>
#include <cassert>
#include <vector>
#include <fstream>
#include <cmath>
#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>
#include <time.h>

// HEALPIX
#include <vec3.h>
#include <planck_rng.h>
#include <xcomplex.h>
#include <arr.h>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <alm_healpix_tools.h>
#include <alm_map_tools.h>
#include <alm.h>
#include <powspec.h>
#include <powspec_fitsio.h>
#include <alm_powspec_tools.h>
#include <fitshandle.h>

// LOCAL
#include "extra_map_tools.hpp"
#include "blens.hpp"
#include "qbinterpol.hpp"

#define BOOST 32
#define PRECISION precParam

using namespace CMB;
using namespace std;

int main(int argc, char **argv)
{
   int precParam = atoi(argv[1]);
  Healpix_Map<float> cmb_map, high_cmb_map,
    dtheta_map, dphi_map;
  int lmax = 3000;
  int nside = 2048;
  PowSpec powspec(1, lmax), powspec_phi(1,lmax);
  int seed;
  Alm<xcomplex<float> > alm(lmax,lmax), alm_phi(lmax,lmax);
  clock_t clock_start, clock_end;

  seed = 1000;//time(0);
  planck_rng rng(seed);

  read_powspec_from_fits("spectrum.fits", powspec, 1, lmax);
  read_powspec_from_fits("spectrum_phi.fits", powspec_phi, 1, lmax);
  create_alm(powspec, alm, rng);
  create_alm(powspec_phi, alm_phi, rng);
  cout << "Generated alms..." << endl;

  { 
     ofstream f("alm_phi.txt");  
     for (long l = 0; l < lmax; l++)
       f << l << " " << alm_phi(l,0) << endl;
  } 

  cmb_map.SetNside(nside, RING);
  high_cmb_map.SetNside(nside*PRECISION, RING);
  dtheta_map.SetNside(nside/BOOST, RING);
  dphi_map.SetNside(nside/BOOST, RING);

  cout << "Generating maps..." << endl;
  clock_start = clock();
  alm2map(alm, cmb_map);
  clock_end = clock();
  cout << "Time for stdmap = " << double(clock_end-clock_start)/CLOCKS_PER_SEC << endl;

  clock_start = clock();
  alm2map(alm, high_cmb_map);
  clock_end = clock();
  cout << "Time for highmap = " << double(clock_end-clock_start)/CLOCKS_PER_SEC << endl;

  cout << "Generating deflection map..." << endl;
  {
    Healpix_Map<float> phimap(nside/BOOST, RING, SET_NSIDE);
    alm2map_der1(alm_phi, phimap, dtheta_map, dphi_map);
  }

  cout << "Map RMS is " << cmb_map.rms() << endl;
  cout << "Power spectrum: N_Cls = " << powspec.tt().size() << endl;

  Healpix_Map<float> errmap(cmb_map.Nside()/BOOST, RING, SET_NSIDE);
  //  Healpix_Map<float> numpix(cmb_map.Nside(), RING, SET_NSIDE);
  Healpix_Map<float> intermap(cmb_map.Nside()/BOOST, RING, SET_NSIDE);
  Healpix_Map<float> delta(cmb_map.Nside()/BOOST, RING, SET_NSIDE);
  Healpix_Map<float> delta2(cmb_map.Nside()/BOOST, RING, SET_NSIDE);
  QuickInterpolateMap<float,double> interpolate(powspec.tt(), 
						1, cmb_map.Nside());
 
  long N = intermap.Npix();
  double rad = cmb_map.max_pixrad();
  struct rusage usage;

  cout << "Interpolating " << N << " pixels..." << endl;
  clock_start = clock();
  
#pragma omp parallel
  {
#pragma omp for schedule(dynamic,10) 
    for (long i = 0; i < N; i++)
      {
	pointing ptg;
	float pixel;

	if ((i%100) == 0)
	  cout << i << endl;

	long j = i*BOOST*BOOST;

	computeMovedDirection(dtheta_map[i],
			      dphi_map[i],
			      intermap.pix2ang(i), ptg);
	ringinfo ri(ptg.theta, ptg.phi, 1.0, 1, 0);
	std::vector<ringpair> vpair;
	vpair.push_back(ringpair(ri));
	alm2map(alm, vpair, &pixel);

	int npix;
	intermap[i] = interpolate.interpolate(cmb_map, ptg,  &npix, &errmap[i]);
	delta[i] = intermap[i]-pixel;
	delta2[i] = high_cmb_map.interpolated_value(ptg) - pixel;
	assert(!isnan(intermap[i]));
      }
  }

  clock_end = clock();
  cout << "Time for a map = " << double(clock_end-clock_start)/CLOCKS_PER_SEC << endl;

#if 1
  {
    fitshandle out;

    out.create("!interpolated.fits");    
    write_Healpix_map_to_fits(out, intermap, FITSUTIL<float>::DTYPE);
  }
#endif

#if 1
  {
    fitshandle out;

    out.create("!delta.fits");    
    write_Healpix_map_to_fits(out, delta, FITSUTIL<float>::DTYPE);
  }
#endif


#if 1
  {
    fitshandle out;

    out.create("!delta2.fits");    
    write_Healpix_map_to_fits(out, delta2, FITSUTIL<float>::DTYPE);
  }
#endif


#if 1
  {
    fitshandle out;

    out.create("!relative_bayes_interpolate.fits");    
    write_Healpix_map_to_fits(out, 100.0*delta/intermap, FITSUTIL<float>::DTYPE);
  }
#endif

#if 1
  {
    fitshandle out;

    out.create("!relative_std_interpolate.fits");    
    write_Healpix_map_to_fits(out, 100.0*delta2/intermap, FITSUTIL<float>::DTYPE);
  }
#endif


#if 1
  {
    fitshandle out;
    out.create("!errmap.fits");
    write_Healpix_map_to_fits(out, errmap, FITSUTIL<float>::DTYPE);
  }
#endif

#if 0
  arr<double> weight(2*nside);
  PowSpec outspec(1,lmax);
  weight.fill(1);

  map2alm(intermap, alm, weight);
  extract_powspec(alm, outspec);
  {
    fitshandle fits;
    fits.create("!test_cl.fits");
    write_powspec_to_fits(fits, outspec, 1);
  }
#endif
  return 0;
}

