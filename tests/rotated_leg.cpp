/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/rotated_leg.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <cstring>
#include <cassert>
#include <vector>
#include <vec3.h>
#include <arr.h>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <cmath>
#include <iostream>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_sf_legendre.h>
#include <powspec.h>
#include <powspec_fitsio.h>
#include <fitshandle.h>
#include "extra_map_tools.hpp"
#include <fstream>
#include "cubic.hpp"
#include "cholesky.hpp"
#include <rotmatrix.h>

//#define USE_GSL

using namespace CMB;
using namespace std;
 
template<typename T>
void compute_Pl_array(T x, arr<T>& Pl) 
{
  int k = 2;
  int n = Pl.size();

  if (n == 0)
    return;
  
  if (n >= 1)
    Pl[0] = 1;
  if (n >= 2)
    Pl[1] = x;

  for (int k = 2; k < n; k++)
    {
      Pl[k] = ((2*k-1)*x*Pl[k-1] - (k-1)*Pl[k-2])/k;
    }
}

//template __attribute__((noinline)) void compute_Pl_array<double>(double x, arr<double>& Pl);
 
template<typename T>
Healpix_Map<T> operator/(const Healpix_Map<T>& m1, const Healpix_Map<T>& m2)
{
  Healpix_Map<T> m(m1.Nside(), m1.Scheme(), SET_NSIDE);

  for (long i = 0; i < m.Npix(); i++)
    m[i] = m1[i]/m2[i];
  return m;
}

template<typename T>
class InterpolateMap
{
private:
  arr<T> cls;  
  arr<T> result;
  arr<vec3> directions;
  T variance;
  int lmax;
  double delta, maxAngle;
  arr<T> precomputed_correlations;
  CubicSpline<T> spline;
  const Healpix_Map<T> *tmap;
  Healpix_Base base_36;
public:

  InterpolateMap(const Healpix_Map<T> *map, const arr<T>& spectrum, double maxAngle, int numPoints, int numNgb) throw()
    : tmap(map), cls(spectrum), lmax(spectrum.size()-1), result(spectrum.size()),
      base_36(map->Nside()/2, NEST, SET_NSIDE)
  {    
    result.fill(0);
    result[0] = 1;

//    planck_assert(map->Scheme() == NEST, "NESTED scheme required");
    
    this->maxAngle = maxAngle;
    precomputeVariance();
    precomputeLegendre(numPoints);
    checkPrecision();
    precomputeDirections();
  }

  void precomputeDirections()
  {
    cout << "Precomputing directions" << endl;
    directions.alloc(tmap->Npix());
    for (int i = 0; i < tmap->Npix(); i++)
      directions[i] = tmap->pix2ang(i);
  }

  void precomputeVariance()
    throw (Message_error)
  {
    // This is the exact variance
    cout << "Precomputing pixel variance" << endl;
    variance = fullComputation(1.0);
  }

  T fullComputation(T x)
    throw (Message_error)
  {
    compute_Pl_array(x, result);
    
    T accum = 0;
    assert(lmax == result.size()-1);
    assert(lmax == cls.size()-1);
    for (int l = 1; l <= lmax; l++)
      accum += (2*l+1)*result[l]*cls[l];
    accum += result[0]*cls[0];
    accum /= (4*M_PI);
    return accum;
  }
  
  // We precompute here a cubic interpolation
  // of Legendre polynomials. This will speed up
  // the interpolation algorithm while keeping
  // much of the precision (quantitatively ?)
  void  precomputeLegendre(int numPoints)
    throw(Message_error)
  {
    cout << "Precomputing correlation function" << endl;

    delta = (1-cos(maxAngle))/(numPoints);
    arr<double> xs;
    
    xs.alloc(numPoints);
    precomputed_correlations.alloc(numPoints);
    for (int i = 0; i < numPoints; i++)
      {
	xs[i] = delta*i;
	precomputed_correlations[i] = fullComputation(1-xs[i]);
      }
    spline = CubicSpline<T>(xs,precomputed_correlations);
  }

  T computeCorrelation(double x)
  {
    return spline.computeRegular(1-x);
  }
  
  void checkPrecision()    
  {
    ofstream tfile("test_file.txt");
    long n = precomputed_correlations.size();
    double xmax = (1-cos(maxAngle))*0.9;
    
    for (int i = 0; i < 10000; i++)
      {
	double q = drand48()*xmax;
	double x = 1-q;
	double a = computeCorrelation(x);
	double b = fullComputation(x);

	tfile << q << " " << x << " " << a << " " << b << " " << a-b << endl;
      }
  }

  ~InterpolateMap() throw()
  {
  }

  /**
   * This function does the actual interpolation of the map "map"
   * in the direction "dir".
   */
  T interpolate4(const pointing& ptg, double* var) 
    throw(Message_error)
  {
    fix_arr<int,4> pixels;
    fix_arr<double,4> wgt;
    fix_arr<double,25> Correlation;
    fix_arr<vec3,5> all_vec;
    gsl_matrix_view M = gsl_matrix_view_array(&Correlation[0], 5, 5);
    int err;

    // First we retrieve the neighbouring pixels.
    tmap->get_interpol(ptg, pixels, wgt);

    // We compute the relative direction of the pixels compared 
    // to the direction of interpolation.
    all_vec[0] = ptg.to_vec3();

    for (int i = 1; i < 5; i++)
      all_vec[i] = directions[pixels[i-1]];
    
    // Do the summations up to lmax for i != j   
    for (int i = 1; i < 5; i++)
      {
	for (int j = 0; j < i; j++)
	  {
	    double cos_theta = dotprod(all_vec[i], all_vec[j]);
	    double accum = computeCorrelation(cos_theta);

	    gsl_matrix_set(&M.matrix, i, j, accum);
	    gsl_matrix_set(&M.matrix, j, i, accum);
	  }    
      }

    // Do the summations up to lmax for i == j   
    {
      // cos(theta) == 1 <=> theta = 0 <=> i==j
      for (int i = 0; i < 5; i++)
	gsl_matrix_set(&M.matrix, i, i, variance);
    }
    
    err = gsl_linalg_cholesky_decomp(&M.matrix);
    planck_assert(err == GSL_SUCCESS, "Error in cholesky decomposition");
    err = gsl_linalg_cholesky_invert (&M.matrix);
    planck_assert(err == GSL_SUCCESS, "Cannot inverse covariance matrix");
    
    T accum_value = 0;
    for (int i = 0; i < 4; i++)
      accum_value += Correlation[i+1]*(*tmap)[pixels[i]];	
    accum_value /= Correlation[0];

    if (var != 0)
      *var = sqrt(1/Correlation[0]);

    return -accum_value;
  }

  void setupMatrix(const arr<vec3>& all_vec, arr<double>& M, int npix)
  {
    for (int i = 0; i < npix; i++)
      {
	M[i*npix+i] = variance;

	for (int j = i+1; j < npix; j++)
	  {
	    double cos_theta = dotprod(all_vec[i], all_vec[j]);

	    //planck_assert(cos_theta*cos_theta <= 1, "Error, |cos(theta)| > 1");

	    M[i*npix + j] = computeCorrelation(cos_theta);
	  }
      }
  }

  T interpolateOnPixels(const pointing& ptg, int *  pixels,
			int npix, double* var) 
    throw(Message_error)
  {
    arr<double> Correlation, Correlation_vector, unit_column;
    arr<vec3> all_vec;
    int err;

    npix++;
    all_vec.alloc(npix);
    all_vec[0] = ptg.to_vec3();

    for (int i = 1; i < npix; i++)
      all_vec[i] = directions[pixels[i-1]];

    Correlation.alloc(npix*npix);
    Correlation_vector.alloc(npix);

    setupMatrix(all_vec, Correlation, npix);
    
#ifdef USE_GSL
    arr<double> basis_v(npix);
    gsl_matrix_view M = gsl_matrix_view_array(&Correlation[0], npix, npix);
    gsl_vector_view basis = gsl_vector_view_array(&basis_v[0], npix);
    gsl_vector_view solution = gsl_vector_view_array(&Correlation_vector[0], npix);

    gsl_vector_set_basis(&basis.vector, 0);

    for (int i = 0; i < npix; i++)
       for (int j = 0; j < i; j++)
          gsl_matrix_set(&M.matrix, i, j, gsl_matrix_get(&M.matrix, j, i));

    err = gsl_linalg_cholesky_decomp(&M.matrix);
    gsl_linalg_cholesky_solve(&M.matrix, &basis.vector, &solution.vector);
#else
    err = do_cholesky_decomposition(Correlation, npix);
    if (err < 0)
      {
	setupMatrix(all_vec, Correlation, npix);
	ofstream mfile("matrix.txt");
	for (int i = 0; i < npix; i++)
	  {
	    for (int j = 0; j < npix; j++)
	      {
		if (j >= i)
		  mfile << Correlation[i*npix+j] << endl;
		else
		  mfile << Correlation[j*npix+i] << endl;
	      }
	  }
	cout << "Error in cholesky decomposition. Matrix dumped to 'matrix.txt'" << endl;
	abort();
      }
    cholesky_solve_0(Correlation, Correlation_vector);
#endif

    // Compute the scalar product
    T accum_value = 0;
    for (int i = 0; i < npix-1; i++)
      accum_value += Correlation_vector[i+1]*(*tmap)[pixels[i]];	
    accum_value /= Correlation_vector[0];
    
    if (var != 0)
      *var = sqrt(1/Correlation_vector[0]);

    return -accum_value;
  }

  T interpolate9(const pointing& ptg, int *npix, double *var)
  {
    fix_arr<int,9> pixels;
    fix_arr<int,8> subpix;
    int valid_pix = 9;

    int pix = tmap->ang2pix(ptg);
    
    pixels[0] = pix;
    tmap->neighbors (pix, subpix);
    memcpy(&pixels[1], &subpix[0], sizeof(int)*8);
    sort(&pixels[0], &pixels[8]);

    for (valid_pix = 0; valid_pix < 9; valid_pix++)
      if (pixels[valid_pix] >= 0)
    	break;

    if (npix)
      *npix = 9-valid_pix;
    return interpolateOnPixels(ptg, &pixels[valid_pix], 9-valid_pix, var);
  }

  T interpolate36(const pointing& ptg, int *npix, double *var)
  {
    fix_arr<int,36> pixels;
    fix_arr<int,8> base_pix;
    int valid_pix = 9;

    int pix = base_36.ang2pix(ptg);
    
    // Get the pixel id in the nside/2 resolution
    base_36.neighbors (pix, base_pix);
    // Now we may multiply the number of available pixels
    // for the neighborhood by adding the 2 missing bits
    sort(&base_pix[0], &base_pix[8]);
    for (valid_pix = 0; valid_pix < 8; valid_pix++)
      if (base_pix[valid_pix] >= 0)
	break;

    pix <<= 2;
    for (int k = 0; k < 4; k++)
      pixels[k] = tmap->nest2ring(pix+k);

    int numPix = 4;
    for (int i = valid_pix; i < 8; i++,numPix+=4)
      {
	int tmp_pix = base_pix[i];

	tmp_pix <<= 2;
	for (int l = 0; l < 4; l++)
	  pixels[numPix+l] = tmap->nest2ring(tmp_pix+l);
      }

    if (npix)
      *npix = numPix;
    return interpolateOnPixels(ptg, &pixels[0], numPix, var);
  }



  /**
   * This function does the actual interpolation of the map "map"
   * in the direction "dir".
   */
  T interpolateRad(const pointing& ptg, double rad, double* var, int *numpix) 
    throw(Message_error)
  {
    vector<int> pixels;
    int npix;

    // First we retrieve the neighbouring pixels.
    tmap->query_disc (ptg, rad, pixels);

    npix = pixels.size();
    if (numpix)
      *numpix = npix;

    return interpolateOnPixels(ptg, &pixels[0], npix, var);
  }
};

using namespace std;

int main()
{
  Healpix_Map<double> mymap, mymap_ref;
  int lmax = 2000;
  PowSpec powspec(1, lmax);

  read_Healpix_map_from_fits("mycmb.fits", mymap, 1, 2);
  read_Healpix_map_from_fits("mycmb_rot.fits", mymap_ref, 1, 2);
  
  
  cout << "Map RMS is " << mymap.rms() << endl;

  read_powspec_from_fits("spectrum.fits", powspec, 1, lmax);
  cout << "Got N_Cls = " << powspec.tt().size() << endl;
  arr<double> flatspec(lmax+1);
  flatspec.fill(1);

  rotmatrix rot;

  rot.Make_Axis_Rotation_Transform(vec3(0,0,1), 0.0025);

  Healpix_Map<double> intermap(mymap.Nside(), RING, SET_NSIDE);
  Healpix_Map<double> errmap(mymap.Nside(), RING, SET_NSIDE);
  Healpix_Map<double> errmap2(mymap.Nside(), RING, SET_NSIDE);
  Healpix_Map<double> healinter(mymap.Nside(), RING, SET_NSIDE);
  Healpix_Map<double> numpix(mymap.Nside(), RING, SET_NSIDE);
  InterpolateMap<double> interpolate(&mymap, powspec.tt(), 12*mymap.max_pixrad(), 500000, 9);
 
  long N = intermap.Npix();
  double rad = mymap.max_pixrad()*3;

  clock_t clock_start = clock();

  for (int i = 0; i < N; i++)
    {
      if ((i % (N/100)) == 0)
	cout << i*100/intermap.Npix() << " %" <<  endl;

      pointing ptg = pointing(rot.Transform(intermap.pix2vec(i)));
      int npix;
      intermap[i] = interpolate.interpolateRad(ptg, rad, &errmap[i], &npix);
      numpix[i] = npix;
      //healinter[i] = mymap.interpolated_value (ptg);      
    }

  clock_t clock_end = clock();

  {
    cout << "Time for a map = " << double(clock_end-clock_start)/CLOCKS_PER_SEC << endl;
  }

  {
    fitshandle out;

    out.create("!delta_high_simple.fits");    
    write_Healpix_map_to_fits(out, mymap_ref-healinter, FITSUTIL<double>::DTYPE);
  }

  {
    fitshandle out;

    out.create("!numpix.fits");    
    write_Healpix_map_to_fits(out, numpix, FITSUTIL<double>::DTYPE);
  }


  {
    fitshandle out;

    out.create("!error.fits");    
    write_Healpix_map_to_fits(out, errmap, FITSUTIL<double>::DTYPE);
  }

  {
    fitshandle out;

    out.create("!error_flat.fits");    
    write_Healpix_map_to_fits(out, errmap2, FITSUTIL<double>::DTYPE);
  }

  {
    fitshandle out;

    out.create("!interpolated.fits");    
    write_Healpix_map_to_fits(out, intermap, FITSUTIL<double>::DTYPE);
  }

  {
    fitshandle out;

    out.create("!delta_high_inter.fits");    
    write_Healpix_map_to_fits(out, abs(mymap-intermap), FITSUTIL<double>::DTYPE);
  }

  {
    fitshandle out;

    out.create("!relative_high_inter.fits");    
    write_Healpix_map_to_fits(out, (mymap-intermap)/mymap, FITSUTIL<double>::DTYPE);
  }

  return 0;
}

