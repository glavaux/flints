/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/perf_lensing.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

// STANDARD
#include <cstring>
#include <cassert>
#include <vector>
#include <fstream>
#include <cmath>
#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>
#include <time.h>

// HEALPIX
#include <vec3.h>
#include <planck_rng.h>
#include <xcomplex.h>
#include <arr.h>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <alm_healpix_tools.h>
#include <alm_map_tools.h>
#include <alm.h>
#include <powspec.h>
#include <powspec_fitsio.h>
#include <alm_powspec_tools.h>
#include <fitshandle.h>

// LOCAL
#include "extra_map_tools.hpp"
#include "blens.hpp"
#include "binterpol.hpp"

#define BOOST 1
#define PRECISION 1

using namespace CMB;
using namespace std;

int main()
{
  Healpix_Map<float> cmb_map, high_cmb_map,
    dtheta_map, dphi_map;
  int lmax = 3000;
  int nside = 1024;
  PowSpec powspec(1, lmax), powspec_phi(1,lmax);
  int seed;
  Alm<xcomplex<float> > alm(lmax,lmax), alm_phi(lmax,lmax);
  clock_t clock_start, clock_end;

  seed = 1000;//time(0);
  planck_rng rng(seed);

  read_powspec_from_fits("spectrum.fits", powspec, 1, lmax);
  read_powspec_from_fits("spectrum_phi.fits", powspec_phi, 1, lmax);
#if 0
  create_alm(powspec, alm, rng);
  create_alm(powspec_phi, alm_phi, rng);
  cout << "Generated alms..." << endl;
#endif

  cmb_map.SetNside(nside, RING);
  dtheta_map.SetNside(nside/BOOST, RING);
  dphi_map.SetNside(nside/BOOST, RING);

#if 0
  cout << "Generating maps..." << endl;
  clock_start = clock();
  alm2map(alm, cmb_map);
  clock_end = clock();
  cout << "Time for stdmap = " << double(clock_end-clock_start)/CLOCKS_PER_SEC << endl;

  clock_start = clock();
  alm2map(alm, high_cmb_map);
  clock_end = clock();
  cout << "Time for highmap = " << double(clock_end-clock_start)/CLOCKS_PER_SEC << endl;

  cout << "Generating deflection map..." << endl;
  {
    Healpix_Map<float> phimap(nside/BOOST, RING, SET_NSIDE);
    alm2map_der1(alm_phi, phimap, dtheta_map, dphi_map);
  }

  cout << "Map RMS is " << cmb_map.rms() << endl;
  cout << "Power spectrum: N_Cls = " << powspec.tt().size() << endl;
#endif
  //  Healpix_Map<float> errmap(cmb_map.Nside(), RING, SET_NSIDE);
  //  Healpix_Map<float> numpix(cmb_map.Nside(), RING, SET_NSIDE);
  Healpix_Map<float> intermap(cmb_map.Nside()/BOOST, RING, SET_NSIDE);
  InterpolateMap<float,double> interpolate(powspec.tt(), 
				     20*cmb_map.max_pixrad(), 
				     500000, cmb_map.Nside());
 
  long N = intermap.Npix();
  double rad = cmb_map.max_pixrad();
  struct rusage usage;

  cout << "Interpolating " << N << " pixels..." << endl;
  clock_start = clock();
  
#pragma omp parallel
  {
#pragma omp for schedule(dynamic,100000) 
    for (long i = 0; i < N; i++)
      {
	pointing ptg;

	long j = i*BOOST*BOOST;

#if 0
	computeMovedDirection(dtheta_map[i],
			      dphi_map[i],
			      cmb_map.pix2ang(j), ptg);
#endif
	ptg = cmb_map.pix2ang(j);
        ptg.theta += 1e-5;
        ptg.phi += 1e-5;
	int npix;
	intermap[i] = interpolate.interpolate9(cmb_map, ptg,  &npix, 0);
      }
  }

  clock_end = clock();
  cout << "Time for a map = " << double(clock_end-clock_start)/CLOCKS_PER_SEC << endl;

#if 1
  {
    fitshandle out;

    out.create("!interpolated.fits");    
    write_Healpix_map_to_fits(out, intermap, FITSUTIL<float>::DTYPE);
  }
#endif

  return 0;
}

