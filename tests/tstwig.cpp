/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/tstwig.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <iomanip>
#include <arr.h>
#include <iostream>
#include "wignerFunction.hpp"

using namespace std;

int main(int argc,char **argv)
{
  int l = 2000;
  
  int m1 = 1900;
  int m2 = 1900;

  int N = 16384;
  arr<double> buf;
  arr<double> wd1;
  arr<double> wd2;

  buf.alloc(N);
  buf.fill(0);
  wd1.alloc(N);
  wd1.fill(0);
  wd2.alloc(N);
  wd2.fill(0);

  for (int n = -l; n <= l; n++)
    {
      CMB::computeWigner_D(l, m1, n, N, &wd1[0]);
      CMB::computeWigner_D(l, m2, n, N, &wd2[0]);

      for (int i = 0; i < N; i++)
	buf[i] += wd1[i]*wd2[i];
    }

#if 0
      for (int n = -l; n<=l;n++)
        cout << setw(8) << n << ",m1 " << setw(8) << n << ",m2";
    cout << endl;
   cout << "------------------------------------------" << endl;
#endif
  for (int i = 0; i < N; i++)
    {
//      for (int n = -l; n<=l;n++)
//        cout << setw(11) << setprecision(5) << all_d1[n+l][i] << " " << setw(11) << setprecision(5) << all_d2[n+l][i] << " ";
     cout << buf[i] << endl;
   }

  return 0;
}
