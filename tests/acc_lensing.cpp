/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/acc_lensing.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

// STANDARD
#include <cstring>
#include <cassert>
#include <vector>
#include <fstream>
#include <cmath>
#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>

// HEALPIX
#include <vec3.h>
#include <arr.h>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <powspec.h>
#include <powspec_fitsio.h>
#include <fitshandle.h>

// LOCAL
#include "extra_map_tools.hpp"
#include "blens.hpp"
#include "binterpol.hpp"

using namespace CMB;
using namespace std;

int main()
{
  Healpix_Map<float> cmb_map, 
    dtheta_map, dphi_map;
  int lmax = 3000;
  PowSpec powspec(1, lmax);

  read_Healpix_map_from_fits("mycmb.fits", cmb_map, 1, 2);
  cout << "Nside CMB = " << cmb_map.Nside() << endl;
  read_Healpix_map_from_fits("phi.fits", dtheta_map, 2, 2);
  read_Healpix_map_from_fits("phi.fits", dphi_map, 3, 2);
  read_powspec_from_fits("spectrum.fits", powspec, 1, lmax);

  cout << "Map RMS is " << cmb_map.rms() << endl;

  cout << "Power spectrum: N_Cls = " << powspec.tt().size() << endl;

  Healpix_Map<float> errmap(cmb_map.Nside(), RING, SET_NSIDE);
  Healpix_Map<float> numpix(cmb_map.Nside(), RING, SET_NSIDE);
  Healpix_Map<float> intermap(cmb_map.Nside(), RING, SET_NSIDE);
  InterpolateMap<float,double> interpolate(powspec.tt(), 
				     20*cmb_map.max_pixrad(), 
				     500000, cmb_map.Nside());
 
  long N = intermap.Npix();
  double rad = cmb_map.max_pixrad();
  struct rusage usage;

  clock_t clock_start = clock();

#pragma omp parallel
  {
#pragma omp for schedule(dynamic,10) 
    for (long i = 0; i < N; i++)
      {
	//	if ((i % (N/100)) == 0)
	//cout << i*100/intermap.Npix() << " %" <<  endl;
	
	pointing ptg;
	
	computeMovedDirection(dtheta_map[i],
			      dphi_map[i],
			      cmb_map.pix2ang(i), ptg);
	int npix;
	intermap[i] = interpolate.interpolate9(cmb_map, ptg,  &npix, &errmap[i]);
	assert(!isnan(intermap[i]));
	numpix[i] = npix;
      }
  }

  clock_t clock_end = clock();
  getrusage(RUSAGE_SELF, &usage);

  cout << "Data size = " << usage.ru_idrss << endl;

  {
    cout << "Time for a map = " << double(clock_end-clock_start)/CLOCKS_PER_SEC << endl;
  }

  {
    fitshandle out;

    out.create("!numpix.fits");    
    write_Healpix_map_to_fits(out, numpix, FITSUTIL<double>::DTYPE);
  }


  {
    fitshandle out;

    out.create("!error.fits");    
    write_Healpix_map_to_fits(out, errmap, FITSUTIL<double>::DTYPE);
  }

  {
    fitshandle out;

    out.create("!interpolated.fits");    
    write_Healpix_map_to_fits(out, intermap, FITSUTIL<double>::DTYPE);
  }

  return 0;
}

