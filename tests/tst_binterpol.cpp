/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/tst_binterpol.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <healpix_map.h>
#include "binterpol.hpp"


int main()
{
  arr<double> spectrum;
  CMB::InterpolateMap<double> inter(spectrum, 1.0, 10000, 2048);
  Healpix_Map<double> map(2048, RING, SET_NSIDE);
  pointing ptg;
  int npix;
  double var;
  
  double v = inter.interpolate9(map, ptg, &npix, &var);

  return 0;
};

