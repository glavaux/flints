/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./tests/tstgeom.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <iomanip>
#include <cassert>
#include <arr.h>
#include <healpix_base.h>
#include <iostream>
#include <fstream>
#include "packMatrix.hpp"
#include "qbinterpol.hpp"
#include <powspec.h>
#include <powspec_fitsio.h>

using namespace std;
using namespace CMB;

void printMatrix(arr<double>& M, int N)
{
  for (int i = 0; i < N; i++)
    {
      for (int j = 0; j < N; j++)
	{
	  cout << setw(6) << setprecision(3) << M[i*N+j] << " ";
	}
      cout << endl;
    }
}

int main()
{
  arr<double> pixGeometry;
  arr<double> matrix, refMatrix;
  Healpix_Base map;
  int lmax = 3000;
  int Nside=512;
 int numNgb=9;
  PowSpec powspec(1, lmax);
  
  read_powspec_from_fits("spectrum.fits", powspec, 1, lmax);

  map.SetNside(Nside, RING);
  int thisRing = 400;
  int startpix, ringpix;
  double costheta, sintheta;
  bool shifted;


  // To check equatorial region
#if 0
  map.get_ring_info (thisRing, startpix, ringpix, costheta, sintheta, shifted);

  int centerMatrix = startpix;

  upMatrix<false>(&pixGeometry[centerMatrix*numMatElements], refMatrix, numNgb);

  for (int i = 0 ; i < map.Npix(); i++)
    {      
      int ring = map.pix2ring(i);

      if (ring != Nside-thisRing)
	continue;

      double norm = 0;
      upMatrix<false>(&pixGeometry[i*numMatElements], matrix, numNgb);
      for (int j = 0 ; j < numNgb*numNgb; j++)
	{
	  double delta = matrix[j]-refMatrix[j];
	  if (abs(delta) > norm)
	    norm = abs(delta);
	}
      cout << ring << " "<< norm << endl;
    }
#endif

#if 0
  map.get_ring_info (thisRing, startpix, ringpix, costheta, sintheta, shifted);
  int otherRing = 4*Nside-thisRing;
  int o_startpix, o_ringpix;
  bool o_shifted;
  double o_costheta, o_sintheta;

  map.get_ring_info (otherRing, o_startpix, o_ringpix, o_costheta, o_sintheta, o_shifted);

  cout << ringpix << " vs " << o_ringpix << endl;
  cout << costheta << " vs " << o_costheta << endl;
  cout << shifted << " vs " << o_shifted << endl;

  assert(o_ringpix == ringpix);
  assert(o_costheta == -costheta);

  for (int i = 1; i < ringpix; i++)
    {
      upMatrix<false>(&pixGeometry[(i+startpix)*numMatElements], refMatrix, numNgb);
      double norm = 0;
      int perm[9] = { 0, 3, 2, 1, 8, 7, 6, 5, 4 };
      int r = o_startpix+i;
      int q = 4*thisRing*thisRing-map.Npix()+r;

      assert(q==(i+startpix));

      upMatrix<false>(&pixGeometry[(o_startpix+i)*numMatElements], matrix, perm, numNgb);

      for (int j = 0 ; j < numNgb; j++)
	for (int k = 0; k < numNgb; k++)
	  {
	    double delta = matrix[j*numNgb+k]-refMatrix[j*numNgb+k];
	    if (abs(delta) > norm)
	      norm = abs(delta);
	  }
      cout << i << " "<< norm << endl;
    }
  
#endif

  QuickInterpolateMap<float,double>
    interpol(powspec.tt(), 
	     1, Nside);
  arr<int> pixels(9);
  arr<vec3> all_vec(9);
  arr<double> cho(81);
  arr<double> M(81), M2(81);
  arr<double> outMatrix(81);
  map.get_ring_info (thisRing, startpix, ringpix, costheta, sintheta, shifted);

  int Nmacro = ringpix/4; 
  for (int i = ringpix/4; i < ringpix; i++)
    {
      interpol.computeMatrix(startpix+i, pixels, all_vec, cho, M, &outMatrix[0]);
      interpol.computeMatrix(startpix+(i%Nmacro), pixels, all_vec, cho, M2, &outMatrix[0]);

	double norm = 0;
      for (int j = 0 ; j < numNgb; j++)
	for (int k = 0; k < numNgb; k++)
	  {
	    double delta = M[j*numNgb+k]-M2[j*numNgb+k];
	    if (abs(delta) > norm)
	      norm = abs(delta);
	  }
      cout << i << " "<< norm << endl;
    }

  return 0;

}
