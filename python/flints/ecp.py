from _ecp_map import ecp_alm2map, ecp_map2alm, ecp_alm2map_spin, ecp_map2alm_spin
from _ecp_map import ecp_pix2ang as _internal_ecp_pix2ang
from _flints_ecp import interpolate_map, interpolate_map_spin2
import numpy as np

def ecp_to_2d_array(m, nrings, nphi):
  """ecp_to_2d_array(map, nrings, nphi) returns a 2d-array (Nrings x Nphi) from the input
  1d array"""
  return m.reshape((nrings,nphi))

def ecp_from_2d_array(m, nrings, nphi):
  """ecp_from_2d_array(map, nrings, nphi) returns a 1d-array (Nrings * Nphi) from the input
  2d array"""
  return m.reshape((nrings*nphi))
  

def ecp_pix2ang(ipix, nrings, nphi):
  """ecp_pix2ang(ipix, nrings, nphi) returns a tuple of 1d-arrays (theta,phi) corresponding
  to the angular coordinates of each pixel on the ECP grid."""
  
  if hasattr(ipix, "__len__"):
    ipix = np.array(ipix,dtype=np.int32)
    return _internal_ecp_pix2ang(ipix, nrings, nphi)

  else:
    ipix = np.array([ipix], dtype=np.int32)
    t,p= _internal_ecp_pix2ang(ipix, nrings, nphi)
    return t[0],p[0]
