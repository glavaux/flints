from cython.view cimport array as cvarray
cimport cython
cimport numpy as npx
import numpy as np
import healpy as hp

cdef extern from "xcomplex.h":

  cdef cppclass xcomplex[T]:
    xcomplex()
    

cdef extern from "psht_cxx.h":

  cdef cppclass psht_joblist[T]:
    psht_joblist()
    void set_ECP_geometry(int,int)
    void set_triangular_alm_info(long,long)
    void add_alm2map(const xcomplex[T] *, T *m, bool)
    void add_alm2map_spin(const xcomplex[T] *almG, const xcomplex[T] *almC, T *mQ, T *mU, int, bool)
    void add_map2alm_spin(T *mQ, T *mU, xcomplex[T] *almG, xcomplex[T] *almC, int, bool)
    void add_map2alm(T *m, xcomplex[T] *, bool)
    void execute()


def test_array(int n):
  return np.empty(n,dtype=np.float)

def ecp_alm2map(double complex[:] alms, int nrings, int nphi):
  cdef psht_joblist[double] psht
  cdef const xcomplex[double] *alm_c
  cdef npx.ndarray[dtype=double,ndim=1] m0
  cdef long Nm
    
  lmax = hp.Alm.getlmax(alms.size)
    
  alm_c = <const xcomplex[double] *>(&alms[0])

  Nm = nrings*nphi
  m = np.zeros(Nm, dtype=np.float)
  m0 = m

  psht.set_ECP_geometry(nrings, nphi)
  psht.set_triangular_alm_info(lmax, lmax)
  psht.add_alm2map(alm_c, &m0[0], False)
  psht.execute()
  
  return m
    
def ecp_map2alm(double[:] m, int lmax, int nrings, int nphi):
  cdef psht_joblist[double] psht
  cdef npx.ndarray[dtype=npx.complex128_t,ndim=1] alm
  cdef xcomplex[double] *alm_c

  alm = np.empty(hp.Alm.getsize(lmax),dtype=np.complex128)
  alm_c = <xcomplex[double] *>(&alm[0])

  psht.set_ECP_geometry(nrings, nphi)
  psht.set_triangular_alm_info(lmax, lmax)
  psht.add_map2alm(&m[0], &alm_c[0], False)
  psht.execute()
  
  return alm
  
  
def ecp_alm2map_spin(double complex[:] almsG, double complex[:] almsC, int spin, int nrings, int nphi):
  """
  ecp_alm2map_spin do a map synthesis from two alms set (almsG and almsC), assuming the field have some spin.
  The map has ECP pixelisation.
  
  Parameters:
    - almsG: E component of the field
    - almsC: B component of the field
    - spin: spin number of the field
    - nrings, nphi: ECP parameters
    
  Returns:
    - tuple of the real and imaginary parts of the field
  """
  cdef psht_joblist[double] psht
  cdef const xcomplex[double] *almG_c
  cdef const xcomplex[double] *almC_c
  cdef npx.ndarray[dtype=double,ndim=1] mQ, mU
  cdef long Nm
    
  lmaxG = hp.Alm.getlmax(almsG.size)
  lmaxC = hp.Alm.getlmax(almsC.size)
    
  if lmaxG != lmaxC:
    raise ValueError("E and B alms component must have the same lmax")

  almG_c = <const xcomplex[double] *>(&almsG[0])
  almC_c = <const xcomplex[double] *>(&almsC[0])

  Nm = nrings*nphi
  mQ = np.zeros(Nm, dtype=np.float)
  mU = np.zeros(Nm, dtype=np.float)

  psht.set_ECP_geometry(nrings, nphi)
  psht.set_triangular_alm_info(lmaxG, lmaxG)
  psht.add_alm2map_spin(almG_c, almC_c, &mQ[0], &mU[0], spin, False)
  psht.execute()
  
  return mQ,mU
    
def ecp_map2alm_spin(double[:] mQ, double[:] mU, int spin, int lmax, int nrings, int nphi):
  """
  ecp_map2alm_spin do a map analysis from a field (mQ real part, mU imaginary part), assuming the field have some spin.
  The map must have an ECP pixelisation.
  
  Parameters:
    - mQ: Real component of the field
    - mU: Imaginary component of the field
    - spin: spin number of the field
    - lmax: lmax to do the analysis
    - nrings, nphi: ECP parameters
    
  Returns:
    a tuple of alms
  """
  cdef psht_joblist[double] psht
  cdef npx.ndarray[dtype=npx.complex128_t,ndim=1] almG, almC
  cdef xcomplex[double] *almG_c
  cdef xcomplex[double] *almC_c

  almG = np.empty(hp.Alm.getsize(lmax),dtype=np.complex128)
  almC = np.empty(hp.Alm.getsize(lmax),dtype=np.complex128)
  almG_c = <xcomplex[double] *>(&almG[0])
  almC_c = <xcomplex[double] *>(&almC[0])

  psht.set_ECP_geometry(nrings, nphi)
  psht.set_triangular_alm_info(lmax, lmax)
  psht.add_map2alm_spin(&mQ[0], &mU[0], &almG_c[0], &almC_c[0], spin, False)
  psht.execute()
  
  return almG,almC
  
  
def ecp_pix2ang(int[:] ipix, int Nrings, int Nphi, double Phi0 = 0):

  cdef long i
  cdef int ring, i_phi
  cdef npx.ndarray[dtype=npx.float64_t,ndim=1] theta, phi
  cdef double pi
  
  pi = np.pi
  
  theta = np.empty(ipix.size, dtype=np.float64)
  phi = np.empty(ipix.size, dtype=np.float64)
  
  for i in range(ipix.size):
    ring = ipix[i] / Nphi;
    i_phi = ipix[i] - ring*Nphi;

    theta[i] = (ring+0.5)*pi/Nrings
    phi[i] = i_phi*2*pi/Nphi + Phi0

  return theta,phi

