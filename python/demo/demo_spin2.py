import flints
import healpy as hp
import numpy as np

LMAX=32

# Read spectrum
cl=[a[:(LMAX+1)].astype(np.float64) for a in hp.read_cl("spectrum_best.fits")]

alm = hp.synalm(cl,lmax=LMAX,new=True)

# Build a map at Nside=64
m_base = hp.alm2map(alm,nside=64,pol=True)

# Extract the theta,phi for a map at Nside=128
theta,phi = hp.pix2ang(128, np.arange(hp.nside2npix(128)))

# Do the interpolation at (theta,phi) (WARNING, the size of array cl indicate the Lmax to consider)
resultQ,resultU,var = flints.healpix.interpolate_map_spin2(m_base[1], m_base[2], theta, phi, cl[1]+cl[2])
#result and var are in the same order as theta, phi
