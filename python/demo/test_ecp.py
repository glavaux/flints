import _ecp_map as em
import healpy as hp
import numpy as np

a= hp.synalm(np.array([0,1,1],dtype=np.double))
a0= hp.synalm(np.array([0,0,0],dtype=np.double))

m = em.ecp_alm2map(a, 128, 128)

m1,m2 = em.ecp_alm2map_spin(a,a0, 2, 128,128)
