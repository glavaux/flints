import flints
import healpy as hp
import numpy as np

# Read spectrum
LMAX=512

# Insure that it is in the proper numpy format, and truncate it to Lmax=128
cl=[a[:(LMAX+1)].astype(np.float64) for a in hp.read_cl("spectrum_best.fits")]
nrings=1024
nphi=1024

almT,almG,almC = hp.synalm(cl,lmax=LMAX,new=True)

# Build a map at Nside=64
m_base = flints.ecp.ecp_alm2map_spin(almG, almC, 2, nrings, nphi)
m_T = flints.ecp.ecp_alm2map(almT, nrings, nphi)

# Extract the theta,phi for a map at Nside=128
theta,phi = hp.pix2ang(512, np.arange(hp.nside2npix(512)))

# Do the interpolation at (theta,phi) (WARNING, the size of array cl indicate the Lmax to consider)
resultQ,resultU,var = flints.ecp.interpolate_map_spin2(m_base[0], m_base[1], nrings, nphi, theta, phi, cl[1]+cl[2])
resultT,varT = flints.ecp.interpolate_map(m_T, nrings, nphi, theta, phi, cl[0])

resultQ = np.array(resultQ)
resultU = np.array(resultU)
mmQ = flints.ecp.ecp_to_2d_array(m_base[0], nrings, nphi)
mmU = flints.ecp.ecp_to_2d_array(m_base[1], nrings, nphi)
#result and var are in the same order as theta, phi
