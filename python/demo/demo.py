from flints.healpix import interpolate_map
import healpy as hp
import numpy as np

# Read spectrum
cl=hp.read_cl("spectrum_best.fits")[0]

# Insure that it is in the proper numpy format, and truncate it to Lmax=128
cl = np.array(cl,dtype=np.float64)[:129]

alm = hp.synalm(cl,lmax=128)

# Build a map at Nside=64
m_base = hp.alm2map(alm,nside=16)

# Extract the theta,phi for a map at Nside=128
theta,phi = hp.pix2ang(128, np.arange(hp.nside2npix(128)))

# Do the interpolation at (theta,phi) (WARNING, the size of array cl indicate the Lmax to consider)
result,var = interpolate_map(m_base, theta, phi, cl)

#result and var are in the same order as theta, phi
