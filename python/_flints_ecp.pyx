from cython.view cimport array as cvarray
cimport cython
cimport numpy as npx
import numpy as np
import healpy as hp

__all__=["interpolate_map","interpolate_spin2"]

cdef extern from "xcomplex.h":
  cdef cppclass xcomplex[T]:
    xcomplex() nogil
    xcomplex(const T&, const T&)

cdef extern from "ecp_map.hpp":
  cdef cppclass ECP_Map[T]:
    ECP_Map()
    void Set(int,int)
    T& operator[](long)

cdef extern from "pointing.h":
   cdef cppclass pointing:
     pointing()
     double theta, phi

cdef extern from "arr.h":
   cdef cppclass arr[T]:
     arr()
     void alloc(long)
     T& operator[](long)

cdef extern from "qbinterpol_ecp.hpp" namespace "CMB":
    cdef cppclass QuickInterpolateMap_ECP[T,T2]:
      QuickInterpolateMap_ECP(const arr[T2]&,
                          int, int) nogil
      void interpolateMany(const ECP_Map[T]&, const pointing *, T *, T *, int)  nogil

cdef extern from *:
     ctypedef int mySpinParameter "2"    # a fake type

cdef extern from "polinterpol_ecp.hpp" namespace "CMB":
    cdef cppclass QuickPolInterpolateMap_ECP[T,T2,int]:
      QuickPolInterpolateMap_ECP(const arr[T2]&,
                            int, int) nogil
      void interpolateMany(const ECP_Map[xcomplex[T]]&, const pointing *, T *, xcomplex[T] *, int)  nogil

@cython.boundscheck(False)
cdef _copy_array_to_map(ECP_Map[npx.npy_float64]& hmap, npx.npy_float64[:] in_data):
  cdef long i, N
  N = in_data.size
  for i in range(N):
     hmap[i] = in_data[i]

@cython.boundscheck(False)
cdef _copy_array_reim_to_cmap(ECP_Map[xcomplex[npx.npy_float64]]& hmap, npx.npy_float64[:] in_data_real, npx.npy_float64[:] in_data_imag):
  cdef long i, N
  N = in_data_real.size
  for i in range(N):
     hmap[i] = xcomplex[npx.npy_float64](in_data_real[i], in_data_imag[i])


@cython.boundscheck(False)
cdef _copy_array_to_directions(npx.npy_float64[:] theta,
                              npx.npy_float64[:] phi,
                              arr[pointing]& directions):
  cdef long i, N

  N = theta.size
  for i in range(N):
    directions[i].theta = theta[i]
    directions[i].phi = phi[i]

@cython.boundscheck(False)
cdef _copy_array_to_arr(npx.npy_float64[:] np_a, 
                       arr[npx.npy_float64]& a):
  cdef long i, N

  N = np_a.size
  for i in range(N):
    a[i] = np_a[i]

@cython.boundscheck(False)
cdef _copy_arr_to_array(arr[npx.npy_float64]& a,
                       npx.npy_float64[:] np_a):
  cdef long i, N

  N = np_a.size
  for i in range(N):
    np_a[i] = a[i]


def interpolate_map(npx.npy_float64[:] in_map not None, int Nrings, int Nphi,
                    npx.npy_float64[:] theta not None,
                    npx.npy_float64[:] phi not None,
                    npx.npy_float64[:] cls not None):
   """
   interpolate_map(in_map,theta,phi,cls) uses flints to perform the optimal interpolation
   at the indicated position (theta,phi). The angular power spectrum must be specified using cls.
   The function return a tuple with the result of the interpolation and the expected standard deviation.
"""

   cdef ECP_Map[npx.npy_float64] hmap
   cdef arr[pointing] directions
   cdef long N
   cdef arr[npx.npy_float64] power_cls
   cdef QuickInterpolateMap_ECP[npx.npy_float64, npx.npy_float64] *qim
   cdef npx.npy_float64[:] result_out, var_out

   N = theta.size
   if phi.size != N:
     raise ValueError("theta.size != phi.size")

   hmap.Set(Nrings, Nphi)
   _copy_array_to_map(hmap, in_map)

   directions.alloc(theta.size)
   _copy_array_to_directions(theta, phi, directions)

   power_cls.alloc(cls.size)
   _copy_array_to_arr(cls, power_cls)   

   qim = new QuickInterpolateMap_ECP[npx.npy_float64, npx.npy_float64](power_cls, Nrings, Nphi);

   result_out = cvarray(shape=(N,), itemsize=sizeof(npx.npy_float64), format="d")
   var_out = cvarray(shape=(N,), itemsize=sizeof(npx.npy_float64), format="d")

   qim.interpolateMany(hmap, &directions[0], &var_out[0], &result_out[0], N)

   del qim

   return result_out,var_out
   
   
   
def interpolate_map_spin2(npx.npy_float64[:] in_map_real not None,
                     npx.npy_float64[:] in_map_imag not None,
                     int Nrings, int Nphi,
                     npx.npy_float64[:] theta not None,
                     npx.npy_float64[:] phi not None,
                     npx.npy_float64[:] cls not None):
   """
   interpolate_spin2(in_map_real,in_map_imag,theta,phi,cls) uses flints to perform the optimal interpolation of the complex 
   spin2 field P = in_map_real + I * in_map_imag at the indicated position (theta,phi). 
   The angular power spectrum of the spin-2 field (<P P^*>) must be specified using the cls.
   The function return a memoryview tuple with the result of the interpolation and the expected standard deviation.
   NOTA: in_map_real and in_map_imag are respectively  Q and U for polarization measurement.
"""

   cdef ECP_Map[xcomplex[npx.npy_float64]] hmap
   cdef arr[pointing] directions
   cdef long N
   cdef arr[npx.npy_float64] power_cls
   cdef QuickPolInterpolateMap_ECP[npx.npy_float64, npx.npy_float64, mySpinParameter] *qim
   cdef npx.npy_float64[:,::1] result_out
   cdef npx.npy_float64[:] var_out
   cdef xcomplex[npx.npy_float64] *forced_out

   N = theta.size
   if phi.size != N:
     raise ValueError("theta.size != phi.size")

   if in_map_real.size != in_map_imag.size:
     raise ValueError("in_map_real.size != in_map_imag.size")

   hmap.Set(Nrings, Nphi)
   _copy_array_reim_to_cmap(hmap, in_map_real, in_map_imag)

   directions.alloc(theta.size)
   _copy_array_to_directions(theta, phi, directions)

   power_cls.alloc(cls.size)
   _copy_array_to_arr(cls, power_cls)   

   qim = new QuickPolInterpolateMap_ECP[npx.npy_float64, npx.npy_float64, mySpinParameter](power_cls, Nrings, Nphi);

   result_out = cvarray(shape=(N,2), itemsize=sizeof(npx.npy_float64), format="d")
   var_out = cvarray(shape=(N,), itemsize=sizeof(npx.npy_float64), format="d")

   forced_out = <xcomplex[npx.npy_float64] *>(&result_out[0,0])

   qim.interpolateMany(hmap, &directions[0], &var_out[0], forced_out, N)

   del qim

   return result_out[:,0],result_out[:,1],var_out
