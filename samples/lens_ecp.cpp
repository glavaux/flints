/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./samples/lens_ecp.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <alm.h>
#include <xcomplex.h>
#include <cstring>
#include <string>
#include <boost/format.hpp>
#include <iostream>
#include <planck_rng.h>
#include <powspec.h>
#include <powspec_fitsio.h>
#include <alm_healpix_tools.h>
#include <alm_powspec_tools.h>
#include <alm_fitsio.h>
#include <fitshandle.h>
#include "multi_healpix.hpp"
#include "lens_ecp_opt_gen.h"
#include "lensing_operator_ecp_flints.hpp"
#include "ecp_transform.hpp"
#include "ecp_fitsio.hpp"

using boost::format;
using boost::str;
using namespace std;
using namespace CMB;

int checkParameters(struct lens_args_info& info)
{
  if (!info.primaryTemp_given && !info.primaryPol_given)
    {
      cerr << "I do not know what to lens. Stop here." << endl;
      return 1;
    }

  if ((info.primaryTemp_given && !info.outputTemp_given) ||
      ((info.primaryPol_given) && !info.outputPol_given))
    {
      cerr << "Inconsistent input/output information. Stop here." << endl;
      return 1;
    }

  if (info.primaryTemp_given && info.primaryPol_given)
    {
      cerr << "Polarization implies temperature. You cannot specify a primary indepedent temperature at the same time." << endl;
      return 1;
    }


  if (info.primaryPol_given && (!info.outputPol_given && !info.computeSpectrum_given))
    {
      cerr << "Strange. You ask for polarization but do not ask to get the output." << endl;
    }

  if (info.primaryTemp_given && (!info.outputTemp_given && !info.outputPol_given && !info.computeSpectrum_given))
    {
      cerr << "Strange. You ask for temperature but do not ask to get the output." << endl;
    }



  return 0;
}

class Lenser
{
public:
  planck_rng rng;
  Healpix_Map<double> lT;
  ECP_Map<double> T, Q, U;
  Healpix_Map<double> lQ, lU;
  Alm<xcomplex<double> > almT, almE, almB, almET, almPhi;
  bool doTemperature, doPolarization;
  PowSpec powspec_tt, powspec_TEB;
  long lmax_spec;
  int Nrings, Nphi;
  LensingOperatorFlints_ECP_to_Healpix_Scalar<double,double> *scalar;
  LensingOperatorFlints_ECP_to_Healpix_Polarization<double,double> *polar;
  struct lens_args_info *pinfo;
  string out_alms, out_spectrum, out_primary, out_temp, out_pol;
    
  Lenser(struct lens_args_info& info);
  ~Lenser();
  
  void initLensingPotential();

  void loadSpectrumTemperature();
  void loadSpectrumPolarization();

  void initTemperature();
  void initPolarization();

  void generateTemperature();
  void generatePolarization();

  void lensTemperature();
  void lensPolarization();
  
  void computeSpectrum(); 
};

Lenser::Lenser(struct lens_args_info& info)
  : scalar(0), polar(0), pinfo(&info), doTemperature(false), doPolarization(false)
{
  if (info.seed_given)
    rng.seed(info.seed_arg);

  almT.Set(info.Lmax_arg, info.Lmax_arg);
  almE.Set(info.Lmax_arg, info.Lmax_arg);
  almB.Set(info.Lmax_arg, info.Lmax_arg);
  almET.Set(info.Lmax_arg, info.Lmax_arg);

  lmax_spec = info.Lmax_arg;

  {
    istringstream iss(info.ecp_res_arg);
    iss >> Nrings >> Nphi;
    if (!iss)
      {
        cerr << "No numbers in ECP resolution specification" << endl;
        exit(1);
      }
  }

}
 
Lenser::~Lenser()
{
  if (scalar != 0)
    delete scalar;
  if (polar != 0)
    delete polar;
}

void Lenser::loadSpectrumTemperature()
{
  if (!pinfo->specTemp_given)
    {
      cerr << "We need the temperature spectrum to be able to lens correctly." << endl;
      exit(1);
    }
  
  read_powspec_from_fits(pinfo->specTemp_arg, powspec_tt, 1, pinfo->Lmax_arg);
  
  T.Set(Nrings, Nphi);
  lT.SetNside(pinfo->NsideOut_arg, RING);
  
  doTemperature = true;
}

void Lenser::loadSpectrumPolarization()
{
  if (!pinfo->specPol_given)
    {
      cerr << "We need the polarization spectrum to be able to lens correctly." << endl;
      exit(1);
    }

  read_powspec_from_fits(pinfo->specPol_arg, powspec_TEB, 4, pinfo->Lmax_arg);
  {
    arr<double> tmp_tt = powspec_TEB.tt();
    powspec_tt.Set(tmp_tt);
  }
  
  Q.Set(Nrings, Nphi);
  U.Set(Nrings, Nphi);
  lQ.SetNside(pinfo->NsideOut_arg, RING);
  lU.SetNside(pinfo->NsideOut_arg, RING);
  T.Set(Nrings, Nphi);
  lT.SetNside(pinfo->NsideOut_arg, RING);
  
  doTemperature = doPolarization = true;
}

void Lenser::initTemperature()
{
  if (doTemperature)
    {
      scalar = new LensingOperatorFlints_ECP_to_Healpix_Scalar<double,double>(Nrings, Nphi, pinfo->NsideOut_arg);
      scalar->update_CMB_spectrum(powspec_tt.tt());
    }
}

void Lenser::initPolarization()
{
  if (doPolarization)
    {
      polar = new LensingOperatorFlints_ECP_to_Healpix_Polarization<double,double>(Nrings, Nphi, pinfo->NsideOut_arg);
      polar->update_CMB_spectrum(powspec_TEB.gg());
    }
}

void Lenser::initLensingPotential()
{
  // Read lensing potential power spectra, if given. And generate random potential.
  // If not given, attempt to read it from "potential_arg".
  if (pinfo->specPotential_given)
    {
      PowSpec potential_spec;

      almPhi.Set(pinfo->LmaxPhi_arg, pinfo->LmaxPhi_arg);
      read_powspec_from_fits(pinfo->specPotential_arg, potential_spec, 1, pinfo->LmaxPhi_arg);
      create_alm(potential_spec, almPhi, rng);

      if (pinfo->savePotential_given)
	{
	  write_Alm_to_fits(pinfo->savePotential_arg, almPhi, pinfo->LmaxPhi_arg, pinfo->LmaxPhi_arg, planckType<double>());
	}
    }
  else
    {
      if (!pinfo->potential_given)
	{
	  cerr << "You must specify either a lensing potential spectrum, or alms of the lensing potential" << endl;
	  exit(1);
	}
      if (string(pinfo->potential_arg) != "zero")
        read_Alm_from_fits(pinfo->potential_arg, almPhi, pinfo->LmaxPhi_arg, pinfo->LmaxPhi_arg);
      else
        {
          almPhi.Set(pinfo->LmaxPhi_arg, pinfo->LmaxPhi_arg);
          almPhi.SetToZero();
        }

      if (pinfo->fudgePhi_given)
        {
          for (int l = 0; l <= pinfo->LmaxPhi_arg; l++)
            for (int m = 0; m <= l; m++)
              almPhi(l,m) *= pinfo->fudgePhi_arg;
        }
    }
}

void Lenser::generateTemperature()
{
  if (!doTemperature || doPolarization)
    return;

  // Generate random fluctuations
  if (strcmp(pinfo->primaryTemp_arg, "random") == 0)
    create_alm (powspec_tt, almT, rng);
  else
    read_Alm_from_fits(pinfo->primaryTemp_arg, almT, pinfo->Lmax_arg, pinfo->Lmax_arg);
    
  fitshandle h;
  h.create(string("!") + out_alms);
  write_Alm_to_fits(h, almT, pinfo->Lmax_arg, pinfo->Lmax_arg, planckType<double>());
  
  ecp_alm2map(almT, T);
}

void Lenser::generatePolarization()
{
  if (!doPolarization)
    return;

  if (strcmp(pinfo->primaryPol_arg, "random") == 0)
    {
      create_alm_pol(powspec_TEB, almT, almE, almB, rng);	  
    }
  else
    {
      if (pinfo->planckPol_flag)
        {
          istringstream iss(pinfo->primaryPol_arg);
          string Talm, Ealm, Balm;
          iss >> Talm >> Ealm >> Balm;
          read_Alm_from_fits(Talm, almT, pinfo->Lmax_arg, pinfo->Lmax_arg, 2);
          read_Alm_from_fits(Ealm, almE, pinfo->Lmax_arg, pinfo->Lmax_arg, 2);
          read_Alm_from_fits(Balm, almB, pinfo->Lmax_arg, pinfo->Lmax_arg, 2);
        }
      else
        {
          read_Alm_from_fits(pinfo->primaryPol_arg, almT, pinfo->Lmax_arg, pinfo->Lmax_arg, 2);
          read_Alm_from_fits(pinfo->primaryPol_arg, almE, pinfo->Lmax_arg, pinfo->Lmax_arg, 3);
          read_Alm_from_fits(pinfo->primaryPol_arg, almB, pinfo->Lmax_arg, pinfo->Lmax_arg, 4);
        }
    }

  write_Alm_to_fits(string("!") + out_alms, almT, almE, almB, 
                    pinfo->Lmax_arg, pinfo->Lmax_arg, planckType<double>());
  
  ecp_alm2map_pol(almT, almE, almB, T, Q, U);
}

void Lenser::lensTemperature()
{
  if (!doTemperature)
    return;
  
  scalar->setLensingField(almPhi);
  scalar->applyLensing(T, lT);
  
  if (!out_temp.empty() && out_pol.empty())
    {
      fitshandle h;

      cout << "Writing temperature to " << out_temp << endl;
      h.create(string("!") + out_temp);
      write_Healpix_map_to_fits(h, lT, FLINTS_convert_to_fitsio(planckType<double>()));
    }
  
  fitshandle hf;
  hf.create("!failure_map.fits");
  write_Healpix_map_to_fits(hf, scalar->failure_map, FLINTS_convert_to_fitsio(planckType<float>()));

  if (!out_primary.empty() && !doPolarization)
    {
      fitshandle h;
      
      cout << "Writing primary temperature to " << out_primary << endl;
      h.create(string("!") + string(out_primary));
      write_ecp_map(h, T, FLINTS_convert_to_fitsio(planckType<double>()));
    }
  
}

void Lenser::lensPolarization()
{
  if (!doPolarization)
    return;

  polar->setLensingField(almPhi);
  polar->applyLensing(Q, U, lQ, lU);      
  
  if (!out_pol.empty())
    {
      fitshandle h;

      cout << "Writing lensed temperature+polarization to " << out_pol << endl;
      h.create(string("!") + string(out_pol));
      write_Healpix_map_to_fits(h, lT, lQ, lU, FLINTS_convert_to_fitsio(planckType<double>()));
    }

  fitshandle hf;
  hf.create("!failure_map_pol.fits");
  write_Healpix_map_to_fits(hf, scalar->failure_map, FLINTS_convert_to_fitsio(planckType<float>()));
  
  if (!out_primary.empty())
    {
      fitshandle h;
      
      cout << "Writing primary temperature+polarization to " << out_primary << endl;
      h.create(string("!") + out_primary);
      write_ecp_map(h, T, Q, U, FLINTS_convert_to_fitsio(planckType<double>()));
    }
}

void Lenser::computeSpectrum()
{
  if (out_spectrum.empty())
    return;

  if (!doPolarization)
    {
      Alm<xcomplex<double> > alm_lensed(lmax_spec, lmax_spec);
      PowSpec powspec(lmax_spec, 1);
      arr<double> alm_weights(2*lT.Nside());
      
      alm_weights.fill(1);
      
      map2alm_iter(lT, alm_lensed, pinfo->iterAlm_arg, alm_weights);
      
      extract_powspec(alm_lensed, powspec);	  
      
      fitshandle out;
      out.create(string("!") + out_spectrum);
      write_powspec_to_fits (out, powspec, 1);
    }
  else
    {
      Alm<xcomplex<double> > 
        almT_lensed(lmax_spec, lmax_spec),
        almE_lensed(lmax_spec, lmax_spec),
        almB_lensed(lmax_spec, lmax_spec);
      PowSpec powspec(4, lmax_spec);
      arr<double> alm_weights(2*lT.Nside());
      
      alm_weights.fill(1.0);
      
      cout << "Computing lensed alms..." << endl;
      map2alm_pol_iter(lT, lQ, lU, almT_lensed, almE_lensed, almB_lensed, 
                       pinfo->iterAlm_arg, alm_weights);
      
      cout << "Extracting and writing spectra to " << pinfo->computeSpectrum_arg << endl;
      extract_powspec(almT_lensed, almE_lensed, almB_lensed, powspec);
      
      fitshandle out;
      out.create(string("!") + out_spectrum);
      write_powspec_to_fits (out, powspec, 4);
    }
}


int main(int argc, char **argv)
{
  struct lens_args_info info;
  struct lens_parse_arg_params argparams;

  lens_parse_arg_init(&info);
  lens_parse_arg_params_init(&argparams);

  argparams.check_required = 0;
  if (lens_parse_arg_ext(argc, argv, &info, &argparams))
    return -1;

  if (info.configfile_given)
    {
      argparams.override = 0;
      argparams.initialize = 0;
      argparams.check_required = 1;
      if (lens_parse_arg_config_file(info.configfile_arg, &info, &argparams))
        {
          lens_parse_arg_free(&info);
          return -1;
        }
    }
  else if (lens_parse_arg_required(&info, argv[0]))
    return -1;

  cout
    << "LENS-FLINTS-ECP  Copyright (C) 2010-2013  Guilhem Lavaux." << endl
    << "This program comes with ABSOLUTELY NO WARRANTY" << endl
    << "This is free software, and you are welcome to redistribute it" << endl
    << "under certain conditions." << endl;


  // Sanity checking of the parameters
  if (checkParameters(info))
    return 1;

  Lenser *lenser = new Lenser(info);

  lenser->initLensingPotential();

  // First the temperature, if required
  if (info.primaryTemp_given)
    lenser->loadSpectrumTemperature();

  // Second the polarization, if required
  if (info.primaryPol_given)
    lenser->loadSpectrumPolarization();

  lenser->initTemperature();
  lenser->initPolarization();

  int batch_nr = 0;
  
  // +++++++++++++++++++++++++++++++++++++++++++++= GENERATE ALMS +++++++++++++++++++++++

  do
    {
      if (info.batch_given)
        {
          lenser->out_alms = str(format("primary_alm_%d.fits") % batch_nr);
          if (info.outputPrimary_given)
            lenser->out_primary = str(format(info.outputPrimary_arg) % batch_nr);
          if (info.outputTemp_given)
            lenser->out_temp = str(format(info.outputTemp_arg) % batch_nr);
          if (info.outputPol_given)
            lenser->out_pol = str(format(info.outputPol_arg) % batch_nr);
          if (info.computeSpectrum_given)
            lenser->out_spectrum = str(format(info.computeSpectrum_arg) % batch_nr);
        }
      else
        {
          lenser->out_alms = "primary_alm.fits";
          if (info.outputTemp_given)
            lenser->out_temp = info.outputTemp_arg;
          if (info.outputPol_given)
            lenser->out_pol = info.outputPol_arg;
          if (info.outputPrimary_given)
            lenser->out_primary = info.outputPrimary_arg;
          if (info.computeSpectrum_given)
            lenser->out_spectrum = info.computeSpectrum_arg;
        }
      
      lenser->generateTemperature();
      lenser->generatePolarization();
      
      lenser->lensTemperature();
      lenser->lensPolarization();
      
      lenser->computeSpectrum();
      
      batch_nr++;
    }
  while (info.batch_given && (batch_nr < info.batch_arg));

  delete lenser;

  return 0;

}
