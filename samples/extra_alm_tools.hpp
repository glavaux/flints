/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./samples/extra_alm_tools.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#ifndef __EXTRA_ALM_TOOLS_HPP
#define __EXTRA_ALM_TOOLS_HPP

#include <cassert>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <iostream>
#include <xcomplex.h>
#include <alm.h>
#include <cmath>
#include "cmb_defs.hpp"

namespace CMB {

  static
  CMB_Map operator-(const CMB_Map& m1, const CMB_Map& m2)
  {
    planck_assert(m1.Nside() == m2.Nside(), 
		  "Map1 and map2 should have the same number of pixels");
    CMB_Map m(m1.Nside(), RING, SET_NSIDE);
    
    for (long i = 0; i < m.Npix(); i++)
      m[i] = m1[i] - m2[i];

    return m;
  }
  
  static
  const ALM_Map& clear(ALM_Map& m)
  {
    m.SetToZero();
    return m;
  }
  
  static 
  ALM_Map operator+(const ALM_Map& alm1, const ALM_Map& alm2)
  {
    planck_assert(alm1.Lmax() == alm2.Lmax(), "Lmax different in alm1 and alm2");
    planck_assert(alm1.Mmax() == alm2.Mmax(), "Mmax different in alm1 and alm2");
    ALM_Map m1(alm1.Lmax(), alm1.Mmax());
    
    for (long m = 0; m <= alm1.Mmax(); m++)
      for (long l = m; l <= alm1.Lmax(); l++)
         m1(l,m) = alm1(l,m) + alm2(l,m);

    return m1;
  }

  static
  ALM_Map operator*(const xcomplex<DataType>& alpha, const ALM_Map& alm1)
  {
    ALM_Map m1 = alm1;
    int lmax = alm1.Lmax(), mmax = alm1.Mmax();
    
    for (int m=0; m<=mmax; ++m)
      for (int l=m; l<=lmax; ++l)
	m1(l,m) *= alpha;
    
    return m1;
  }

  static
  const ALM_Map& operator*=(ALM_Map& alm, const DataType& alpha)
  {
    long mmax = alm.Mmax(), lmax = alm.Lmax();
    for (int m=0; m<=mmax; ++m)
      for (int l=m; l<=lmax; ++l)
	alm(l,m) *= alpha;
    
    return alm;
  }

  static 
  ALM_Map operator-(const ALM_Map& alm1, const ALM_Map& alm2)
  {
    planck_assert(alm1.conformable(alm2), "Alm1 and Alm2 are not conformable");
    
    ALM_Map m1 = alm1;
    int lmax = alm1.Lmax(), mmax = alm1.Mmax();
    
    for (int m=0; m<=mmax; ++m)
      for (int l=m; l<=lmax; ++l)
	m1(l,m) = m1(l,m)-alm2(l,m);
    
    return m1;
  }

  static
  ALM_Map operator-(const ALM_Map& alm1)
  {
    int lmax = alm1.Lmax(), mmax = alm1.Mmax();
    ALM_Map m1(lmax, mmax);

    for (int m=0; m<=mmax; ++m)
      for (int l=m; l<=lmax; ++l)
        m1(l,m) = -alm1(l,m);

    return m1;
  }

  
  static
  const ALM_Map &operator+=(ALM_Map& m, const ALM_Map& m2)
  {
    m.Add(m2);
    return m;
  }

  static
  const ALM_Map &operator-=(ALM_Map& m1, const ALM_Map& m2)
  {
    planck_assert(m1.conformable(m2), "Alm1 and Alm2 are not conformable");
    int lmax = m1.Lmax(), mmax = m1.Mmax();
    
    for (int m=0; m<=mmax; ++m)
      for (int l=m; l<=lmax; ++l)
	m1(l,m) = m1(l,m)-m2(l,m);
    
    return m1;
  }

  template<typename T>
  xcomplex<T> dot_product(const Alm<xcomplex<T> >& alm1, const Alm<xcomplex<T> >& alm2)
  {
    double scalar = 0;
    
    planck_assert(alm1.conformable(alm2), "alm1 and alm2 not conformables");
    int lmax = alm1.Lmax(), mmax = alm1.Mmax();

    for (int l=0; l<=lmax; ++l)
      {
	scalar += (alm1(l,0).re * alm2(l,0).re);
      }

    for (int m=1; m<=mmax; ++m)
      {
	for (int l=m; l<=lmax; ++l)
	  {
	    const xcomplex<T>& a1 = alm1(l,m), a2 = alm2(l,m);
	    scalar += 2*(a1.re*a2.re + a1.im*a2.im);
	  }
      }
    
    return xcomplex<T>(scalar,0);
  }

  static
  xcomplex<DataType> operator/(const xcomplex<DataType>& c1, const xcomplex<DataType>& c2)
  {
    return c1*c2.conj()*(1/c2.norm());
  }
  
  static
  const xcomplex<DataType>& operator/=(xcomplex<DataType>& c1, const xcomplex<DataType>& c2)
  {
    return c1*=c2.conj()*(1/c2.norm());
  }

  static
  DataType norm_L2(const ALM_Map& alms)
  {
    return dot_product(alms,alms).real();
  }

  /** A reimplementation of Healpix create_alm using GSL's random 
   * generators.
   */

  template<typename T> 
  void create_rand_alm(gsl_rng *rng,
		       const arr<T> &powspec, Alm<xcomplex<T> > &alm)
  {
    int lmax = alm.Lmax();
    int mmax = alm.Mmax();
    const double hsqrt2 = 1/sqrt(2.);
    
    assert(powspec.size() > lmax);
    for (int l=0; l<=lmax; ++l)
      {
	double rms_tt = sqrt(powspec[l]);
	alm(l,0).Set(gsl_ran_gaussian_ziggurat(rng, rms_tt), 0);

	rms_tt *= hsqrt2;
	for (int m=1; m<=std::min(l,mmax); ++m)
	  {
	    double zr = gsl_ran_gaussian_ziggurat(rng, rms_tt);
	    double zi = gsl_ran_gaussian_ziggurat(rng, rms_tt);

	    alm(l,m).Set(zr, zi);
	  }
      }
  }

};

#endif
