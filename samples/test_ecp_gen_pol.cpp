/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./samples/test_ecp_gen_pol.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <cstdlib>
#include <cmath>
#include <gsl/gsl_rng.h>
#include <powspec.h>
#include <alm_powspec_tools.h>
#include <powspec_fitsio.h>
#include <fitshandle.h>
#include <healpix_map_fitsio.h>
#include <alm.h>
#include <alm_fitsio.h>
#include <alm_healpix_tools.h>
#include <planck_rng.h>
#include "ecp_map.hpp"
#include "extra_map_tools.hpp"
#include "psht_cxx.h"
#include "ecp_transform.hpp"
#include "ecp_fitsio.hpp"
#include <healpix_map.h>
#include "polinterpol_ecp.hpp"

using namespace std;
using namespace CMB;

#define NSIDE_REF 512

template<typename T>
void test_generation(Alm<xcomplex<T> >& almsG, Alm<xcomplex<T> >& almsC,
                     ECP_Map<T>& mQ, ECP_Map<T>& mU)
{
  Healpix_Map<double> mQ_H(NSIDE_REF, RING, SET_NSIDE), mU_H(NSIDE_REF, RING, SET_NSIDE);

  ecp_alm2map_pol(almsG, almsC, mQ, mU);
  
  fitshandle h;
  h.create("!test_ecp_Q.fits");
  write_ecp_map(h, mQ, planckType<double>());

  h.create("!test_ecp_U.fits");
  write_ecp_map(h, mU, planckType<double>());

  alm2map_spin(almsG, almsC, mQ_H, mU_H, 2);
  h.create("!test_healpix_Q.fits");
  write_Healpix_map_to_fits(h, mQ_H, planckType<double>());

  h.create("!test_healpix_U.fits");
  write_Healpix_map_to_fits(h, mU_H, planckType<double>());
}

template<typename T>
void test_interpolation(ECP_Map<T>& mQ, ECP_Map<T>& mU, const arr<T>& spec)
{
  typedef xcomplex<T> cplx;

  Healpix_Map<cplx > out_m(NSIDE_REF, RING, SET_NSIDE);
  Healpix_Map<T> out_mvar(NSIDE_REF, RING, SET_NSIDE); 
  QuickPolInterpolateMap_ECP<T> interpol(spec, mQ.Nrings(), mQ.Nphi(), 0);
  arr<pointing> ptgs;
  ECP_Map<cplx> mP(mQ.Nrings(), mQ.Nphi());

  ptgs.alloc(out_m.Npix());
  for (long p = 0; p < out_m.Npix(); p++)
    {
      ptgs[p] = out_m.pix2vec(p);
    }
  for (long p = 0; p < mP.Npix(); p++)
    {
      mP[p] = cplx(mQ[p], mU[p]);
    }
  
  interpol.interpolateMany(mP, &ptgs[0], &out_mvar[0], &out_m[0], ptgs.size());

  fitshandle h_result_Q, h_result_U, h_var;
  Healpix_Map<T> tmp_map(NSIDE_REF, RING, SET_NSIDE);
  h_result_Q.create("!result_interpol_Q.fits");
  for (long p = 0; p < out_m.Npix(); p++)
    tmp_map[p] = out_m[p].real();
  write_Healpix_map_to_fits(h_result_Q, tmp_map, planckType<double>());

  h_result_U.create("!result_interpol_U.fits");
  for (long p = 0; p < out_m.Npix(); p++)
    tmp_map[p] = out_m[p].imag();
  write_Healpix_map_to_fits(h_result_U, tmp_map, planckType<double>());

  h_var.create("!result_rms.fits");
  write_Healpix_map_to_fits(h_var, out_mvar, planckType<double>());
}

int main()
{
  int lmaxCMB = NSIDE_REF*3;
  int Nrings = NSIDE_REF, Nphi = NSIDE_REF;
  PowSpec cmbspec;
  planck_rng rng;
  Alm<xcomplex<double> > almCMB_G(lmaxCMB, lmaxCMB), almCMB_C(lmaxCMB, lmaxCMB);
  ECP_Map<double> mQ(Nrings, Nphi), mU(Nrings, Nphi);
  
  read_powspec_from_fits("spectrum_phi.fits", cmbspec, 1, lmaxCMB);

  create_alm(cmbspec, almCMB_G, rng);
  almCMB_C.SetToZero();

  write_Alm_to_fits("!ecp_alm.fits", almCMB_G, almCMB_G.Lmax(), almCMB_G.Mmax(), planckType<double>());   
  
  test_generation(almCMB_G, almCMB_C, mQ, mU);
  test_interpolation<double>(mQ, mU, cmbspec.tt());
  
  return 0;
}

