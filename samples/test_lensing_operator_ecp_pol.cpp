/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./samples/test_lensing_operator_ecp_pol.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <cstdlib>
#include <cmath>
#include <gsl/gsl_rng.h>
#include <powspec.h>
#include <alm_powspec_tools.h>
#include <powspec_fitsio.h>
#include <fitshandle.h>
#include <healpix_map_fitsio.h>
#include <alm.h>
#include <alm_fitsio.h>
#include <alm_healpix_tools.h>
#include <planck_rng.h>
#include "extra_map_tools.hpp"
#include "lensing_operator_ecp_flints.hpp"
#include "ecp_transform.hpp"
#include "ecp_map.hpp"
#include "ecp_fitsio.hpp"

using namespace std;
using namespace CMB;

template<typename T>
Healpix_Map<xcomplex<T> > merge(const Healpix_Map<T>& Q, const Healpix_Map<T>& U)
{
  Healpix_Map<xcomplex<T> > P;

  P.SetNside(Q.Nside(), RING);
  for (long i = 0; i < P.Npix(); i++)
    P[i] = xcomplex<T>(Q[i], U[i]);
  return P;
}

template<typename T>
ECP_Map<xcomplex<T> > merge(const ECP_Map<T>& Q, const ECP_Map<T>& U)
{
  ECP_Map<xcomplex<T> > P;

  P.Set(Q.Nrings(), Q.Nphi());
  for (long i = 0; i < P.Npix(); i++)
    P[i] = xcomplex<T>(Q[i], U[i]);
  return P;
}

template<typename T>
void split(const ECP_Map<xcomplex<T> >& P, ECP_Map<T>& Q, ECP_Map<T>& U)
{
  Q.Set(P.Nrings(), P.Nphi());
  U.Set(P.Nrings(), P.Nphi());

  for (long i = 0; i < P.Npix(); i++)
    {
      Q[i] = P[i].real();
      U[i] = P[i].imag();
    }
}

template<typename T>
void split(const Healpix_Map<xcomplex<T> >& P, Healpix_Map<T>& Q, Healpix_Map<T>& U)
{
  Q.SetNside(P.Nside(), RING);
  U.SetNside(P.Nside(), RING);

  for (long i = 0; i < P.Npix(); i++)
    {
      Q[i] = P[i].real();
      U[i] = P[i].imag();
    }
}



int main()
{
  int Nrings = 512, Nphi = 256, NsideOut = 256;
  int lmaxCMB = 128, lmaxPhi = 128;
  PowSpec cmbspec, phispec;
  ECP_Map<double> inQ, inU, tran_lensed_CMB_Q, tran_lensed_CMB_U;
  Healpix_Map<xcomplex<double> > lensed_Pol;
  ECP_Map<xcomplex<double> > Pol, tran_lensed_Pol;
  Healpix_Map<double> lensed_CMB_Q, lensed_CMB_U;
  LensingOperatorFlints_ECP_to_Healpix_Polarization<double,double> *lo;
  Alm<xcomplex<double > > lo_pot(lmaxPhi, lmaxPhi);
  planck_rng rng;
  Alm<xcomplex<double> > almT(lmaxCMB, lmaxCMB), 
    almE(lmaxCMB, lmaxCMB), almB(lmaxCMB, lmaxCMB);

  read_powspec_from_fits("spectrum.fits", cmbspec, 4, lmaxCMB);
  read_powspec_from_fits("spectrum_phi.fits", phispec, 1, lmaxPhi);

  create_alm_pol(cmbspec, almT, almE, almB, rng);
  create_alm(phispec, lo_pot, rng);
  inQ.Set(Nrings, Nphi);
  inU.Set(Nrings, Nphi);
  ecp_alm2map_pol(almE, almB, inQ, inU);

  lo = new LensingOperatorFlints_ECP_to_Healpix_Polarization<double,double>(Nrings, Nphi, NsideOut);
  lo->update_CMB_spectrum(cmbspec.tt());
  lo->setLensingField(lo_pot);

  Pol = merge(inQ, inU);
  lensed_Pol.SetNside(NsideOut, RING);
  tran_lensed_Pol.Set(Nrings, Nphi);

  lo->applyLensing(Pol, lensed_Pol);

  fitshandle f;
  f.create("!failure_map.fits");
  write_Healpix_map_to_fits(f, lo->failure_map, planckType<float>());
  lo->applyLensingTransposed(lensed_Pol, tran_lensed_Pol);

  split(lensed_Pol, lensed_CMB_Q, lensed_CMB_U);
  split(tran_lensed_Pol, tran_lensed_CMB_Q, tran_lensed_CMB_U);


  f.create("!cmb_alms_E.fits");
  write_Alm_to_fits(f, almE, almE.Lmax(), almE.Mmax(), planckType<double>());

  f.create("!lensed_cmb_Q.fits");
  write_Healpix_map_to_fits(f, lensed_CMB_Q, planckType<double>());

  f.create("!lensed_cmb_U.fits");
  write_Healpix_map_to_fits(f, lensed_CMB_U, planckType<double>());

  f.create("!tran_lensed_cmb_Q.fits");
  write_ecp_map(f, tran_lensed_CMB_Q, planckType<double>());

  f.create("!tran_lensed_cmb_U.fits");
  write_ecp_map(f, tran_lensed_CMB_U, planckType<double>());

  f.create("!original_cmb_Q.fits");
  write_ecp_map(f, inQ, planckType<double>());

  f.create("!original_cmb_U.fits");
  write_ecp_map(f, inU, planckType<double>());

  xcomplex<double> A = dot_product(lensed_Pol, lensed_Pol).real();
  xcomplex<double>  B = tran_lensed_Pol.dot(Pol).real();
  
  cout << A << " " << B << " " << A-B << " " << (A-B)/sqrt(A.norm()) << endl;

  delete lo;

  return 0;
}

