/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./samples/test_lensing_operator.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <cstdlib>
#include <cmath>
#include <gsl/gsl_rng.h>
#include <powspec.h>
#include <alm_powspec_tools.h>
#include <powspec_fitsio.h>
#include <fitshandle.h>
#include <healpix_map_fitsio.h>
#include <alm.h>
#include <alm_fitsio.h>
#include <alm_healpix_tools.h>
#include <planck_rng.h>
#include "extra_map_tools.hpp"

#include "lensing_operator_flints.hpp"

using namespace std;
using namespace CMB;



int main()
{
  int lmaxCMB = 64, lmaxPhi = 64;
  PowSpec cmbspec, phispec;
  Healpix_Map<double> inCMB, lensed_CMB, tran_lensed_CMB;
  LensingOperatorFlints_Scalar<double,double> *lo;
  Alm<xcomplex<double > > lo_pot(lmaxPhi, lmaxPhi);
  planck_rng rng;
  Alm<xcomplex<double> > almCMB(lmaxCMB, lmaxCMB);

  read_powspec_from_fits("spectrum.fits", cmbspec, 1, lmaxCMB);
  read_powspec_from_fits("spectrum_phi.fits", phispec, 1, lmaxPhi);

  create_alm(cmbspec, almCMB, rng);
  create_alm(phispec, lo_pot, rng);
  inCMB.SetNside(128, RING);
  alm2map(almCMB, inCMB);

  lensed_CMB.SetNside(inCMB.Nside(), RING);
  tran_lensed_CMB.SetNside(inCMB.Nside(), RING);
 
  lo = new LensingOperatorFlints_Scalar<double,double>(inCMB.Nside());
  lo->update_CMB_spectrum(cmbspec.tt());
  lo->setLensingField(lo_pot);
  lo->applyLensing<true>(inCMB, lensed_CMB);
  lo->applyLensing<false>(lensed_CMB, tran_lensed_CMB);

  double A = dot_product(lensed_CMB, lensed_CMB),
    B = dot_product(inCMB, tran_lensed_CMB);
  
  cout << A << " " << B << " " << A-B << " " << (A-B)/max(A,B) << endl;

  delete lo;

  return 0;
}

