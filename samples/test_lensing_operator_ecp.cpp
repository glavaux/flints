/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./samples/test_lensing_operator_ecp.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <cstdlib>
#include <cmath>
#include <gsl/gsl_rng.h>
#include <powspec.h>
#include <alm_powspec_tools.h>
#include <powspec_fitsio.h>
#include <fitshandle.h>
#include <healpix_map_fitsio.h>
#include <alm.h>
#include <alm_fitsio.h>
#include <alm_healpix_tools.h>
#include <planck_rng.h>
#include "extra_map_tools.hpp"
#include "lensing_operator_ecp_flints.hpp"
#include "ecp_transform.hpp"
#include "ecp_map.hpp"
#include "ecp_fitsio.hpp"

using namespace std;
using namespace CMB;

int main()
{
  int Nrings = 512, Nphi = 1024, NsideOut = 256;
  int lmaxCMB = 64, lmaxPhi = 64;
  PowSpec cmbspec, phispec;
  ECP_Map<double> inCMB, tran_lensed_CMB;
  Healpix_Map<double> lensed_CMB;
  LensingOperatorFlints_ECP_to_Healpix_Scalar<double,double> *lo;
  Alm<xcomplex<double > > lo_pot(lmaxPhi, lmaxPhi);
  planck_rng rng;
  Alm<xcomplex<double> > almCMB(lmaxCMB, lmaxCMB);

  read_powspec_from_fits("spectrum.fits", cmbspec, 1, lmaxCMB);
  read_powspec_from_fits("spectrum_phi.fits", phispec, 1, lmaxPhi);

  create_alm(cmbspec, almCMB, rng);
  create_alm(phispec, lo_pot, rng);
  inCMB.Set(Nrings, Nphi);
  ecp_alm2map(almCMB, inCMB);

  lensed_CMB.SetNside(NsideOut, RING);
  tran_lensed_CMB.Set(Nrings, Nphi);

  fitshandle f;
 
  lo = new LensingOperatorFlints_ECP_to_Healpix_Scalar<double,double>(Nrings, Nphi, NsideOut);
  lo->update_CMB_spectrum(cmbspec.tt());
  lo->setLensingField(lo_pot);
  lo->applyLensing(inCMB, lensed_CMB);
  f.create("!failure_map.fits");
  write_Healpix_map_to_fits(f, lo->failure_map, planckType<float>());
  lo->applyLensingTransposed(lensed_CMB, tran_lensed_CMB);

  f.create("!cmb_alms.fits");
  write_Alm_to_fits(f, almCMB, almCMB.Lmax(), almCMB.Mmax(), planckType<double>());

  f.create("!lensed_cmb.fits");
  write_Healpix_map_to_fits(f, lensed_CMB, planckType<double>());

  f.create("!tran_lensed_cmb.fits");
  write_ecp_map(f, tran_lensed_CMB, planckType<double>());

  f.create("!original_cmb.fits");
  write_ecp_map(f, inCMB, planckType<double>());


  double A = dot_product(lensed_CMB, lensed_CMB),
    B = inCMB.dot(tran_lensed_CMB);
  
  cout << A << " " << B << " " << A-B << " " << (A-B)/max(A,B) << endl;

  delete lo;

  return 0;
}

