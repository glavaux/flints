/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./samples/test_ecp_gen.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <cstdlib>
#include <cmath>
#include <gsl/gsl_rng.h>
#include <powspec.h>
#include <alm_powspec_tools.h>
#include <powspec_fitsio.h>
#include <fitshandle.h>
#include <healpix_map_fitsio.h>
#include <alm.h>
#include <alm_fitsio.h>
#include <alm_healpix_tools.h>
#include <planck_rng.h>
#include "ecp_map.hpp"
#include "extra_map_tools.hpp"
#include "psht_cxx.h"
#include "ecp_transform.hpp"
#include "ecp_fitsio.hpp"
#include <healpix_map.h>
#include "qbinterpol_ecp.hpp"

using namespace std;
using namespace CMB;

#define NSIDE_REF 256

void test_generation(Alm<xcomplex<double> >& alms, ECP_Map<double>& m)
{
  ecp_alm2map(alms, m);
  
  fitshandle h;
  h.create("!test_ecp.fits");
  write_ecp_map(h, m, planckType<double>());
}

void test_interpolation(ECP_Map<double>& m, const arr<double>& spec)
{
  Healpix_Map<double> out_m(NSIDE_REF, RING, SET_NSIDE), out_mvar(NSIDE_REF, RING, SET_NSIDE); 
  QuickInterpolateMap_ECP<double> interpol(spec, m.Nrings(), m.Nphi(), 0);
  arr<pointing> ptgs;
  gsl_rng *rng = gsl_rng_alloc(gsl_rng_default);

  ptgs.alloc(out_m.Npix());
  for (long p = 0; p < out_m.Npix(); p++)
    ptgs[p] = out_m.pix2vec(p);

  interpol.interpolateMany(m, &ptgs[0], &out_mvar[0], &out_m[0], ptgs.size());
  for (long i = 0; i < out_m.Npix(); i++)
    out_m[i] += (out_mvar[i] > 0) ? gsl_ran_gaussian(rng, out_mvar[i]) : 0;

  fitshandle h_result, h_var;
  h_result.create("!result_interpol.fits");
  h_var.create("!result_rms.fits");
  write_Healpix_map_to_fits(h_result, out_m, planckType<double>());
  write_Healpix_map_to_fits(h_var, out_mvar, planckType<double>());
  gsl_rng_free(rng);
}

int main()
{
  int lmaxCMB = 1024;
  int Nrings = 1024, Nphi = 2048;
  PowSpec cmbspec;
  planck_rng rng;
  Alm<xcomplex<double> > almCMB(lmaxCMB, lmaxCMB);
  ECP_Map<double> m(Nrings, Nphi);
  
  read_powspec_from_fits("spectrum.fits", cmbspec, 1, lmaxCMB);

  create_alm(cmbspec, almCMB, rng);

  write_Alm_to_fits("!ecp_alm.fits", almCMB, almCMB.Lmax(), almCMB.Mmax(), planckType<double>()); 
  
  test_generation(almCMB, m);
  test_interpolation(m, cmbspec.tt());
  
  return 0;
}

