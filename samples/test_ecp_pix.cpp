/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./samples/test_ecp_pix.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <stdlib.h>
#undef NASSERT
#include <assert.h>
#include "ecp_map.hpp"

int main()
{
  int Nrings = 128, Nphi = 128;
  ECP_Map_Base geom(Nrings, Nphi);

  for (long p = 0; p < geom.Npix(); p++)
    {
      double theta, phi;

      geom.pix2ang(p, theta, phi);
      int p2 = geom.ang2pix(theta,phi);
      assert(p==p2);
    }
  return 0;
}
