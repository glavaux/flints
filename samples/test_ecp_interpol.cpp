/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./samples/test_ecp_interpol.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <stdlib.h>
#undef NASSERT
#include <assert.h>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include "ecp_map.hpp"

template<typename T>
T interpol(const ECP_Map<T>& m, const pointing& p)
{
  fix_arr<int, 4> pixlist;
  fix_arr<double, 4> wgt;
  T result = 0;
 
  m.get_interpol(p, pixlist, wgt);
  for (int i = 0 ; i < 4; i++)
    result += wgt[i] * m[pixlist[i]];
  return result;
}

int main()
{
  int Nrings = 16, Nphi = 16;
  ECP_Map<float> geom(Nrings, Nphi);
  Healpix_Map<float> out;

  out.SetNside(512,RING);

  for (long p = 0; p < geom.Npix(); p++)
    {
      geom[p] = p;
    }

  for (long p = 0; p < out.Npix(); p++)
    out[p] = interpol(geom, out.pix2ang(p));

  fitshandle f;
  f.create("!interpol.fits");
  write_Healpix_map_to_fits(f, out, planckType<float>());

  return 0;
}
