/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./samples/lens.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/


#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <alm.h>
#include <xcomplex.h>
#include <cstring>
#include <iostream>
#include <planck_rng.h>
#include <powspec.h>
#include <powspec_fitsio.h>
#include <alm_healpix_tools.h>
#include <alm_powspec_tools.h>
#include <alm_fitsio.h>
#include <fitshandle.h>
#include "multi_healpix.hpp"
#include "lens_opt_gen.h"
#include "lensing_operator_flints.hpp"

using namespace std;
using namespace CMB;

int checkParameters(struct lens_args_info& info)
{
  if (!info.primaryTemp_given && !info.primaryPol_given)
    {
      cerr << "I do not know what to lens. Stop here." << endl;
      return 1;
    }

  if ((info.primaryTemp_given && !info.outputTemp_given) ||
      ((info.primaryPol_given) && !info.outputPol_given))
    {
      cerr << "Inconsistent input/output information. Stop here." << endl;
      return 1;
    }

  if (info.primaryTemp_given && info.primaryPol_given)
    {
      cerr << "Polarization implies temperature. You cannot specify a primary indepedent temperature at the same time." << endl;
      return 1;
    }


  if (info.primaryPol_given && (!info.outputPol_given && !info.computeSpectrum_given))
    {
      cerr << "Strange. You ask for polarization but do not ask to get the output." << endl;
    }

  if (info.primaryTemp_given && (!info.outputTemp_given && !info.outputPol_given && !info.computeSpectrum_given))
    {
      cerr << "Strange. You ask for temperature but do not ask to get the output." << endl;
    }



  return 0;
}

int main(int argc, char **argv)
{
  struct lens_args_info info;
  struct lens_parse_arg_params argparams;
  planck_rng rng;
  Healpix_Map<double> T, lT;
  Healpix_Map<double> Q, U, lQ, lU;
  Alm<xcomplex<double> > almT, almE, almB, almET, almPhi;
  bool doTemperature = false, doPolarization = false;
  PowSpec powspec_tt, powspec_TEB;
  long lmax_spec;

  lens_parse_arg_init(&info);
  lens_parse_arg_params_init(&argparams);

  argparams.check_required = 0;
  if (lens_parse_arg_ext(argc, argv, &info, &argparams))
    return -1;

  if (info.configfile_given)
    {
      argparams.override = 0;
      argparams.initialize = 0;
      argparams.check_required = 1;
      if (lens_parse_arg_config_file(info.configfile_arg, &info, &argparams))
        {
          lens_parse_arg_free(&info);
          return -1;
        }
    }
  else if (lens_parse_arg_required(&info, argv[0]))
    return -1;

  cout
    << "LENS-FLINTS  Copyright (C) 2010-2011  Guilhem Lavaux." << endl
    << "This program comes with ABSOLUTELY NO WARRANTY" << endl
    << "This is free software, and you are welcome to redistribute it" << endl
    << "under certain conditions." << endl;

  if (info.seed_given)
    rng.seed(info.seed_arg);

  almT.Set(info.Lmax_arg, info.Lmax_arg);
  almE.Set(info.Lmax_arg, info.Lmax_arg);
  almB.Set(info.Lmax_arg, info.Lmax_arg);
  almET.Set(info.Lmax_arg, info.Lmax_arg);

  lmax_spec = info.Lmax_arg;

  // Sanity checking of the parameters
  if (checkParameters(info))
    return 1;

  // +++++++++++++++++++++++++++++++++++++ Prepare lensing potential ++++++++++++++
  
  // Read lensing potential power spectra, if given. And generate random potential.
  // If not given, attempt to read it from "potential_arg".
  if (info.specPotential_given)
    {
      PowSpec potential_spec;

      almPhi.Set(info.LmaxPhi_arg, info.LmaxPhi_arg);
      read_powspec_from_fits(info.specPotential_arg, potential_spec, 1, info.LmaxPhi_arg);
      create_alm(potential_spec, almPhi, rng);

      if (info.savePotential_given)
	{
	  write_Alm_to_fits(info.savePotential_arg, almPhi, info.LmaxPhi_arg, info.LmaxPhi_arg, planckType<double>());
	}
    }
  else
    {
      if (!info.potential_given)
	{
	  cerr << "You must specify either a lensing potential spectrum, or alms of the lensing potential" << endl;
	  return 1;
	}
      read_Alm_from_fits(info.potential_arg, almPhi, info.LmaxPhi_arg, info.LmaxPhi_arg);

      if (info.fudgePhi_given)
        {
          for (int l = 0; l <= info.LmaxPhi_arg; l++)
            for (int m = 0; m <= l; m++)
              almPhi(l,m) *= info.fudgePhi_arg;
        }
    }


  // +++++++++++++++++++++++++++++++++++ Do primary temperature ++++++++++++++++++
  // Now initialize the primary fluctuations map.

  // First the temperature, if required
  if (info.primaryTemp_given)
    {
      if (!info.specTemp_given)
	{
	  cerr << "We need the temperature spectrum to be able to lens correctly." << endl;
	  return 1;
	}

      read_powspec_from_fits(info.specTemp_arg, powspec_tt, 1, info.Lmax_arg);

      // Generate random fluctuations
      if (strcmp(info.primaryTemp_arg, "random") == 0)
	{
	  create_alm (powspec_tt, almT, rng);
	}
      else
	{
	  read_Alm_from_fits(info.primaryTemp_arg, almT, info.Lmax_arg, info.Lmax_arg);
	}
      fitshandle h;
      h.create("!primary_alm.fits");
      write_Alm_to_fits(h, almT, info.Lmax_arg, info.Lmax_arg, planckType<double>());

      T.SetNside(info.NsidePrimary_arg, RING);
      lT.SetNside(info.NsideOut_arg, RING);

      alm2map(almT, T);

      doTemperature = true;
    }


  // ++++++++++++++++++++++++++++++++ Do primary polarization +++++++++++++++++++++
  // Second the polarization, if required
  if (info.primaryPol_given)
    {
      if (!info.specPol_given)
	{
	  cerr << "We need the polarization spectrum to be able to lens correctly." << endl;
	  return 1;
	}

      read_powspec_from_fits(info.specPol_arg, powspec_TEB, 4, info.Lmax_arg);
      {
	arr<double> tmp_tt = powspec_TEB.tt();
	powspec_tt.Set(tmp_tt);
      }

      if (strcmp(info.primaryPol_arg, "random") == 0)
	{
	  create_alm_pol(powspec_TEB, almT, almE, almB, rng);	  
	}
      else
	{
	  read_Alm_from_fits(info.primaryPol_arg, almT, info.Lmax_arg, info.Lmax_arg, 2);
	  read_Alm_from_fits(info.primaryPol_arg, almE, info.Lmax_arg, info.Lmax_arg, 3);
	  read_Alm_from_fits(info.primaryPol_arg, almB, info.Lmax_arg, info.Lmax_arg, 4);
	}
      
      Q.SetNside(info.NsidePrimary_arg, RING);
      U.SetNside(info.NsidePrimary_arg, RING);
      lQ.SetNside(info.NsideOut_arg, RING);
      lU.SetNside(info.NsideOut_arg, RING);
      T.SetNside(info.NsidePrimary_arg, RING);
      lT.SetNside(info.NsideOut_arg, RING);
 
      alm2map_pol(almT, almE, almB, T, Q, U);
      
      doTemperature = true;
      doPolarization = true;
    }
  

  // +++++++++++++++++++++++++++++++++++++++++++++= LENSING +++++++++++++++++++++++
  if (doTemperature)
    {
      LensingOperatorFlints_Scalar<double,double> 
	scalar(info.NsidePrimary_arg, info.NsideOut_arg);
      
      scalar.update_CMB_spectrum(powspec_tt.tt());
      scalar.setLensingField(almPhi);
      scalar.applyLensing<false>(T, lT);

      if (info.outputTemp_given && !info.outputPol_given)
	{
	  fitshandle h;

	  cout << "Writing temperature to " << info.outputTemp_arg << endl;
	  h.create(string("!") + string(info.outputTemp_arg));
	  write_Healpix_map_to_fits(h, lT, FLINTS_convert_to_fitsio(planckType<double>()));
	}

      if (info.outputPrimary_given && !info.outputPol_given)
        {
	  fitshandle h;

	  cout << "Writing primary temperature to " << info.outputPrimary_arg << endl;
	  h.create(string("!") + string(info.outputPrimary_arg));
	  write_Healpix_map_to_fits(h, T, FLINTS_convert_to_fitsio(planckType<double>()));
        }

      if (info.computeSpectrum_given && !doPolarization)
	{
	  Alm<xcomplex<double> > alm_lensed(lmax_spec, lmax_spec);
	  PowSpec powspec(1, lmax_spec);
	  arr<double> alm_weights(2*lT.Nside());

	  alm_weights.fill(1);

	  map2alm_iter(lT, alm_lensed, info.iterAlm_arg, alm_weights);
	  
	  extract_powspec(alm_lensed, powspec);	  

	  fitshandle out;
	  out.create(info.computeSpectrum_arg);
	  write_powspec_to_fits (out, powspec, 1);
	}
    }

  if (doPolarization)
    {
      LensingOperatorFlints_Polarization<double,double> 
	polar(info.NsidePrimary_arg, info.NsideOut_arg);

      polar.update_CMB_spectrum(powspec_TEB.gg());
      polar.setLensingField(almPhi);
      polar.applyLensing<false>(Q, U, lQ, lU);      

      if (info.outputPol_given)
	{
	  fitshandle h;

	  cout << "Writing lensed temperature+polarization to " << info.outputPol_arg << endl;
	  h.create(string("!") + string(info.outputPol_arg));
	  write_Healpix_map_to_fits(h, lT, lQ, lU, FLINTS_convert_to_fitsio(planckType<double>()));
	}
      
      if (info.outputPrimary_given)
	{
	  fitshandle h;

	  cout << "Writing primary temperature+polarization to " << info.outputPrimary_arg << endl;
	  h.create(string("!") + string(info.outputPrimary_arg));
	  write_Healpix_map_to_fits(h, T, Q, U, FLINTS_convert_to_fitsio(planckType<double>()));
	}


      if (info.computeSpectrum_given)
	{
	  Alm<xcomplex<double> > 
	    almT_lensed(lmax_spec, lmax_spec),
	    almE_lensed(lmax_spec, lmax_spec),
	    almB_lensed(lmax_spec, lmax_spec);
	  PowSpec powspec(4, lmax_spec);
	  arr<double> alm_weights(2*lT.Nside());

	  alm_weights.fill(1.0);

	  cout << "Computing lensed alms..." << endl;
	  map2alm_pol_iter(lT, lQ, lU, almT_lensed, almE_lensed, almB_lensed, 
			   info.iterAlm_arg, alm_weights);
	  
	  cout << "Extracting and writing spectra to " << info.computeSpectrum_arg << endl;
	  extract_powspec(almT_lensed, almE_lensed, almB_lensed, powspec);

	  fitshandle out;
	  out.create(info.computeSpectrum_arg);
	  write_powspec_to_fits (out, powspec, 4);
	}
    }

  return 0;

}
