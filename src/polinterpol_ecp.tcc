/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/polinterpol_ecp.tcc
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/


#include <cmath>
#include <iomanip>
#include <omp.h>
#include "wignerFunction.hpp"
#include "extra_complex.h"
#include "eigenvals.hpp"
#include "packMatrix.hpp"

namespace CMB
{  

  template<typename T,typename T2, int s>
  void QuickPolInterpolateMap_ECP<T,T2,s>::matrixError(const arr<xcomplex<T2> >& Correlation, int npix)
  {
    std::ofstream mfile_r("matrix_re.txt");
    std::ofstream mfile_i("matrix_im.txt");
    for (int i = 0; i < npix; i++)
      {
	for (int j = 0; j < npix; j++)
	  {
              mfile_r << std::setprecision(20) << Correlation[i*npix+j].real() << std::endl;
              mfile_i << std::setprecision(20) << Correlation[i*npix+j].imag() << std::endl;
          }
      }
    std::cout
      << "Error in covariance matrix decomposition. "
      << "Matrix dumped to 'matrix.txt'" << std::endl;
    abort();
  }
  
  class PolInterpolateCacheError {};
  
  template<typename T,typename T2,int s>
  void QuickPolInterpolateMap_ECP<T,T2,s>::doRealComputation()
    throw (PlanckError)
  {
    int numPoints = precomputed_correlations.size();
    arr<arr<T2> > data_correlations(omp_get_max_threads());

    precomputed_correlations.fill(0);
   
#pragma omp parallel 
    {
      arr<T2> data(2*numPoints);
      arr<T2> &C = data_correlations[omp_get_thread_num()];

      C.alloc(numPoints);
      C.fill(0);

#pragma omp for schedule(dynamic,10)
      for (int l = std::abs(s); l <= lmax; l++)
        {
          (std::cout << l << " " ).flush();
          // We compute d^l_{-s,-s}, because we are interested
          // in spin s, (spin +s field)
          computeWigner_D(l, -s, -s, 2*numPoints, &data[0]);
          T2 prefac = (2*l+1)/(4*M_PI)*cls[l]; 
          for (int i = 0; i < numPoints; i++)
            C[i] += prefac*data[i];
        }
      std::cout << " FINISHING..." << std::endl;
    }
    precomputed_correlations.fill(0);
    
    for (int j = 0; j < data_correlations.size(); j++)
      for (int i = 0; i < data_correlations[j].size(); i++)
        precomputed_correlations[i] += data_correlations[j][i];


    {
      std::ofstream df("dataout.txt");
      for (int i = 0 ; i < numPoints; i++)
	df << M_PI*i/numPoints << " " << precomputed_correlations[i] << std::endl;
    }
  }

  template<typename T,typename T2, int s>
  void QuickPolInterpolateMap_ECP<T,T2,s>::computeRotation(double theta, double phi,
						     double thetap, double phip,
						     double& alpha, double& beta, double& gamma)
  {
    // This is the extreme case. But we are handling a pixelization
    // on ring so that happens quite often...
    if (phi == phip)
      {
	if (theta < thetap)
	  {
	    alpha = gamma = 0;
	    beta = thetap-theta;
	  }
	else
	  {
	    alpha = gamma = M_PI;
	    beta = theta-thetap;
	  }
	return;
      }

    double ctheta = cos(theta), stheta = sin(theta);
    double ctheta_p = cos(thetap), stheta_p = sin(thetap);
    double c_delta_phi = cos(phi-phip);
    double sin_delta_phi = sin(phip-phi);
    double cphi = cos(phi), cphi_p = cos(phip);
    double sphi = sin(phi), sphi_p = sin(phip);

    double u[3] = {  stheta*cphi, stheta*sphi, ctheta };
    double v[3] = { stheta_p * cphi_p, stheta_p*sphi_p, ctheta_p };
    
    double u_n_2 = u[0]*u[0] + u[1]*u[1] + u[2]*u[2];
    double v_n_2 = v[0]*v[0] + v[1]*v[1] + v[2]*v[2];

    double cos_beta = (u[0]*v[0]+u[1]*v[1]+u[2]*v[2])/sqrt(u_n_2*v_n_2);
    double sin_beta = sqrt(1-cos_beta*cos_beta);

    double cos_alpha =
      -(ctheta_p*stheta*c_delta_phi - stheta_p*ctheta);
    // We do not actually divide by sin_beta here to avoid singularities...
    ///sin_beta; 
    // This improves the stability of the later atan2 inversion
    double sin_alpha =
      stheta*sin_delta_phi;

    // Same thing for cos_gamma
    double cos_gamma =
      (ctheta*stheta_p*c_delta_phi - stheta*ctheta_p);
    double sin_gamma =
      -stheta_p*sin_delta_phi;

    assert(std::abs(cos_alpha) <= 1);
    assert(std::abs(cos_gamma) <= 1);

    alpha = atan2(sin_alpha, cos_alpha);
    gamma = atan2(sin_gamma, cos_gamma);
    if (std::abs(cos_beta) > 1)
     {
       if (std::abs(cos_beta) > 1+1e-8)
	 std::cerr << "WARNING: cos(beta) > 1 (value is " << cos_beta-1 << ") , enforcing cos(beta) = 1." << std::endl;
       cos_beta = (cos_beta < -1) ? -1 : 1;
     }
    beta = acos(cos_beta);
  }

  template<typename T,typename T2, int s>
  void QuickPolInterpolateMap_ECP<T,T2,s>::precomputeCorrelations()
    throw(PlanckError)
  {
    int numPoints;

    if (VERBOSE)
      std::cout << "Precomputing correlation function" << std::endl;
    
    int minimalPoints = int(round(2*M_PI/minAngle));
    if (VERBOSE)
      std::cout << " ++ minimal number of points = " << minimalPoints << " / " << 2*lmax+1 << std::endl;
    if (minimalPoints < 2*lmax+1)
      minimalPoints = 2*lmax+1;

    int il2 = 0;
    for (il2 = 0; minimalPoints > 0; il2++)
      minimalPoints /= 2;
    il2+=2;
    numPoints = 1 << il2;
    
    if (VERBOSE)
      std::cout << "Using " << numPoints << " points for the correlation" << std::endl;

    delta = M_PI/numPoints;

    precomputed_correlations.alloc(numPoints);

    try
      {
	if (USE_CACHE)
	  {
	    std::ifstream fcache("cache_pol_wigner_2.dat");
	    if (!fcache)
	      throw PolInterpolateCacheError();

	    double d;
	    int n;
	    double maxA;

	    std::cout << "Cache detected" << std::endl;

	    fcache.read((char*)&d, sizeof(double));
	    fcache.read((char *)&n, sizeof(int));
	    fcache.read((char *)&maxA, sizeof(double));
	    if (d != delta || n != numPoints || maxA != minAngle)
	      throw PolInterpolateCacheError();
	    arr<T2> xs;
	    xs.alloc(numPoints);
	    fcache.read((char *)&xs[0], sizeof(T2)*numPoints);
	    fcache.read((char *)&variance, sizeof(T2));
	    fcache.read((char *)&precomputed_correlations[0], sizeof(T2)*numPoints);
	    if (fcache.eof())
	      throw PolInterpolateCacheError();
	    
	    std::cout << "Cache loaded successfully" << std::endl;
	    std::cout << "Building cubic splines.." << std::endl;
            cubic = CubicSpline<T2>(xs,precomputed_correlations);
	    return;
	  }
      }
    catch (PolInterpolateCacheError e)
      {
	std::cout << "Error while opening cache. rebuilding it." << std::endl;
      }

    doRealComputation();

    variance = precomputed_correlations[0];
    arr<T2> xs;
    xs.alloc(numPoints);
    for (int i = 0; i < numPoints; i++)
      {
	xs[i] = delta*i;	
	precomputed_correlations[i] /= variance;
      }

    if (USE_CACHE)
      {
	std::ofstream fcache("cache_pol_wigner_2.dat");

	fcache.write((char *)&delta, sizeof(double));
	fcache.write((char *)&numPoints, sizeof(int));
	fcache.write((char *)&minAngle, sizeof(double));
	fcache.write((char *)&xs[0], sizeof(T2)*numPoints);
	fcache.write((char *)&variance, sizeof(T2));
	fcache.write((char *)&precomputed_correlations[0], sizeof(T2)*numPoints);
      }
    cubic = CubicSpline<T2>(xs,precomputed_correlations);
  }

  template<typename T,typename T2, int s>
  void QuickPolInterpolateMap_ECP<T,T2,s>::setupMatrix(const arr<pointing>& all_vec, 
						 arr<xcomplex<T2> >& M, 
						 const arr<int>& pixels, T2 noiseReg)
    throw(PlanckError)
  {
    int npix = all_vec.size();

    for (int i = 0; i < npix; i++)
      {
        M[i*npix+i] = T2(1) + noiseReg;
        if (pixels[i] < 0) {
          for (int j = i+1; j < npix; j++)
            M[i*npix+j] = 0;
          continue;
        }
	
	for (int j = i+1; j < npix; j++)
	  {
	    if (pixels[j] < 0)
	      {
          M[i*npix + j] = 0;
          continue;
	      }
	    double alpha, beta, gamma;
	    computeRotation(all_vec[j].theta,
			    all_vec[j].phi,
			    all_vec[i].theta,
			    all_vec[i].phi,
			    alpha, beta, gamma);
	    double value1 = computeCorrelation(beta);
	    // We have a spin +s field. Thus the -s.
	    double epsilon = -s*(alpha+gamma);
	    
	    M[i*npix + j] = value1*xcomplex<T2>(cos(epsilon),sin(epsilon));
	    M[j*npix + i] = conj(M[i*npix + j]);
	  }
      }
  }

  template<typename T,typename T2, int s>
  bool QuickPolInterpolateMap_ECP<T,T2,s>::getMatrix(xcomplex<T2>* C, T2* nregul, int pix, arr<int>& pixel_list)
  {
    unsigned int ring = baseLevel.pix2ring(pix);
    xcomplex<T2> *cap, *backupPixels;
    xcomplex<T2> *v;
    
    if (bad_matrix[ring])
      return false;

    v = &equatorialRings[ring*numMatrixElements];
    *nregul = noiseRegul[ring];
    upMatrix<false>(v, C, numNeighbours);
    return true;
  }

  template<typename T,typename T2, int s>
  xcomplex<T> QuickPolInterpolateMap_ECP<T,T2,s>::interpolate(const ECP_Map<xcomplex<T> >& map,
							const pointing& ptgs,
							int *npix, T *var)
    throw (PlanckError)
  {
    T var0;
    int npix0;
    xcomplex<T> result0;

    interpolateMany(map, &ptgs, &npix0, &var0, &result0, 1);

    if (npix)
      *npix = npix0;
    if (var)
      *var = var0;

    return result0;
  }

  template<typename T,typename T2, int s>
  void QuickPolInterpolateMap_ECP<T,T2,s>::interpolateMany(const ECP_Map<xcomplex<T> >& map,
						     const pointing *ptgs,
						     T *var, xcomplex<T> *result,
						     int numDirections)
    throw (PlanckError)
   {
     interpolateManyBase<false,true>(map, ptgs, 0, var, result, numDirections);
   }

  template<typename T,typename T2, int s>
  void QuickPolInterpolateMap_ECP<T,T2,s>::interpolateMany(const ECP_Map<xcomplex<T> >& map,
                                              const pointing *ptgs,
                                              int *npix, xcomplex<T> *result,
                                              int numDirections)
    throw (PlanckError)
   {
     interpolateManyBase<true,false>(map, ptgs, npix, 0, result, numDirections);
   }

  template<typename T,typename T2, int s>
  void QuickPolInterpolateMap_ECP<T,T2,s>::interpolateMany(const ECP_Map<xcomplex<T> >& map,
						     const pointing *ptgs,
						     int *npix, T *var, xcomplex<T> *result,
						     int numDirections)
    throw (PlanckError)
   {
     interpolateManyBase<true,true>(map, ptgs, npix, var, result, numDirections);
   }

  template<typename T,typename T2, int s>
  void QuickPolInterpolateMap_ECP<T,T2,s>::interpolateMany(const ECP_Map<xcomplex<T> >& map,
                                              const pointing *ptgs,
                                              xcomplex<T> *result,
                                              int numDirections)
    throw (PlanckError)                         
   {                                              
     interpolateManyBase<false,false>(map, ptgs, 0, 0, result, numDirections);
   }

  template<typename T, typename T2, int s> template<bool doVar>
  void QuickPolInterpolateMap_ECP<T,T2,s>::do_basic_interpolation(const ECP_Map<xcomplex<T> >& map,
                                                                  const pointing& ptg, xcomplex<T>& val, double& err)
  {
    fix_arr<int, 4> npix;
    fix_arr<double, 4> wgt;
    // Something went wrong. The matrix is too ill-conditioned
    // to be reliable. Just to a basic interpolation
    map.get_interpol(ptg, npix, wgt);
    val = 0;
    for (int i = 0; i < 4; i++)
      {
        val += wgt[i] * map[npix[i]];
      }
    if (doVar)
      {
        err = 0;
        for (int i = 0; i < 4; i++)
          {
            double delta = (val-map[npix[i]]).norm();
            err += delta;
          }
        err = std::sqrt(err/3.);
      }
  }


  template<typename T, typename T2, int s> template<bool doVar>
  void QuickPolInterpolateMap_ECP<T,T2,s>::do_basic_interpolation_transposed(const xcomplex<T>& map_value,
                                                                             const pointing& ptg, ECP_Map<xcomplex<T> >& val, double& err)
  {
    fix_arr<int, 4> npix;
    fix_arr<double, 4> wgt;
    // Something went wrong. The matrix is too ill-conditioned
    // to be reliable. Just to a basic interpolation
    val.get_interpol(ptg, npix, wgt);
    for (int i = 0; i < 4; i++)
      {
        val[npix[i]] += wgt[i] * map_value;
      }
    if (doVar)
      err = 0;
  }

  
  template<typename T,typename T2, int s> template<bool doNpix,bool doVar>
  void QuickPolInterpolateMap_ECP<T,T2,s>::interpolateManyBase(const ECP_Map<xcomplex<T> >& map,
						  const pointing *ptgs, 
						  int *npix, T *var, xcomplex<T> *result,
						  int numDirections)
    throw (PlanckError)
  {
    
#pragma omp parallel
    {
      arr<int> pixel_list(numNeighbours);
      arr<xcomplex<T2> > workSpace(numNeighbours*numNeighbours+numNeighbours*2);
      xcomplex<T2> *CorrelationNgb = &workSpace[0];
      xcomplex<T2> *localCorrelation = &workSpace[numNeighbours*numNeighbours];
      xcomplex<T2> *Correlation_vector = &workSpace[numNeighbours*numNeighbours+numNeighbours];
      xcomplex<T2> geomVariance;
      for (int i = 0 ; i < numNeighbours; i++)
        localCorrelation[i] = 0;
#pragma omp for schedule(static)      
      for (int q = 0 ; q < numDirections; q++) {
        pointing vInterpolate = ptgs[q];
        int pix = map.ang2pix(ptgs[q]);
        xcomplex<T> accum_value = 0;
        T2 nregul;

        // We discover the neighbours
        discoverNeighbours(pix, pixel_list);
        
        // First we extract the geometry matrix from the cache
        if (!getMatrix(CorrelationNgb, &nregul, pix, pixel_list))
          {
            double loc_var;

            do_basic_interpolation<doVar>(map, vInterpolate, result[q], loc_var);
            if (doVar)
              var[q] = -loc_var;
            if (doNpix)
              npix[q] = 4;
            continue;
          }
        
        // Setup the correlation between the direction to interpolate 
        // and the direction of the pixels of the mesh.
        for (int i = 0; i < numNeighbours; i++)
          {
            int pixcur = pixel_list[i];
            double alpha, beta, gamma;
            pointing& dir = directions[pixcur];
            computeRotation(vInterpolate.theta,
                            vInterpolate.phi,
                            dir.theta,
                            dir.phi,
                            alpha, beta, gamma);
            // Still looking at spin +s fields
            double epsilon = -s*(alpha+gamma);

            localCorrelation[i] = computeCorrelation(beta)*xcomplex<T2>(cos(epsilon),sin(epsilon));
          }
  // 12.15
        
        CMB_Interpolation::compute_line<xcomplex<T2>, true>(CorrelationNgb, localCorrelation, 
						                                                Correlation_vector, geomVariance, 
						                                                numNeighbours, nregul);
  // 16.74
        
        if (geomVariance.real() < 0)
          {
            double loc_var;

            do_basic_interpolation<doVar>(map, vInterpolate, result[q], loc_var);
            if (doVar)
              var[q] = -loc_var;
            if (doNpix)
              npix[q] = 4;
            continue;
          }

        // Compute the scalar product
        for (int i = 0; i < numNeighbours; i++)
          accum_value += conj(Correlation_vector[i])*map[pixel_list[i]];
        if (doVar)
          var[q] = sqrt(variance*geomVariance.real());
        result[q] = accum_value;
        if (doNpix) {
          int n = 0;
          npix[q] = numNeighbours;
        }

        assert(!std::isnan(accum_value.real()));
      }
    }
  }



 template<typename T,typename T2, int s> template<bool doNpix,bool doVar>
  void QuickPolInterpolateMap_ECP<T,T2,s>::interpolateManyTransposedBase(const xcomplex<T> * restrict_flints mapValue,
								     ECP_Map<xcomplex<T> >& outMap,
								     const pointing * restrict_flints ptgs, 
								     int *npix, T *var,
								     int numDirections)
    throw (PlanckError)
  {
    arr<int> pixel_list(numNeighbours);
    arr<xcomplex<T2> > workSpace(numNeighbours*numNeighbours+numNeighbours*2);
    xcomplex<T2> *CorrelationNgb = &workSpace[0];
    xcomplex<T2> *localCorrelation = &workSpace[numNeighbours*numNeighbours];
    xcomplex<T2> *Correlation_vector = &workSpace[numNeighbours*numNeighbours+numNeighbours];
    xcomplex<T2> geomVariance;
    
    for (int i = 0 ; i < numNeighbours; i++)
      localCorrelation[i] = 0;
    for (int q = 0 ; q < numDirections; q++) {
      pointing vInterpolate = ptgs[q];
      T2 nregul;
      int pix = baseLevel.ang2pix(ptgs[q]);

      // We discover the neighbours
      discoverNeighbours(pix, pixel_list);
      
      xcomplex<T> localVal = mapValue[q];
      // First we extract the geometry matrix from the cache
      if (!getMatrix(CorrelationNgb, &nregul, pix, pixel_list))
        {
          double loc_var;

          do_basic_interpolation_transposed<doVar>(localVal, vInterpolate, outMap, loc_var);
          if (doVar)
            var[q] = -loc_var;
          if (doNpix)
            npix[q] = 4;
          continue;
        }
      
      // Setup the correlation between the direction to interpolate 
      // and the direction of the pixels of the mesh.
      for (int i = 0; i < numNeighbours; i++)
        {
          int pixcur = pixel_list[i];
          double alpha, beta, gamma;

          pointing& dir = directions[pixcur];
          computeRotation(vInterpolate.theta,
	            vInterpolate.phi,
	            dir.theta,
	            dir.phi,
	            alpha, beta, gamma);
          double epsilon = -s*(alpha+gamma);

          localCorrelation[i] = computeCorrelation(beta)*xcomplex<T2>(cos(epsilon),sin(epsilon));
        }
// 12.15
      
      CMB_Interpolation::compute_line<xcomplex<T2>, true>(CorrelationNgb, localCorrelation, 
						Correlation_vector, geomVariance, 
						numNeighbours, nregul);
// 16.74
      
      if (geomVariance.real() < 0)
        {
          double loc_var;

          do_basic_interpolation_transposed<doVar>(localVal, vInterpolate, outMap, loc_var);
          if (doVar)
            var[q] = -loc_var;
          if (doNpix)
            npix[q] = 4;
          continue;
        }


      // Compute the scalar product
      for (int i = 0; i < numNeighbours; i++)
	{
	  long pix = pixel_list[i];
	  outMap[pix] += (Correlation_vector[i])*localVal;
	}

      if (doVar)
        var[q] = sqrt(variance*geomVariance.real());
      if (doNpix) {
        int n = 0;
        for (int i = 0 ; i < numNeighbours; i++)
          n += (pixel_list[i] >= 0) ? 1 : 0;
        npix[q] = n;
      }
    }
  }

  
  template<typename T, typename T2, int s>
  void QuickPolInterpolateMap_ECP<T,T2,s>::discoverNeighbours(long pix, arr<int>& pixels)
  {
    fix_arr<int,8> subpix;
    baseLevel.neighbors(pix, subpix);
    pixels[0] = pix;
    memcpy(&pixels[1], &subpix[0], sizeof(int)*8);
  }
  
  static void printMatrix(const char *base_fname, xcomplex<double> *M, int N)
  {
    std::string fr(base_fname), fi(base_fname);

    fr += "real";
    fi += "imag";
    std::ofstream f1(fr.c_str());
    std::ofstream f2(fi.c_str());

    for (int i = 0; i < N; i++)
      {
	for (int j = 0; j < N; j++)
	  {
	    f1 << M[i*N+j].real() << std::endl;
	    f2 << M[i*N+j].imag() << std::endl;
	  }
      }
  }

  template<typename T, typename T2, int s>
  bool QuickPolInterpolateMap_ECP<T,T2,s>::computeMatrix(int pix,
                                                         arr<int>& pixels,
                                                         arr<pointing>& all_vec,
                                                         arr<xcomplex<T2> >& cho,
                                                         arr<xcomplex<T2> >& M,
                                                         xcomplex<T2> *out, T2 *nregul)
  {
    T2 prec_level = 1/std::sqrt(std::numeric_limits<T2>::epsilon());

    discoverNeighbours(pix, pixels);

    for (int j = 0; j < numNeighbours; j++)
      if (pixels[j] >= 0)
        all_vec[j] = directions[pixels[j]];
	
    int err = -1;
    
    *nregul = std::sqrt(std::numeric_limits<T2>::epsilon());
    while (true)
      {
        setupMatrix(all_vec, cho, pixels, *nregul);
        err = do_c_cholesky_decomposition(cho, numNeighbours);
        if (err < 0)
          {
            *nregul *= 2;
            std::cout << "Boosting Noise regulation to " << *nregul << std::endl;
            continue;
          }
        do_c_cholesky_inversion(cho, numNeighbours);
        do_c_cholesky_multiplication(cho, M, numNeighbours);

        // Check that the diagonal does not blow the precision
#if 0
        for (int i = 0; i < numNeighbours; i++)
          if (M[i*(numNeighbours+1)].real() > prec_level/10)
            {
              err = -1;
              break;
            }
#endif
        if (err < 0)
          *nregul *= 2;
        else
          break;
        std::cout << "Boosting Noise regulation to " << *nregul << std::endl;
      }

    upMatrix<true>(&M[0], out, numNeighbours);

    return false;
  }

  
  template<typename T, typename T2, int s>
  void QuickPolInterpolateMap_ECP<T,T2,s>::precomputeGeometry()
    throw (PlanckError)
  {
    arr<int> pixels;
    arr<pointing> all_vec;
    arr<xcomplex<T2> > M, cho;

    equatorialRings.alloc(numMatrixElements*Nrings);

    pixels.alloc(numNeighbours);
    all_vec.alloc(numNeighbours);
    M.alloc(numNeighbours*numNeighbours);
    cho.alloc(numNeighbours*numNeighbours);
    noiseRegul.alloc(Nrings);
    bad_matrix.alloc(Nrings);

    std::cout << "Computing pixelization statistical properties..." << std::endl;

    try {
      if (USE_CACHE) {
        int locNumNeighbours, locNumMatElements, locNrings, locNphi, locBackupRings;

        std::ifstream f("cache_pol_geometry.dat");
        if (!f)
          throw PolInterpolateCacheError();

        f.read((char *)&locNumNeighbours, sizeof(int));
        f.read((char *)&locNumMatElements, sizeof(int));
        f.read((char *)&locNrings, sizeof(int));
        f.read((char *)&locNphi, sizeof(int));
        if (locNphi != Nphi || locNrings != Nrings || locNumNeighbours != numNeighbours  ||
            locNumMatElements != numMatrixElements)
          throw PolInterpolateCacheError();
	f.read((char *)&equatorialRings[0], sizeof(xcomplex<T2>)*numMatrixElements*Nrings);
        f.read((char *)&bad_matrix[0], sizeof(bool)*Nrings);
	if (!f)
	  throw PolInterpolateCacheError();
        return;
      }
    } catch (const PolInterpolateCacheError & e) 
       {
          std::cerr << "Error loading cache. Rebuilding." << std::endl;
       }

    int lastpct = -1;
//#pragma omp parallel for schedule(static)
    for (int ring = 1; ring < Nrings-1; ring++)
      {
        int startpix = ring * Nphi;

        bad_matrix[ring] = computeMatrix(startpix, pixels, all_vec, cho, M, &equatorialRings[ring*numMatrixElements], &noiseRegul[ring]);
      }
    bad_matrix[0] = bad_matrix[Nrings-1] = true;

    if (USE_CACHE) {
      std::ofstream f("cache_pol_geometry.dat");
      if (!f)
         return;
      f.write((char *)&numNeighbours, sizeof(int));
      f.write((char *)&numMatrixElements, sizeof(int));
      f.write((char *)&Nrings, sizeof(int));
      f.write((char *)&Nphi, sizeof(int));
      f.write((char *)&equatorialRings[0], sizeof(xcomplex<T2>)*numMatrixElements*Nrings);
      f.write((char *)&bad_matrix[0], sizeof(bool)*Nrings);
    }
  }

};
