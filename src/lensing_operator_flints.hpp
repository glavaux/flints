/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/lensing_operator_flints.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

/**
 * This file is part of FLINTS, for interpolating random fields on the sphere.
 * Copyright (C) 2010-2011 Guilhem Lavaux
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _LENSING_OPERATOR_FLINTS_HPP
#define _LENSING_OPERATOR_FLINTS_HPP

#include <fstream>
#include <iostream>
#include <healpix_map.h>
#ifdef _OPENMP
#include <omp.h>
#endif
#include <arr.h>
#include "multi_healpix.hpp"
#include "blens.hpp"
#include "qbinterpol.hpp"
#include "polinterpol.hpp"

namespace CMB {


  /**
   * This class is an helper to easily and quickly produce lensed temperature maps.
   * It also supports the computation of transposed lensed maps. The code uses OpenMP for
   * parallelization, with the use of some tricks for the transposed lensing.
   *
   * Instantiating this class may use _a lot_ of memory. It allocates 5 healpix maps for 
   * handling the deflections (for chi2 computation it is better to keep a backup of these
   * maps), a ring NCPU of healpix maps for parallelization of the transposed lensing,
   * two arrays of for the new direction of sampling (with a size equal to the number of pixels),
   * and the interpolator itself which has a description of healpix geometry in terms of covariances.
   *
   * This "operator" also supports interpolating to a map of different resolution that the input
   * map. However, the typical use should be to have either the same or a lower resolution than
   * the input map (in lensing mode, and the opposite in transposed lensing mode).
   *
   * The complete initialization of the operator is done in several steps: first the constructor 
   * LensingOperatorFlints_Scalar#LensingOperatorFlints_Scalar, then #update_CMB_spectrum and #setLensingField. 
   *
   * There are two templates parameters that govern the precision of the computation. "T"
   * is the floating point precision of the maps. "T2" is the floating point precision of the
   * covariance matrices. One generally wants "T2" to be double. "T" may be float though.
   *
   * Here is an example on how to use this class:
   * \include example_lensing_scalar.cpp
   * The example consists in initializing the operator and then do one lensing operation. 
   *
   * \sa LensingOperatorFlints_Polarization
   */
  template<typename T,typename T2>
  class LensingOperatorFlints_Scalar
  {
  protected:
    Healpix_Map<T> deflection_phi, deflection_theta, phi_map,
      backup_deflection_phi, backup_deflection_theta;
    QuickInterpolateMap<T,T2> *interpolate;
    int Nside, NsideOut;
    arr<Healpix_Map<T> > outmap_ring;
    arr<pointing> ptgs, backup_ptgs;
  public:
#define NSIDE_OUT(a,b) ((b < 0) ? a : b)
    /**
     * This is the constructor of the temperature lensing operator. 
     * It initializes
     * 
     * \param nSide The resolution used for input/primary fluctuations
     * \param nSideOut Optionally, it is possible to specify a different output (lensed) resolution.
     */
    LensingOperatorFlints_Scalar(long nSide, long nSideOut = -1)
  : deflection_phi(NSIDE_OUT(nSide, nSideOut), RING, SET_NSIDE),
    deflection_theta(NSIDE_OUT(nSide, nSideOut), RING, SET_NSIDE),
    backup_deflection_theta(NSIDE_OUT(nSide, nSideOut), RING, SET_NSIDE),
    backup_deflection_phi(NSIDE_OUT(nSide, nSideOut), RING, SET_NSIDE),
    phi_map(NSIDE_OUT(nSide, nSideOut), RING, SET_NSIDE),
    Nside(nSide), NsideOut(nSideOut < 0 ? nSide : nSideOut)
#undef NSIDE_OUT
    {
      interpolate = 0;
      deflection_theta.fill(0);
      deflection_phi.fill(0);
#ifdef _OPENMP
      outmap_ring.alloc(omp_get_max_threads());
#else
      outmap_ring.alloc(1);
#endif
      for (int i = 0; i < outmap_ring.size(); i++)
	outmap_ring[i].SetNside(nSide, RING);
      ptgs.alloc(phi_map.Npix());
    }

    /**
     * This the destructor of the lensing operator
     */
    ~LensingOperatorFlints_Scalar()
    {
      delete interpolate;
    }

    /**
     * In this function actually occurs the initialization of the
     * interpolator. To do this, it makes use of the given power spectrum of temperature 
     * fluctuations in \p Cls. After that, provided the spectrum does not change, the operator
     * can be used any number of times.
     *
     * \param Cls This is the power spectrum of temperature fluctuations. 
     *
     */
    void update_CMB_spectrum(const arr<T2>& Cls)
    {
      std::cerr << "Somebody has requested to update the Cls assumed to achieve interpolation" << std::endl;
      std::cerr << " => recomputing pixelization..." << std::endl;
      if (interpolate)
	delete interpolate;
      interpolate = new QuickInterpolateMap<T,T2>(Cls, 1, Nside, 0);
    }

    /**
     * This function saves the current deflection field. Once one field can be saved.
     *
     * \sa restoreField
     */
    void saveField()
    {
      backup_deflection_theta = deflection_theta;
      backup_deflection_phi = deflection_phi;
      backup_ptgs = ptgs;
    }

    /**
     * This function restores the deflection field stored by #saveField to the current
     * active deflection field.
     *
     * \sa saveField
     */
    void restoreField()
    {
      deflection_phi = backup_deflection_phi;
      deflection_theta = backup_deflection_theta;
      ptgs = backup_ptgs;
    }

    /**
     * This function setups the current deflection field according to the coefficients of the
     * given in the parameter \p lenspot. They are the spherical harmonic coefficients of the
     * lensing _potential_ (i.e. not the convergence).
     *
     * \param lenspot the lensing potential expressed in terms of its spherical harmonic decomposition.     
     */
    void setLensingField(const Alm<xcomplex<T> >& lenspot)
    {
      std::cerr << " => changing deflection field..." << std::endl;
      alm2map_der1(lenspot, phi_map, deflection_theta, deflection_phi);
#ifdef _OPENMP
#pragma omp parallel for schedule(static)
#endif
      for (long i = 0; i < ptgs.size(); i++) {
        computeMovedDirection(deflection_theta[i],
  			      deflection_phi[i],
			      phi_map.pix2ang(i), ptgs[i]);
      }
      std::cerr << " <= done" << std::endl;
    }

    /**
     * This function setups the current deflection field according to the map of the
     * lensing potential given in the parameter \p lensing_potential. This function
     * does a spherical harmonic transform to translate this map into its spherical harmonic
     * decomposition.
     *
     * \param lensing_potential the map of the lensing potential
     */
    void setLensingField(const Healpix_Map<T>& lensing_potential)
    {
      int lmax_lens = 2*lensing_potential.Nside();
      arr<double> weights(lmax_lens);
      Alm<xcomplex<T> > alms(lmax_lens, lmax_lens);
    
      weights.fill(1);
    
      std::cerr << " => computing deflection field..." << std::endl;
      std::cerr << "       harmonic space..." << std::endl;
      HEALPIX_method(lensing_potential, alms, weights);

      std::cerr << "       computing gradient at CMB resolution..." << std::endl;
      setLensingField(alms);
    }

    /**
     * This is the main function of the LensingOperatorFlints_Scalar class. This function 
     * handles the lensing and transposed lensing operation itself. The operator acts as direct
     * or transposed operator depending on the template parameter "transposedLensing". 
     * A number of sanity checks are run which may trigger an exception PlanckError if they
     * are not satisfied. The conditions are the following:
     *    - both input and output maps must be in \p RING scheme
     *    - the interpolator must have been initialized, i.e. #update_CMB_spectrum has been 
     *      called.
     *    - if transposedLensing is false, then the input map must have the resolution given by 
     *      input Nside at the construction of the LensingOperatorFlints_Scalar object (#LensingOperatorFlints_Scalar), and the output map must have the same resolution as indicated during this construction.
     *    - if transposedLensing is true, the resolutions are reversed.
     *
     * This function makes use of OpenMP to speed up the computation when it is possible. So 
     * do not call it inside an OpenMP loop.
     *
     * \param in the input map on which to apply the direct/transposed lensing operator
     * \param out the output of the operator.
     *
     * \sa LensingOperatorFlints_Scalar, update_CMB_spectrum
     */ 
    template<bool transposedLensing>
    void applyLensing(const Healpix_Map<T>& in,
		      Healpix_Map<T>& out)
      throw (PlanckError)
    {
      bool condition; 

      if (transposedLensing)
	condition = (in.Nside()==NsideOut)&&(out.Nside()==Nside);
      else
	condition = (in.Nside()==Nside)&&(out.Nside()==NsideOut);

      planck_assert(condition, "incompatible input/output maps");
      planck_assert(in.Scheme() == RING, "input must be in RING ordering");
      planck_assert(out.Scheme() == RING, "output must be in RING ordering");
      planck_assert(interpolate != 0, "CMB spectrum must have been initialized");

      if (!transposedLensing) {
#ifdef _OPENMP
#pragma omp parallel
#endif
	{
	  long startpix;
	  long endpix;
          int curThread, numUsedThreads;
#ifdef _OPENMP
	  numUsedThreads = omp_get_num_threads();
          curThread = omp_get_thread_num();
#else
          numUsedThreads = 1;
          curThread = 0;
#endif
	  startpix = curThread*1L*out.Npix()/numUsedThreads;
	  endpix = (curThread+1L)*out.Npix()/numUsedThreads;
 	  interpolate->interpolateMany(in, &ptgs[startpix], &out[startpix], endpix-startpix);
	}
      }
      else 
	{
         int numUsedThreadsGlobal;
#ifdef _OPENMP
#pragma omp parallel
#endif
	  {       
            int numUsedThreads, curThread;
#ifdef _OPENMP
	    numUsedThreads = omp_get_num_threads();
            curThread = omp_get_thread_num();
            if (curThread == 0)
              numUsedThreadsGlobal = numUsedThreads;
#else
            numUsedThreadsGlobal = numUsedThreads = 1;
            curThread = 0;
#endif
	  
	    Healpix_Map<T>& outmap = outmap_ring[curThread];
	  
	    long startpix = curThread*1L*in.Npix()/numUsedThreads;
	    long endpix = (curThread+1L)*in.Npix()/numUsedThreads;
	  
	    outmap.fill(0);
	    interpolate->interpolateManyTransposed(&in[startpix], outmap, &ptgs[startpix], endpix-startpix);
	  }
	  out.fill(0);
	  // Now reduce the maps
	  for (int m = 0; m < numUsedThreadsGlobal; m++)
	    {
	      Healpix_Map<T>& outmap = outmap_ring[m];

#ifdef _OPENMP
#pragma omp parallel for schedule(static)
#endif
	      for (long i = 0; i < out.Npix(); i++)
		out[i] += outmap[i];
	    }
	}

    }

  };

  /**
   * This class is an helper to easily and quickly produce lensed polarization maps.
   * It also supports the computation of transposed lensed maps. The code uses OpenMP for
   * parallelization, with the use of some tricks for the transposed lensing.
   *
   * As for LensingOperatorFlints_Scalar, instantiating this class may use _a lot_ of memory.
   * It allocates 5 healpix maps for 
   * handling the deflections (for chi2 computation it is better to keep a backup of these
   * maps), a ring NCPU of healpix maps for parallelization of the transposed lensing,
   * two arrays of for the new direction of sampling (with a size equal to the number of pixels),
   * and the interpolator itself which has a description of healpix geometry in terms of covariances. This description is twice heavier than for the scalar case as polarization requires the use of complex numbers.
   *
   * This "operator" also supports interpolating to a map of different resolution that the input
   * map. However, the typical use should be to have either the same or a lower resolution than
   * the input map (in lensing mode, and the opposite in transposed lensing mode).
   *
   * There are two templates parameters that govern the precision of the computation. "T"
   * is the floating point precision of the maps. "T2" is the floating point precision of the
   * covariance matrices. One generally wants "T2" to be double. "T" may be float though.
   */
  template<typename T,typename T2>
  class LensingOperatorFlints_Polarization
  {
  protected:
    Healpix_Map<T> deflection_phi, deflection_theta, phi_map,
      backup_deflection_phi, backup_deflection_theta;
    QuickPolInterpolateMap<T,T2> *interpolate;
    bool transposed;
    int Nside, NsideOut;
    arr<Healpix_Map<xcomplex<T> > > outmap_ring;
    arr<pointing> ptgs, backup_ptgs;
    Healpix_Map<xcomplex<T> > tmp_in, tmp_out;
  public:
#define NSIDE_OUT(a,b) ((b < 0) ? a : b)
    /**
     * This is the constructor of the polarization lensing operator. 
     * 
     */
    LensingOperatorFlints_Polarization(long nSide, long nSideOut = -1)
  : deflection_phi(NSIDE_OUT(nSide, nSideOut), RING, SET_NSIDE),
    deflection_theta(NSIDE_OUT(nSide, nSideOut), RING, SET_NSIDE),
    backup_deflection_theta(NSIDE_OUT(nSide, nSideOut), RING, SET_NSIDE),
    backup_deflection_phi(NSIDE_OUT(nSide, nSideOut), RING, SET_NSIDE),
    phi_map(NSIDE_OUT(nSide, nSideOut), RING, SET_NSIDE),
    Nside(nSide), NsideOut(NSIDE_OUT(nSide, nSideOut))
#undef NSIDE_OUT
    {
      interpolate = 0;
      deflection_theta.fill(0);
      deflection_phi.fill(0);
#ifdef _OPENMP
      outmap_ring.alloc(omp_get_max_threads());
#else
      outmap_ring.alloc(1);
#endif
      for (int i = 0; i < outmap_ring.size(); i++)
	outmap_ring[i].SetNside(nSide, RING);
      ptgs.alloc(phi_map.Npix());

      tmp_in.SetNside(Nside, RING);
      tmp_out.SetNside(NsideOut, RING);
    }

    ~LensingOperatorFlints_Polarization()
    {
      delete interpolate;
    }

    /**
     * In this function actually occurs the initialization of the
     * interpolator. To do this, it makes use of the given power spectrum of polarization 
     * fluctuations in \p Cls. After that, provided the spectrum does not change, the operator
     * can be used any number of times. The spectrum of polarization fluctuations is generally only
     * the spectrum of the \f$ E \f$ mode. However to take into account \f$ B \f$ mode in the computation
     * of the interpolation it just consists in summing the two spectrum and thus \f$ \mathrm{Cls} \equiv C_{\ell,E} + C_{\ell,B} \f$.
     *
     * \param Cls This is the power spectrum of polarization fluctuations. 
     *
     */
    void update_CMB_spectrum(const arr<T2>& Cls_GG)
    {
      std::cerr << "Somebody has requested to update the Cls assumed to achieve interpolation" << std::endl;
      std::cerr << " => recomputing pixelization..." << std::endl;
      if (interpolate)
	delete interpolate;

      interpolate = new QuickPolInterpolateMap<T,T2>(Cls_GG, 1, Nside);
    }

    /**
     * This function saves the current deflection field. Once one field can be saved.
     *
     * \sa restoreField
     */
    void saveField()
    {
      backup_deflection_theta = deflection_theta;
      backup_deflection_phi = deflection_phi;
      backup_ptgs = ptgs;
    }

    /**
     * This function restores the deflection field stored by #saveField to the current
     * active deflection field.
     *
     * \sa saveField
     */
    void restoreField()
    {
      deflection_phi = backup_deflection_phi;
      deflection_theta = backup_deflection_theta;
      ptgs = backup_ptgs;
    }

    /**
     * This function setups the current deflection field according to the coefficients of the
     * given in the parameter \p lenspot. They are the spherical harmonic coefficients of the
     * lensing _potential_ (i.e. not the convergence).
     *
     * \param lenspot the lensing potential expressed in terms of its spherical harmonic decomposition.     
     */
    void setLensingField(const Alm<xcomplex<T> >& lenspot)
    {
      std::cerr << " => changing deflection field..." << std::endl;
      alm2map_der1(lenspot, phi_map, deflection_theta, deflection_phi);
#ifdef _OPENMP
#pragma omp parallel for schedule(static)
#endif
      for (long i = 0; i < ptgs.size(); i++) {
        computeMovedDirection(deflection_theta[i],
  			      deflection_phi[i],
			      phi_map.pix2ang(i), ptgs[i]);
      }
      std::cerr << " <= done" << std::endl;
    }

    /**
     * This function setups the current deflection field according to the map of the
     * lensing potential given in the parameter \p lensing_potential. This function
     * does a spherical harmonic transform to translate this map into its spherical harmonic
     * decomposition.
     *
     * \param lensing_potential the map of the lensing potential
     */
     void setLensingField(const Healpix_Map<T>& lensing_potential)
    {
      int lmax_lens = 2*lensing_potential.Nside();
      arr<double> weights(lmax_lens);
      Alm<xcomplex<T> > alms(lmax_lens, lmax_lens);
    
      weights.fill(1);
    
      std::cerr << " => computing deflection field..." << std::endl;
      std::cerr << "       harmonic space..." << std::endl;
      HEALPIX_method(lensing_potential, alms, weights);

      std::cerr << "       computing gradient at CMB resolution..." << std::endl;
      setLensingField(alms);
    }

    /**
     * This function handles the lensing and transposed lensing operation itself. The arguments (\p in and \p out) 
     * are two complex maps. Generally, you would prefer to use #applyLensing(const Healpix_Map<T>& , const Healpix_Map<T>& , Healpix_Map<T>& , Healpix_Map<T>& ) as it does not require conversion of maps to complex types. Currently this function
     * is used internally to achieve the lensing operation itself. The complex maps corresponds to the complex polarization 
     * field \f$P = Q + i U\f$. 
     * 
     * This function makes use of OpenMP to speed up the computation when it is possible. So 
     * do not call it inside an OpenMP loop.
     *
     * \sa LensingOperatorFlints_Polarization, update_CMB_spectrum, applyLensing(const Healpix_Map<T>& , const Healpix_Map<T>& , Healpix_Map<T>& , Healpix_Map<T>& )
     */ 
    template<bool transposedLensing>
    void applyLensing(const Healpix_Map<xcomplex<T> >& in, 
		      Healpix_Map<xcomplex<T> >& out)
    {
      bool condition; 

      if (transposedLensing)
	condition = (in.Nside()==NsideOut)&&(out.Nside()==Nside);
      else
	condition = (in.Nside()==Nside)&&(out.Nside()==NsideOut);

      planck_assert(condition, "incompatible input/output maps");
      planck_assert(in.Scheme() == RING, "input must be in RING ordering");
      planck_assert(out.Scheme() == RING, "output must be in RING ordering");
      planck_assert(interpolate != 0, "CMB spectrum must have been initialized");
      int numUsedThreads;

      if (!transposedLensing) {
#ifdef _OPENMP
#pragma omp parallel
#endif
	{
          long startpix, endpix;
#ifdef _OPENMP
	  startpix = omp_get_thread_num()*1L*out.Npix()/omp_get_num_threads();
	  endpix = (omp_get_thread_num()+1L)*out.Npix()/omp_get_num_threads();
#else
          startpix = 0;
          endpix = out.Npix();
#endif
 	  interpolate->interpolateMany(in, &ptgs[startpix], &out[startpix], endpix-startpix);
	}
      }
      else 
	{
#ifdef _OPENMP
#pragma omp parallel
#endif
	  {       
            int curThread;
#ifdef _OPENMP
	    numUsedThreads = omp_get_num_threads();
            curThread = omp_get_thread_num();
#else
            numUsedThreads = 1;
            curThread = 0;
#endif
	  
	    Healpix_Map<xcomplex<T> >& outmap = outmap_ring[curThread];
	  
	    long startpix = curThread*1L*in.Npix()/numUsedThreads;
	    long endpix = (curThread+1L)*in.Npix()/numUsedThreads;
	  
	    outmap.fill(0);
	    interpolate->interpolateManyTransposed(&in[startpix], outmap, &ptgs[startpix], endpix-startpix);
	  }
	  out.fill(0);
	  // Now reduce the maps
	  for (int m = 0; m < numUsedThreads; m++)
	    {
	      Healpix_Map<xcomplex<T> >& outmap = outmap_ring[m];

#ifdef _OPENMP
#pragma omp parallel for schedule(static)
#endif
	      for (long i = 0; i < out.Npix(); i++)
		out[i] += outmap[i];
	    }
	}

    }


    /**
     * This is the main function of the LensingOperatorFlints_Polarization class. This function 
     * handles the lensing and transposed lensing operation itself. The operator acts as direct
     * or transposed operator depending on the template parameter "transposedLensing". 
     * A number of sanity checks are run which may trigger an exception PlanckError if they
     * are not satisfied. The conditions are the following:
     *    - both input and output maps must be in RING scheme
     *    - the interpolator must have been initialized, i.e. #update_CMB_spectrum has been 
     *      called.
     *    - if transposedLensing is false, then the input map must have the resolution given by 
     *      input Nside at the construction of the LensingOperatorFlints_Polarization object (#LensingOperatorFlints_Polarization), and the output map must have the same resolution as indicated during this construction.
     *    - if transposedLensing is true, the resolutions are reversed.
     *
     * This function takes and produces standard (Q,U) maps.
     *
     * This function makes use of OpenMP to speed up the computation when it is possible. So 
     * do not call it inside an OpenMP loop.
     *
     * \sa LensingOperatorFlints_Scalar, update_CMB_spectrum
     */ 
    template<bool transposedLensing>
    void applyLensing(const Healpix_Map<T>& inQ, const Healpix_Map<T>& inU,
		      Healpix_Map<T>& outQ, Healpix_Map<T>& outU)
    {
      Healpix_Map<xcomplex<T> >& tmp1 = (transposedLensing) ? tmp_out : tmp_in;
      Healpix_Map<xcomplex<T> >& tmp2 = (transposedLensing) ? tmp_in : tmp_out;
      
#ifdef _OPENMP
#pragma omp parallel for schedule(static)
#endif
      for (long i = 0; i < tmp_in.Npix(); i++)
	{
	  tmp1[i] = xcomplex<T>(inQ[i], inU[i]);
	}

      applyLensing<transposedLensing>(tmp1, tmp2);

#ifdef _OPENMP
#pragma omp parallel for schedule(static)
#endif
      for (long i = 0; i < tmp_out.Npix(); i++)
	{
	  outQ[i] = tmp2[i].real();
	  outU[i] = tmp2[i].imag();
	}
    }

  };


};
#endif
