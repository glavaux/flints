/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/cholesky_lapack.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/


#ifndef __CHOLESKY_SOLVE_HPP
#define __CHOLESKY_SOLVE_HPP

#include <cmath>
#include <cassert>
#include <arr.h>
#include "fortran_interface.hpp"

extern "C" void F77_FUNC(dpotrf)(char *uplo, int *N, double *A, int *lda, int *info);

namespace CMB {

  template<typename T>
  T square_cholesky(T a) { return a*a;}

  int do_cholesky_decomposition(arr<double>& M, int N, double precision = 0)
  {
    char uplo = 'U';
    int info;
   
    F77_FUNC(dpotrf)(&uplo, &N, &M[0], &N, &info);
    assert(info >= 0);
    return (info != 0) ? -1 : 0;      
  }

  /**
   * This function does the inversion of L (as decomposed by do_cholesky)
   */
  template<typename T>
  void do_cholesky_inversion(arr<T>& cho, int N)
  {
    for (int i = 0; i < N; i++)
      {
	cho[N*i+i] = 1/cho[i*N+i];
	for (int j = (i+1); j < N; j++)
	  {
	    T sum = 0;
	    for (int k = i; k < j; k++)
	      sum -= cho[j*N+k]*cho[k*N+i];
	    cho[j*N+i] = sum/cho[j*N+j];
	  }
      }    
  }

  /**
   * This function does the multiplication L^{t} * L
   */
  template<typename T>
  void do_cholesky_multiplication(const arr<T>& cho, arr<T>& M, int N)
  {
    for (int i = 0; i < N; i++)
      {
	for (int j = i; j < N; j++)
	  {
	    T sum = 0;
	    for (int k = j; k < N; k++)
	      {
		sum += cho[k*N+i]*cho[k*N+j];
	      }
	    M[i*N+j] = M[j*N+i] = sum;
	  }
      }
  }

  template<typename T>
  void do_cholesky_rev_multiplication(const arr<T>& cho, arr<T>& M, int N)
  {
    for (int i = 0; i < N; i++)
      {
	for (int j = i; j < N; j++)
	  {
	    T sum = 0;
	    for (int k = 0; k <= i; k++)
	      {
		sum += cho[i*N+k]*cho[j*N+k];
	      }
	    M[i*N+j] = M[j*N+i] = sum;
	  }
      }
  }

  template<typename T>
  void cholesky_solve_0(const arr<T>& cho, arr<T>& col0)
  {
    int N = col0.size();

    assert(cho.size() == N*N);

    col0[0] = 1./cho[0];

    // Solve for L y = (1 0 0...)
    for (int i = 1; i < N; i++)
      {
	T result = 0;
	const T *ChoLine = &cho[i*N];
	T *c0 = &col0[0];

	for (int k = i-1; k >=0; k--)
	  {
	    result -= (*ChoLine)*(*c0);
	    ChoLine++;
	    c0++;
	  }
	*c0 = result/(*ChoLine);
      }

    // Solve for L^t x = y
    for (int i = N-1; i >= 0; i--)
      {
	__builtin_prefetch(&cho[(i-1)*N+i]);
	T result = col0[i];
	T diag = cho[i*N+i];
	const T *ChoLine = &cho[(i+1)*N + i];
	T *c0 = &col0[i+1];

	for (int k = i+1; k<N; k++)
	  {
	    result -= (*c0)*(*ChoLine);
	    c0++;
	    ChoLine+=N;
	  }

	col0[i] = result/diag;
      }
  }

};

#endif
