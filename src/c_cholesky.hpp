/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/c_cholesky.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/


#ifndef __COMPLEX_CHOLESKY_SOLVE_HPP
#define __COMPLEX_CHOLESKY_SOLVE_HPP

#include <cmath>
#include <cassert>
#include <arr.h>
#include <xcomplex.h>

namespace CMB {

  template<typename T>
  int do_c_cholesky_decomposition(arr<xcomplex<T> >& M, int N)
  {
    int invalid = 0;
    for (int i = 0; i < N; i++)
      {
	T diag;

	{
	  // The diagonal part
	  T result = M[i*N+i].real();
	  for (int k = i-1; k>=0; k--)
	    result -= norm(M[i*N+k]);

	  // Instead of having to branch
	  // we do simple arithmetic. Branching
	  // is delayed to the end of the function.
	  invalid += std::signbit(result);

	  diag = sqrt(std::max(T(0),result));
	  M[i*N+i] = xcomplex<T>(diag, 0);
	}

	for (int j = i+1; j < N; j++)
	  {
	    xcomplex<T> result = conj(M[i*N+j]);
	    for (int k = i-1; k>=0; k--)
	      result -= conj(M[i*N+k])*M[j*N+k];

	    M[j*N+i] = xcomplex<T>(result.real()/diag, result.imag()/diag);
	  }
      }
    return invalid ? -1 : 0;
  }

  /**
   * This function does the inversion of L (as decomposed by do_cholesky)
   */
  template<typename T>
  void do_c_cholesky_inversion(arr<xcomplex<T> >& cho, int N)
  {
    for (int i = 0; i < N; i++)
      {
	cho[N*i+i] = xcomplex<T>(1/cho[i*N+i].real(),0);
	for (int j = (i+1); j < N; j++)
	  {
	    xcomplex<T> sum = 0;
	    for (int k = i; k < j; k++)
	      sum -= cho[j*N+k]*cho[k*N+i];
	   
	    T diag = cho[j*N+j].real();
	    cho[j*N+i] = xcomplex<T>(sum.real()/diag, sum.imag()/diag);
	  }
      }    
  }

  /**
   * This function does the multiplication L^{t} * L
   */
  template<typename T>
  void do_c_cholesky_multiplication(const arr<xcomplex<T> >& cho, arr<xcomplex<T> >& M, int N)
  {
    for (int i = 0; i < N; i++)
      {
	for (int j = i; j < N; j++)
	  {
	    xcomplex<T> sum = 0;
	    for (int k = j; k < N; k++)
	      {
		sum += conj(cho[k*N+i])*cho[k*N+j];
	      }
            if (i!=j)
              {
                M[i*N+j] = sum;
                M[j*N+i] = conj(sum);
              }
            else
              {
                M[i*N+i] = sum.real();
              }
          }
      }
  }

  template<typename T>
  void do_c_cholesky_rev_multiplication(const arr<xcomplex<T> >& cho, arr<xcomplex<T> >& M, int N)
  {
    for (int i = 0; i < N; i++)
      {
	for (int j = i; j < N; j++)
	  {
	    xcomplex<T> sum = 0;
	    for (int k = 0; k <= i; k++)
	      {
		sum += cho[i*N+k]*conj(cho[j*N+k]);
	      }
	    M[i*N+j] = sum;
            M[j*N+i] = conj(sum);
	  }
      }
  }

};

#endif
