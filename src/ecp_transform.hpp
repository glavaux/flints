/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/ecp_transform.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#ifndef __ECP_ALM_TOOLS_HPP
#define __ECP_ALM_TOOLS_HPP

#include <alm.h>
#include <xcomplex.h>
#include "ecp_map.hpp"

template<typename T>
void ecp_alm2map(const Alm<xcomplex<T> >& alms, ECP_Map<T>& m);

template<typename T>
void ecp_alm2map_der1(const Alm<xcomplex<T> >& alms, ECP_Map<T>& m, ECP_Map<T>& m_dtheta, ECP_Map<T>& m_dphi);

template<typename T>
void ecp_alm2map_pol(const Alm<xcomplex<T> >& almsG, const Alm<xcomplex<T> >& almsC, ECP_Map<T>& mQ, ECP_Map<T>& mU);

template<typename T>
void ecp_alm2map_pol(const Alm<xcomplex<T> >& almsT, const Alm<xcomplex<T> >& almsG, 
                     const Alm<xcomplex<T> >& almsC, 
                     ECP_Map<T>& mT, ECP_Map<T>& mQ, ECP_Map<T>& mU);


#endif
