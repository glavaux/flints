/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/qbinterpol.tcc
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/


#include "eigenvals.hpp"
#include <iomanip>

namespace CMB
{

  template<typename T,typename T2>
  void QuickInterpolateMap<T,T2>::compute_Pl_array(T2 x, arr<T2>& Pl) 
  {
    int k = 2;
    int n = Pl.size();
    
    if (n == 0)
      return;
    
    if (n >= 1)
      Pl[0] = 1;
    if (n >= 2)
      Pl[1] = x;
    
    for (int k = 2; k < n; k++)
      {
	Pl[k] = ((2*k-1)*x*Pl[k-1] - (k-1)*Pl[k-2])/k;
      }
  }

  template<typename T,typename T2>
  void QuickInterpolateMap<T,T2>::matrixError(const arr<T2>& Correlation, int npix)
  {
    std::ofstream mfile("matrix.txt");
    for (int i = 0; i < npix; i++)
      {
	for (int j = 0; j < npix; j++)
	  {
	    if (j >= i)
	      mfile << std::setprecision(30) << Correlation[i*npix+j] << std::endl;
	    else
	      mfile << std::setprecision(30) << Correlation[j*npix+i] << std::endl;
	  }
      }
    std::cout
      << "Error in covariance matrix decomposition. "
      << "Matrix dumped to 'matrix.txt'" << std::endl;
    abort();
  }
  
  template<typename T,typename T2>
  T2 QuickInterpolateMap<T,T2>::fullComputation(T2 x)
    throw (PlanckError)
  {
    arr<T2> result(lmax+1);

    compute_Pl_array(x, result);
    
    T2 accum = 0;
    assert(lmax == result.size()-1);
    assert(lmax == cls.size()-1);
    for (int l = 1; l <= lmax; l++)
      accum += (2*l+1)*result[l]*cls[l];
    accum += result[0]*cls[0];
    accum /= (4*M_PI);
    return accum/variance;
  }

  class InterpolateCacheError {};
  
  template<typename T,typename T2>
  void QuickInterpolateMap<T,T2>::precomputeLegendre(int numPoints)
    throw(PlanckError)
  {
    if (VERBOSE)
      std::cout << "Precomputing correlation function" << std::endl;
    
    delta = (1-cos(maxAngle))/(numPoints);
    precomputed_correlations.alloc(numPoints);

    try
      {
	if (USE_CACHE)
	  {
	    std::ifstream fcache("cache_legendre.dat");
	    if (!fcache)
	      throw InterpolateCacheError();

	    double d;
	    int n;
	    double maxA;

	    std::cout << "Cache detected" << std::endl;

	    fcache.read((char*)&d, sizeof(double));
	    fcache.read((char *)&n, sizeof(int));
	    fcache.read((char *)&maxA, sizeof(double));
	    if (d != delta || n != numPoints || maxA != maxAngle)
	      throw InterpolateCacheError();
	    arr<T2> xs;
	    xs.alloc(numPoints);
	    fcache.read((char *)&xs[0], sizeof(T2)*numPoints);
	    fcache.read((char *)&precomputed_correlations[0], sizeof(T2)*numPoints);
	    if (fcache.eof())
	      throw InterpolateCacheError();
	    
	    std::cout << "Cache loaded successfully" << std::endl;
	    std::cout << "Building cubic splines.." << std::endl;
            cubic = CubicSpline<T2>(xs,precomputed_correlations);
	    return;
	  }
      }
    catch (InterpolateCacheError e)
      {
	std::cout << "Error while opening cache. rebuilding it." << std::endl;
      }

    arr<T2> xs;
    xs.alloc(numPoints);
#pragma omp parallel for schedule(static)
    for (int i = 0; i < numPoints; i++)
      {
	xs[i] = delta*i;
	precomputed_correlations[i] = fullComputation(1-xs[i]);
      }
    if (USE_CACHE)
      {
	std::ofstream fcache("cache_legendre.dat");

	fcache.write((char *)&delta, sizeof(double));
	fcache.write((char *)&numPoints, sizeof(int));
	fcache.write((char *)&maxAngle, sizeof(double));
	fcache.write((char *)&xs[0], sizeof(double)*numPoints);
	fcache.write((char *)&precomputed_correlations[0], sizeof(double)*numPoints);
      }
    cubic = CubicSpline<T2>(xs,precomputed_correlations);
  }

  template<typename T,typename T2>
  void QuickInterpolateMap<T,T2>::setupMatrix(const arr<vec3>& all_vec, 
					      arr<T2>& M, const arr<int>& pixels)
    throw(PlanckError)
  {
    int npix = all_vec.size();

    for (int i = 0; i < npix; i++)
      {
	M[i*npix+i] = ((T2)1) + noiseRegulation;
        if (pixels[i] < 0) {
          for (int j = i+1; j < npix; j++)
            M[i*npix+j] = 0;
          continue;
        }
	
	for (int j = i+1; j < npix; j++)
	      {
                if (pixels[j] < 0)
                  {
                     M[i*npix + j] = 0;
                     continue;
                  }
		double cos_theta = dotprod(all_vec[i], all_vec[j]);
		//planck_assert(cos_theta*cos_theta <= 1, "Error, |cos(theta)| > 1");
		M[i*npix + j] = computeCorrelation(cos_theta);
	      }
      }
  }

  template<typename T,typename T2>
  bool QuickInterpolateMap<T,T2>::getMatrix(T2* restrict_flints C, int pix, arr<int>& pixel_list)
  {
    unsigned int ring = lastLevel.pix2ring(pix);
    unsigned int northRing;
    bool southCap;
    unsigned int startEquatorial = Nside+backupRings;
    T2 * restrict_flints cap, * restrict_flints backupPixels;
    bool * restrict_flints rMat, * restrict_flints rMatEx;
    bool regulated = false;

    if (ring > 2*Nside)
      {
	southCap = true;
	northRing = 4*Nside-ring;
      }
    else
      {
	southCap = false;
	northRing = ring;
      }

    T2 * restrict_flints v;

    if (northRing >= startEquatorial)
      {
        v = &equatorialRings[(ring-startEquatorial)*numMatrixElements];
//        regulated = regulatedEq[ring-startEquatorial];
      }
    else 
     if (northRing >= Nside)
      {
        unsigned int startpix, onring;
        unsigned int equatorialStartPix;

        // We are still in the equatorial region but in the south cap.
        // Because of the peculiar geometry of pixels on the boundary 
        // we have to consider them as part of the cap.
        equatorialStartPix = (northRing-Nside)*Nside;
        startpix = 4*(capPixels + equatorialStartPix);
        onring = (pix-startpix) % Nside;
	backupPixels = southCap ? &extraRingsS[0] : &extraRingsN[0];
        v = &backupPixels[(onring + equatorialStartPix)*numMatrixElements];
//        regulated = rMatEx[onring+equatorialStartPix];
      }
    else
      {
        cap = southCap ? &this->southCap[0] : &this->northCap[0];

       unsigned int startpix0 = northRing*(northRing-1)/2;
       unsigned int ringpix = 4*northRing;
       unsigned int startpix = (southCap) ? (lastLevel.Npix() - 4*startpix0 - ringpix) : 4*startpix0;
       unsigned int onring = (pix-startpix) % northRing;

       v = &cap[(onring + startpix0)*numMatrixElements];
//       regulated = rMat[onring+startpix0];
      }
    upMatrix<false>(v, C, numNeighbours);
    return regulated;
  }

  template<typename T,typename T2>
  T QuickInterpolateMap<T,T2>::interpolate(const Healpix_Map<T>& map,
					      const pointing& ptgs,
					      int *npix, T *var)
    throw (PlanckError)
  {
    T var0;
    int npix0;
    T result0;

    interpolateMany(map, &ptgs, &npix0, &var0, &result0, 1);

    if (npix)
      *npix = npix0;
    if (var)
      *var = var0;

    return result0;
  }

  template<typename T,typename T2>
  void QuickInterpolateMap<T,T2>::interpolateMany(const Healpix_Map<T>& map,
                                              const pointing * ptgs,
                                              T *var, T *result,
                                              int numDirections)
    throw (PlanckError)
   {
     interpolateManyBase<false,true>(map, ptgs, 0, var, result, numDirections);
   }

  template<typename T,typename T2>
  void QuickInterpolateMap<T,T2>::interpolateMany(const Healpix_Map<T>& map,
                                              const pointing *ptgs,
                                              int *npix, T *result,
                                              int numDirections)
    throw (PlanckError)
   {
     interpolateManyBase<true,false>(map, ptgs, npix, 0, result, numDirections);
   }

  template<typename T,typename T2>
  void QuickInterpolateMap<T,T2>::interpolateMany(const Healpix_Map<T>& map,
                                              const pointing *ptgs,
                                              int *npix, T *var, T *result,
                                              int numDirections)
    throw (PlanckError)
   {
     interpolateManyBase<true,true>(map, ptgs, npix, var, result, numDirections);
   }

  template<typename T,typename T2>
  void QuickInterpolateMap<T,T2>::interpolateMany(const Healpix_Map<T>& map,
                                              const pointing *ptgs,
                                              T *result,
                                              int numDirections)
    throw (PlanckError)                         
   {                                              
     interpolateManyBase<false,false>(map, ptgs, 0, 0, result, numDirections);
   }

  template<typename T,typename T2> template<bool doNpix>
  void QuickInterpolateMap<T,T2>::interpolateManyTransposedBase(const T * restrict_flints mapValue,
							Healpix_Map<T>& outMap,
							const pointing * restrict_flints ptgs,
							int numDirections,
							int *npix)
    throw  (PlanckError)
  {
    if (DO_EXTRA_CHECKING)
      {
        if (outMap.Nside() != lastLevel.Nside())
          throw PlanckError("Incompatible Nside used for outMap");
        if (outMap.Scheme() != RING)
          throw PlanckError("outMap must be in RING order");
      }
    arr<int> pixel_list(numNeighbours);
    arr<T2> workSpace(numNeighbours*numNeighbours+numNeighbours*2);
    T2 * restrict_flints CorrelationNgb = &workSpace[0];
    T2 * restrict_flints localCorrelation = &workSpace[numNeighbours*numNeighbours];
    T2 * restrict_flints Correlation_vector = &workSpace[numNeighbours*numNeighbours+numNeighbours];
    T2 geomVariance;
 
    for (int i = 0 ; i < numNeighbours; i++)
      localCorrelation[i] = 0;
    
    for (long q = 0; q < numDirections; q++) {
      vec3 vInterpolate = ptgs[q];
      int pix = lastLevel.ang2pix(ptgs[q]);

      // We discover the neighbours                                                                                                     
      discoverNeighbours(pix, pixel_list);
      
      // First we extract the geometry matrix from the cache                                                                            
      getMatrix(CorrelationNgb, pix, pixel_list);
      
      // Setup the correlation between the direction to interpolate                                                                     
      // and the direction of the pixels of the mesh.                                                                                   
      for (int i = 0; i < numNeighbours; i++)
	{
	  int pix = pixel_list[i];
	  // The only thing is we do not want is                      
	  // localCorrelation[i] == infinity or Nan
	  // however this is impossible:
	  //    (1) we cleaned up the buffer when entering the function
	  //    (2) we are only putting valid numbers with computeCorrelation
	  if (pix < 0)
	    continue;
	  
	  T2 cos_theta = dotprod(vInterpolate, directions[pixel_list[i]]);
	  localCorrelation[i] = computeCorrelation(cos_theta);
	}
      // 12.15                                                                                                                                
      CMB_Interpolation::compute_line<T2,false>(CorrelationNgb, localCorrelation,
				      Correlation_vector, geomVariance,
				      numNeighbours);
      // 16.74                                                                                                                                
      // Compute the scalar product                        
      T localVal = mapValue[q];
      for (int i = 0; i < numNeighbours; i++)
	{
	  long pix = pixel_list[i];
	  if (pix < 0)
	    continue;
	  outMap[pix]  += Correlation_vector[i]*localVal;
	}
      if (doNpix) {
	int n = 0;
	for (int i = 0 ; i < numNeighbours; i++)
	  n += (pixel_list[i] >= 0) ? 1 : 0;
	npix[q] = n;
      }
    }
  }
  
  template<typename T,typename T2> template<bool doNpix,bool doVar>
  void QuickInterpolateMap<T,T2>::interpolateManyBase(const Healpix_Map<T>& map,
						      const pointing * restrict_flints ptgs, 
						      int *npix, T *var, T *result,
						      int numDirections)
    throw (PlanckError)
  {
    arr<int> pixel_list(numNeighbours);
    arr<T2> workSpace(numNeighbours*numNeighbours+numNeighbours*2);
    T2 * restrict_flints CorrelationNgb = &workSpace[0];
    T2 * restrict_flints localCorrelation = &workSpace[numNeighbours*numNeighbours];
    T2 * restrict_flints Correlation_vector = &workSpace[numNeighbours*numNeighbours+numNeighbours];
    T2 geomVariance;
    
    for (int i = 0 ; i < numNeighbours; i++)
      localCorrelation[i] = 0;
    for (int q = 0 ; q < numDirections; q++) {
      vec3 vInterpolate = ptgs[q];
      int pix = map.ang2pix(ptgs[q]);
      T accum_value = 0;

      // We discover the neighbours
      discoverNeighbours(pix, pixel_list);
      
      // First we extract the geometry matrix from the cache
      bool regulated = getMatrix(CorrelationNgb, pix, pixel_list);
      
      // Setup the correlation between the direction to interpolate 
      // and the direction of the pixels of the mesh.
      for (int i = 0; i < numNeighbours; i++)
	{
          int pix = pixel_list[i];
	  // The only thing is we do not want is
	  // localCorrelation[i] == infinity or Nan
	  // however this is impossible:
	  //    (1) we cleaned up the buffer when entering the function
	  //    (2) we are only putting valid numbers with computeCorrelation
          if (pix < 0) 
	    continue;
	    
          T2 cos_theta = dotprod(vInterpolate, directions[pix]);
          localCorrelation[i] = computeCorrelation(cos_theta);
	}
// 12.15
      
      CMB_Interpolation::compute_line<T2,doVar>(CorrelationNgb, localCorrelation, 
				      Correlation_vector, geomVariance, 
				      numNeighbours);
// 16.74
      
      // Compute the scalar product
      for (int i = 0; i < numNeighbours; i++)
        {
          int pix = pixel_list[i];
          if (pix < 0)
            continue;
	  accum_value += Correlation_vector[i]*map[pix];
       }
      if (doVar && geomVariance < 0)
        {
#if 0
          if (fabs(geomVariance) > SMALL_ERROR)
            {
              std::cerr << "Problem in variance estimation of the interpolator. (Variance is =" << geomVariance << ")" << std::endl;
              std::cerr << "Pixel is " << pix << std::endl;
	      //              abort();
            }
#endif
	if (DO_DIAGNOSIS) {
		std::ofstream  f("statvarerr.txt", std::ios::app);
		f << geomVariance << " " << regulated << std::endl;
	 }
          geomVariance = 0;
        }
//15.52
      
      if (doVar)
        var[q] = sqrt(variance*geomVariance);
      result[q] = accum_value;
      if (doNpix) {
        int n = 0;
        for (int i = 0 ; i < numNeighbours; i++)
          n += (pixel_list[i] >= 0) ? 1 : 0;
        npix[q] = n;
      }

      assert(!std::isnan(accum_value));
    }
  }

  
  template<typename T, typename T2>
  void QuickInterpolateMap<T,T2>::discoverNeighbours(long pix, arr<int>& pixels)
   {
     // If we are at the base level, no other 
     // computations are necessary. Grab the neighbours.
     if (interpolateLevel == 0)
       {
	 fix_arr<int,8> subpix;
	 lastLevel.neighbors(pix, subpix);
	 pixels[0] = pix;
	 memcpy(&pixels[1], &subpix[0], sizeof(int)*8);
	 return;
       }

     int extraPixels = 1 << (2*interpolateLevel);
     int basepix = (lastLevel.ring2nest(pix) >> (2*interpolateLevel));
     fix_arr<int,8> sub_basepix;
     baseLevel.neighbors(basepix, sub_basepix);
     
     for (int j = 0; j < extraPixels; j++)
       pixels[j] = lastLevel.nest2ring(basepix*extraPixels + j);

     // Generate the pixel number in RING mode
     for (int i = 0; i < 8; i++)
       {
	 if (sub_basepix[i] == -1)
	   {
	     for (int j = 0; j<extraPixels; j++)
	       pixels[extraPixels*(i+1)+j] = -1;
	     continue;
	   }
	 
	 int bpix = sub_basepix[i]*extraPixels;
	 for (int j = 0; j < extraPixels; j++)
	   pixels[extraPixels*(i+1)+j] = lastLevel.nest2ring(bpix + j);
       }
   }

  template<typename T, typename T2>
  bool QuickInterpolateMap<T,T2>::computeMatrix(int pix,
						arr<int>& pixels,
						arr<vec3>& all_vec,
						arr<T2>& cho,
						arr<T2>& M,
						T2 *out)
  {
    discoverNeighbours(pix, pixels);

    for (int j = 0; j < numNeighbours; j++)
      if (pixels[j] >= 0)
	all_vec[j] = directions[pixels[j]];

    setupMatrix(all_vec, cho, pixels);

    bool regulated = false;

    int err;

    err = do_cholesky_decomposition<T2,arr<T2> >(cho, numNeighbours);
    if (err < 0)
      {
        arr<T2> cho_save;

        noiseRegulation = TYPE_INFO<T2>::one_sqrt_precision;
        setupMatrix(all_vec, cho, pixels);
	err = do_cholesky_decomposition<T2, arr<T2> >(cho, numNeighbours);
	if (err < 0)
	  {
	    setupMatrix(all_vec, cho, pixels);
	    matrixError(cho, numNeighbours);
	  }
	regulated = true;
      }

    do_cholesky_inversion(cho, numNeighbours);
    do_cholesky_multiplication(cho, M, numNeighbours);

    for (int i = 0; i < numNeighbours; i++)
      {
        for (int j = i+1; j < numNeighbours; j++)
          {
	    M[i*numNeighbours+j] /= (((T2)1) + noiseRegulation);
	    M[j*numNeighbours+i] = conj(M[i*numNeighbours+j]);
          }
      }
    noiseRegulation = 0;

    upMatrix<true>(&M[0], out, numNeighbours);
    return regulated;
  }

  
  template<typename T, typename T2>
  void QuickInterpolateMap<T,T2>::precomputeGeometry()
    throw (PlanckError)
  {
    arr<int> pixels;
    arr<vec3> all_vec;
    arr<T2> M, cho;

    northCap.alloc(numMatrixElements*capPixels);
    southCap.alloc(numMatrixElements*capPixels);
    regulatedN.alloc(capPixels);
    regulatedS.alloc(capPixels);
    regulatedEq.alloc(2*(Nside-backupRings+1));
    regulatedExtraN.alloc(sizeExtra);
    regulatedExtraS.alloc(sizeExtra);
    equatorialRings.alloc(numMatrixElements*(2*(Nside-backupRings+1)));
    extraRingsN.alloc(numMatrixElements*sizeExtra);
    extraRingsS.alloc(numMatrixElements*sizeExtra);

    pixels.alloc(numNeighbours);
    all_vec.alloc(numNeighbours);
    M.alloc(numNeighbours*numNeighbours);
    cho.alloc(numNeighbours*numNeighbours);

    std::cout << "Computing pixelization statistical properties..." << std::endl;

    try {
      if (USE_CACHE) {
        int locNumNeighbours, locNumMatElements, locNside, locBackupRings;

        std::ifstream f("cache_geometry_scalar.dat");
        if (!f)
          throw InterpolateCacheError();

        f.read((char *)&locNumNeighbours, sizeof(int));
	f.read((char *)&locBackupRings, sizeof(int));
        f.read((char *)&locNumMatElements, sizeof(int));
        f.read((char *)&locNside, sizeof(int));
        if (locNside != lastLevel.Nside() || locNumNeighbours != numNeighbours ||
            locNumMatElements != numMatrixElements || locBackupRings != backupRings)
          throw InterpolateCacheError();
 	f.read((char *)&equatorialRings[0], sizeof(T2)*numMatrixElements*(2*(Nside-backupRings+1)));
	f.read((char *)&northCap[0], sizeof(T2)*numMatrixElements*capPixels);
	f.read((char *)&southCap[0], sizeof(T2)*numMatrixElements*capPixels);
        f.read((char *)&extraRingsN[0], sizeof(T2)*numMatrixElements*sizeExtra);
        f.read((char *)&extraRingsS[0], sizeof(T2)*numMatrixElements*sizeExtra);
	if (!f)
	  throw InterpolateCacheError();
        return;
      }
    } catch (const InterpolateCacheError & e) 
       {
          std::cerr << "Error loading cache. Rebuilding." << std::endl;
       }

    // Two steps for precomputing geometry
    // First we compute the pixelization on each ring in north cap (south cap is
    // just a mirror.
    // We need only one matrix for each ring in the equatorial plane

    int lastpct = -1;
    for (int ring = 0; ring <= 2*(Nside-backupRings); ring++)
      {
	int startpix, ringpix;
	double costheta, sintheta;
	bool shifted;

	lastLevel.get_ring_info(ring+Nside+backupRings, startpix, ringpix, costheta, sintheta, shifted);
	regulatedEq[ring] = computeMatrix(startpix, pixels, all_vec, cho, M, &equatorialRings[ring*numMatrixElements]);
      }

    // There is a pi/4 periodicity in the coefficient. We store only one fourth of each ring.
    // First ring is 1
    for (int r = 1; r < Nside; r++)
      {
	 bool shifted;
         int startpixN = 2*r*(r-1);
	 int ringpix = 4*r;
	 int southRing = 4*Nside-r;
	 int startpixS = lastLevel.Npix()-startpixN-ringpix;
         int startpix0 = startpixN/4;
         for (int i = 0; i < r; i++)
	   {
	     regulatedN[(startpix0+i)] = computeMatrix(startpixN+i, pixels, all_vec, cho, M,
			   &northCap[(startpix0+i)*numMatrixElements]);
	     regulatedS[startpix0+i] = computeMatrix(startpixS+i, pixels, all_vec, cho, M,
			   &southCap[(startpix0+i)*numMatrixElements]);
	   }
      }
    for (int r = 0; r < backupRings; r++)
      {
         int startpixN = 2*Nside*(Nside-1) + r*4*Nside;
	 int ringpix = 4*Nside;
	 int startpixS = lastLevel.Npix()-startpixN - ringpix;
         for (int i = 0; i < Nside; i++)
	   {
	     regulatedExtraN[Nside*r+i] = computeMatrix(startpixN+i, pixels, all_vec, cho, M, &extraRingsN[(Nside*r+i)*numMatrixElements]);
	     regulatedExtraS[Nside*r+i] = computeMatrix(startpixS+i, pixels, all_vec, cho, M, &extraRingsS[(Nside*r+i)*numMatrixElements]);
	   }
       }
 
    if (USE_CACHE) {
      std::ofstream f("cache_geometry_scalar.dat");
      if (!f)
         return;
      f.write((char *)&numNeighbours, sizeof(int));
      f.write((char *)&backupRings, sizeof(int));
      f.write((char *)&numMatrixElements, sizeof(int));
      f.write((char *)&Nside, sizeof(int));
      f.write((char *)&equatorialRings[0], sizeof(T2)*numMatrixElements*(2*(Nside-backupRings+1)));
      f.write((char *)&northCap[0], sizeof(T2)*numMatrixElements*capPixels);
      f.write((char *)&southCap[0], sizeof(T2)*numMatrixElements*capPixels);
      f.write((char *)&extraRingsN[0], sizeof(T2)*numMatrixElements*sizeExtra);
      f.write((char *)&extraRingsS[0], sizeof(T2)*numMatrixElements*sizeExtra);
    }
  }

};
