/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/lensing_operator_naive.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

/**
 * This file is part of FLINTS, for interpolating random fields on the sphere.
 * Copyright (C) 2010-2011 Guilhem Lavaux
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace CMB {

template<typename T,typename T2>
class LensingOperator
{
protected:
  Healpix_Map<T> deflection_phi, deflection_theta, phi_map,
    backup_deflection_phi, backup_deflection_theta;
  bool transposed;
  int Nside;
  arr<Healpix_Map<T> > outmap_ring;
  arr<pointing> ptgs, backup_ptgs;
public:
  LensingOperator(const arr<T2>& Cls, long nSide)
    : deflection_phi(nSide, RING, SET_NSIDE),
      deflection_theta(nSide, RING, SET_NSIDE),
      backup_deflection_theta(nSide, RING, SET_NSIDE),
      backup_deflection_phi(nSide, RING, SET_NSIDE),
      phi_map(nSide, RING, SET_NSIDE),
      Nside(nSide)
  {
    transposed = false;
    deflection_theta.fill(0);
    deflection_phi.fill(0);
    outmap_ring.alloc(omp_get_max_threads());
    for (int i = 0; i < outmap_ring.size(); i++)
      outmap_ring[i].SetNside(nSide, RING);
    ptgs.alloc(phi_map.Npix());
  }

  ~LensingOperator()
  {
  }

  void setTranspose(bool i) { transposed = i; }

  void update_CMB_spectrum(const arr<T2>& Cls)
  {
  }

  void saveField()
  {
    backup_deflection_theta = deflection_theta;
    backup_deflection_phi = deflection_phi;
    backup_ptgs = ptgs;
  }

  void restoreField()
  {
    deflection_phi = backup_deflection_phi;
    deflection_theta = backup_deflection_theta;
    ptgs = backup_ptgs;
  }

  void setLensingField(const Alm<xcomplex<T> >& lenspot)
  {
    cerr << " => changing deflection field..." << endl;
    alm2map_der1(lenspot, phi_map, deflection_theta, deflection_phi);
#pragma omp parallel for schedule(static)
    for (long i = 0; i < ptgs.size(); i++) {
        computeMovedDirection(deflection_theta[i],
  			      deflection_phi[i],
			      phi_map.pix2ang(i), ptgs[i]);
    }
    cerr << " <= done" << endl;
  }

  void setLensingField(const CMB_Map& lensing_potential)
  {
    int lmax_lens = 2*lensing_potential.Nside();
    arr<double> weights(lmax_lens);
    ALM_Map alms(lmax_lens, lmax_lens);
    
    weights.fill(1);
    
    cerr << " => computing deflection field..." << endl;
    cerr << "       harmonic space..." << endl;
    HEALPIX_method(lensing_potential, alms, weights);

    cerr << "       computing gradient at CMB resolution..." << endl;
    setLensingField(alms);
  }

  void update_CMB_spectrum(const arr<T2>& Cls)
  {
    cerr << "Somebody has requested to update the Cls assumed to achieve interpolation" << endl;
    cerr << " => recomputing pixelization..." << endl;
#ifdef USE_FLINTS
    if (interpolate)
      delete interpolate;
    interpolate = new QuickInterpolateMap<T,T2>(Cls, 1, Nside, 0);
#endif
  }

  template<bool transposedLensing>
  void applyLensing(const Healpix_Map<T>& in,
		    Healpix_Map<T>& out)
  {
    int numUsedThreads;

    if (!transposedLensing) {
#pragma omp parallel for schedule(static)
       for(long i = 0; i < out.Npix(); i++){
          pointing&p = ptgs[i];
          out[i] = in.interpolated_value(p);
       }
    } else {
#pragma omp parallel
        {
          if (omp_get_thread_num() == 0)
            numUsedThreads = omp_get_num_threads();

          Healpix_Map<T>& outmap = outmap_ring[omp_get_thread_num()];

          long startpix = omp_get_thread_num()*1L*outmap.Npix()/omp_get_num_threads();
          long endpix = (omp_get_thread_num()+1L)*outmap.Npix()/omp_get_num_threads();

          outmap.fill(0);
          for (long i = startpix; i < endpix; i++)
            {
               fix_arr<int,4> pix;
               fix_arr<double,4> wgt;
               pointing& p = ptgs[i];
               in.get_interpol(p, pix, wgt);
               for (int j = 0; j < 4; j++)
                 outmap[pix[j]] += wgt[j]*in[i];
            }
        }
        out.fill(0);
       // Now reduce the maps
        for (int m = 0; m < numUsedThreads; m++)
          {
            Healpix_Map<T>& outmap = outmap_ring[m];

#pragma omp parallel for schedule(static)
            for (long i = 0; i < out.Npix(); i++)
              out[i] += outmap[i];
          }

    }
  }
#endif

  void operator()(const Healpix_Map<T>& in,
		  Healpix_Map<T>& out)
  {
    if (transposed)      
      applyLensing<true>(in, out);
    else
      applyLensing<false>(in, out);
  }
};
