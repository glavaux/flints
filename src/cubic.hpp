/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/cubic.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/


#ifndef __CUBIC_HPP
#define __CUBIC_HPP

#include <cmath>
#include <arr.h>
#include "multi_healpix.hpp"
#include "inv.hpp"

namespace CMB
{
  template<typename T>
  struct SplineParam
  {
    T a, b, c, d;
  };

  template<typename T>
  class CubicSpline
  {
  private:
    arr<T> points;
    arr<T> pos;
    arr<SplineParam<T> > spline;
    T inv_delta;
    T origin;
    
  public:
    CubicSpline()
    {
    }
    
    CubicSpline(const arr<T>& xs, const arr<T>& data) throw(PlanckError)
      : pos(xs), points(data)
    {
      spline.alloc(data.size());
      planck_assert(data.size() == xs.size(), "The array of positions and samples must be conformal.");
      computeSpline();
      inv_delta = ((T)1.0)/(pos[1]-pos[0]);
      origin = pos[0];
    }

    ~CubicSpline()
    {
    }

    void computeSplineParameter(long i);
    void computeSpline();

    template<typename T2>
    static T2 cube(T2 a) { return a*a*a; }
    
    template<typename T2>
    static T2 square(T2 a) { return a*a; }

    int lookupX(T x) const
      throw(PlanckError)
    {
      int N = pos.size();
      int pmin = 1;
      int pmax = N;
      int pmid = N/2;

      //planck_assert(x <= pos[N-1], "Invalid x for lookup");
      if (x > pos[N-1])
	{
	  std::cerr << "Invalid x=" << x << " for lookup" << std::endl;
	  abort();
	}
      while (pmax-pmin > 1)
	{
	  if (x <= pos[pmid])
	    pmax = pmid;
	  else
	    pmin = pmid;
	  pmid = (pmin+pmax)/2;
	}
      return pmid;
    }

    tsize lookupX_regular(T x) const
      throw(PlanckError)
    {
      //      planck_assert(x <= pos[pos.size()-1], "Invalid x for lookup");
      return std::max(tsize((x-origin)*inv_delta),tsize(1L));
    }

    T compute(T x) const
      throw(PlanckError)
    {
      assert(x < pos[pos.size()-1]);
      
      int id = std::min(lookupX(x), pos.size()-2);
      
      T dx = x-pos[id];
      T x3 = cube(dx), x2 = square(dx);
      const SplineParam<T>& par = spline[id];

      return 
	x3 * par.a + x2 * par.b +
	dx * par.c + par.d;

    }

    T computeRegular(T x) const
      throw(PlanckError)
    {
      int id = std::min(lookupX_regular(x), pos.size()-2);
      T dx = x-pos[id];
      T x3 = cube(dx), x2 = square(dx);
      const SplineParam<T>& par = spline[id];

      return 
	x3 * par.a + x2 * par.b +
	dx * par.c + par.d;
    }
  };


  template<typename T>
  void CubicSpline<T>::computeSplineParameter(long i)
  {
    fix_arr<T,4*4> matrix_data;
    fix_arr<T,4*4> inv_matrix;
    T values[4],params[4];
    T pos1 = pos[i-1]-pos[i];
    T pos2 = pos[i+1]-pos[i];
    T pos3 = 0.5*pos1;
    T pos4 = 0.5*pos2;

    matrix_data[0*4 + 0] = cube(pos1);
    matrix_data[0*4 + 1] = square(pos1);
    matrix_data[0*4 + 2] = pos1;
    matrix_data[0*4 + 3] = 1;
    values[0] = points[i-1];

    matrix_data[1*4 + 0] = 3*square(pos3);
    matrix_data[1*4 + 1] = 2*pos3;
    matrix_data[1*4 + 2] = 1;
    matrix_data[1*4 + 3] = 0;
    values[1] = (points[i]-points[i-1])/(pos[i]-pos[i-1]);

    matrix_data[2*4 + 0] = cube(pos2);
    matrix_data[2*4 + 1] = square(pos2);
    matrix_data[2*4 + 2] = pos2;
    matrix_data[2*4 + 3] = 1;
    values[2] = points[i+1];

    matrix_data[3*4 + 0] = 3*square(pos4);
    matrix_data[3*4 + 1] = 2*pos4;
    matrix_data[3*4 + 2] = 1;
    matrix_data[3*4 + 3] = 0;
    values[3] = (points[i+1]-points[i])/(pos[i+1]-pos[i]);

    // We must now solve for X in  matrix * X = values
    inv4x4<fix_arr<T,4*4>, T>(matrix_data,inv_matrix);
    prod_vec(inv_matrix, values, params);

    spline[i].a = params[0];
    spline[i].b = params[1];
    spline[i].c = params[2];
    spline[i].d = params[3];
  }

  template<typename T>
  void CubicSpline<T>::computeSpline()
  {
    long Npts = points.size();

    for (long i = 1; i < Npts-1; i++)
      {
	computeSplineParameter(i);
      }
  }

};


#endif
