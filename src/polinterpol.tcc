/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/polinterpol.tcc
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/


#ifdef _OPENMP
#include <omp.h>
#else
#define omp_get_max_threads() 1
#define omp_get_thread_num() 0
#endif
#include <cmath>
#include <iomanip>
#include "wignerFunction.hpp"
#include "extra_complex.h"
#include "eigenvals.hpp"
#include "packMatrix.hpp"
#include "numerics.hpp"

namespace CMB
{  

  template<typename T,typename T2, int s>
  void QuickPolInterpolateMap<T,T2,s>::matrixError(const arr<xcomplex<T2> >& Correlation, int npix)
  {
    std::ofstream mfile_r("matrix_re.txt");
    std::ofstream mfile_i("matrix_im.txt");
    for (int i = 0; i < npix; i++)
      {
	for (int j = 0; j < npix; j++)
	  {
              mfile_r << std::setprecision(20) << Correlation[i*npix+j].real() << std::endl;
              mfile_i << std::setprecision(20) << Correlation[i*npix+j].imag() << std::endl;
          }
      }
    std::cout
      << "Error in covariance matrix decomposition. "
      << "Matrix dumped to 'matrix.txt'" << std::endl;
    abort();
  }
  
  class PolInterpolateCacheError {};
  
  template<typename T,typename T2,int s>
  void QuickPolInterpolateMap<T,T2,s>::doRealComputation()
    throw (PlanckError)
  {
    int numPoints = precomputed_correlations.size();
    arr<arr<T2> > local_corr(omp_get_max_threads());

    precomputed_correlations.fill(0);
   
   
#pragma omp parallel 
    {
      arr<T2> data(2*numPoints);
      int t = omp_get_thread_num();

      local_corr[t].alloc(numPoints);
      local_corr[t].fill(0);
#pragma omp for schedule(dynamic,10)
      for (int l = std::abs(s); l <= lmax; l++)
	{
	  // We compute d^l_{-s,-s}, because we are interested
	  // in spin s, (spin +s field)
	  computeWigner_D(l, -s, -s, 2*numPoints, &data[0]);
	  T2 prefac = (2*l+1)/(4*M_PI)*cls[l]; 
	  for (int i = 0; i < numPoints; i++)
	      local_corr[t][i] += prefac*data[i];
	}
    }

   
  for (int j = 0; j < local_corr.size(); j++)
    {
      if (local_corr[j].size() == 0)
        continue;
#pragma omp parallel for schedule(static)
      for (int i = 0; i < numPoints; i++)
        {
          precomputed_correlations[i] += local_corr[j][i];
        }
    }

  std::ofstream df("dataout.txt");
  for (int i = 0 ; i < numPoints; i++)
   df << M_PI*i/numPoints << " " << precomputed_correlations[i] << std::endl;
  }

  template<typename T,typename T2, int s>
  void QuickPolInterpolateMap<T,T2,s>::computeRotation(double theta, double phi,
						     double thetap, double phip,
						     double& alpha, double& beta, double& gamma)
  {
    // This is the extreme case. But we are handling a pixelization
    // on ring so that happens quite often...
    if (phi == phip)
      {
	if (theta < thetap)
	  {
	    alpha = gamma = 0;
	    beta = thetap-theta;
	  }
	else
	  {
	    alpha = gamma = M_PI;
	    beta = theta-thetap;
	  }
	return;
      }

    double ctheta = cos(theta), stheta = sin(theta);
    double ctheta_p = cos(thetap), stheta_p = sin(thetap);
    double c_delta_phi = cos(phi-phip);
    double sin_delta_phi = sin(phip-phi);
    double cphi = cos(phi), cphi_p = cos(phip);
    double sphi = sin(phi), sphi_p = sin(phip);

    double u[3] = {  stheta*cphi, stheta*sphi, ctheta };
    double v[3] = { stheta_p * cphi_p, stheta_p*sphi_p, ctheta_p };
    
    double u_n_2 = u[0]*u[0] + u[1]*u[1] + u[2]*u[2];
    double v_n_2 = v[0]*v[0] + v[1]*v[1] + v[2]*v[2];

    double cos_beta = (u[0]*v[0]+u[1]*v[1]+u[2]*v[2])/sqrt(u_n_2*v_n_2);
    double sin_beta = sqrt(1-cos_beta*cos_beta);

    double cos_alpha =
      -(ctheta_p*stheta*c_delta_phi - stheta_p*ctheta);
    // We do not actually divide by sin_beta here to avoid singularities...
    ///sin_beta; 
    // This improves the stability of the later atan2 inversion
    double sin_alpha =
      stheta*sin_delta_phi;

    // Same thing for cos_gamma
    double cos_gamma =
      (ctheta*stheta_p*c_delta_phi - stheta*ctheta_p);
    double sin_gamma =
      -stheta_p*sin_delta_phi;

    assert(std::abs(cos_alpha) <= 1);
    assert(std::abs(cos_gamma) <= 1);

    alpha = atan2(sin_alpha, cos_alpha);
    gamma = atan2(sin_gamma, cos_gamma);
    if (std::abs(cos_beta) > 1)
     {
       if (std::abs(cos_beta) > 1+1e-8)
	 std::cerr << "WARNING: cos(beta) > 1 (value is " << cos_beta-1 << ") , enforcing cos(beta) = 1." << std::endl;
       cos_beta = (cos_beta < -1) ? -1 : 1;
     }
    beta = acos(cos_beta);
  }

  template<typename T,typename T2, int s>
  void QuickPolInterpolateMap<T,T2,s>::precomputeCorrelations()
    throw(PlanckError)
  {
    int numPoints;

    if (VERBOSE)
      std::cout << "Precomputing correlation function" << std::endl;
    
    int minimalPoints = 2*M_PI/minAngle;
    if (minimalPoints < 2*lmax+1)
      minimalPoints = 2*lmax+1;

    std::cout << "Minimal number of points = " << minimalPoints <<  " lmax=" << lmax << std::endl;
    int il2 = 0;
    for (il2 = 0; minimalPoints > 0; il2++)
      minimalPoints /= 2;
    il2+=2;
    numPoints = 1 << il2;
    if (VERBOSE)
      std::cout << "Using " << numPoints << " points for the correlation" << std::endl;

    delta = M_PI/numPoints;

    precomputed_correlations.alloc(numPoints);

    try
      {
	if (USE_CACHE)
	  {
	    std::ifstream fcache("cache_pol_wigner_2.dat");
	    if (!fcache)
	      throw PolInterpolateCacheError();

	    double d;
	    int n;
	    double maxA;

	    std::cout << "Cache detected" << std::endl;

	    fcache.read((char*)&d, sizeof(double));
	    fcache.read((char *)&n, sizeof(int));
	    fcache.read((char *)&maxA, sizeof(double));
	    if (d != delta || n != numPoints || maxA != minAngle)
	      throw PolInterpolateCacheError();
	    arr<T2> xs;
	    xs.alloc(numPoints);
	    fcache.read((char *)&xs[0], sizeof(T2)*numPoints);
	    fcache.read((char *)&variance, sizeof(T2));
	    fcache.read((char *)&precomputed_correlations[0], sizeof(T2)*numPoints);
	    if (fcache.eof())
	      throw PolInterpolateCacheError();
	    
	    std::cout << "Cache loaded successfully" << std::endl;
	    std::cout << "Building cubic splines.." << std::endl;
            cubic = CubicSpline<T2>(xs,precomputed_correlations);
	    return;
	  }
      }
    catch (PolInterpolateCacheError e)
      {
	std::cout << "Error while opening cache. rebuilding it." << std::endl;
      }

    doRealComputation();

    variance = precomputed_correlations[0];
    arr<T2> xs;
    xs.alloc(numPoints);
    for (int i = 0; i < numPoints; i++)
      {
	xs[i] = delta*i;	
	precomputed_correlations[i] /= variance;
      }

    if (USE_CACHE)
      {
	std::ofstream fcache("cache_pol_wigner_2.dat");

	fcache.write((char *)&delta, sizeof(double));
	fcache.write((char *)&numPoints, sizeof(int));
	fcache.write((char *)&minAngle, sizeof(double));
	fcache.write((char *)&xs[0], sizeof(T2)*numPoints);
	fcache.write((char *)&variance, sizeof(T2));
	fcache.write((char *)&precomputed_correlations[0], sizeof(T2)*numPoints);
      }
    cubic = CubicSpline<T2>(xs,precomputed_correlations);
  }

  template<typename T,typename T2, int s>
  void QuickPolInterpolateMap<T,T2,s>::setupMatrix(const arr<pointing>& all_vec, 
						 arr<xcomplex<T2> >& M, 
						 const arr<int>& pixels)
    throw(PlanckError)
  {
    int npix = all_vec.size();

    for (int i = 0; i < npix; i++)
      {
	M[i*npix+i] = 1 + noiseRegulation;
        if (pixels[i] < 0) {
          for (int j = i+1; j < npix; j++)
            M[i*npix+j] = 0;
          continue;
        }
	
	for (int j = i+1; j < npix; j++)
	  {
	    if (pixels[j] < 0)
	      {
		M[i*npix + j] = 0;
		continue;
	      }
	    double alpha, beta, gamma;
	    computeRotation(all_vec[j].theta,
			    all_vec[j].phi,
			    all_vec[i].theta,
			    all_vec[i].phi,
			    alpha, beta, gamma);
	    double value1 = computeCorrelation(beta);
	    // We have a spin +s field. Thus the -s.
	    double epsilon = -s*(alpha+gamma);
	    
	    M[i*npix + j] = value1*xcomplex<T2>(cos(epsilon),sin(epsilon));
	    M[j*npix + i] = conj(M[i*npix + j]);
	  }
      }
  }

  template<typename T,typename T2, int s>
  void QuickPolInterpolateMap<T,T2,s>::getMatrix(xcomplex<T2>* C, T2& noise, int pix, arr<int>& pixel_list)
  {
    unsigned int ring = lastLevel.pix2ring(pix);
    unsigned int northRing;
    bool southCap;
    unsigned int startEquatorial = Nside+backupRings;
    xcomplex<T2> *cap, *backupPixels;
    T2 *noisecap, *noiseBackup;

    if (ring > 2*Nside)
      {
        southCap = true;
        northRing = 4*Nside-ring;
        cap = &this->southCap[0];
        noisecap = &this->noiseSouth[0];
        backupPixels = &extraRingsS[0];
        noiseBackup = &noiseExtraS[0];
      }
    else
      {
        southCap = false;
        northRing = ring;
        cap = &this->northCap[0];
        noisecap = &this->noiseNorth[0];
        backupPixels = &extraRingsN[0];
        noiseBackup = &noiseExtraN[0];
      }
 
    // In the south cap we have to permute some pixels
    xcomplex<T2> *v;

    if (northRing >= startEquatorial)
      {
        unsigned int matId = ring-startEquatorial;
        v = &equatorialRings[matId*numMatrixElements];
        noise = noiseEquatorialRings[matId];
      }
    else
    if (northRing >= Nside)
      {
        unsigned int startpix, onring;
        unsigned int equatorialStartPix;
        unsigned int matId;

        // We are still in the equatorial region but in the south cap.
        // Because of the peculiar geometry of pixels on the boundary 
        // we have to consider them as part of the cap.
        equatorialStartPix = (northRing-Nside)*Nside;
	startpix = 4*(capPixels + equatorialStartPix);
        onring = (pix-startpix) % Nside;
        matId = (onring + equatorialStartPix);
	v = &backupPixels[matId*numMatrixElements];
        noise = noiseBackup[matId];
      }
    else
      {
       unsigned int startpix0 = northRing*(northRing-1)/2;
       unsigned int ringpix = 4*northRing;
       unsigned int startpix = (southCap) ? (lastLevel.Npix() - 4*startpix0 - ringpix) : 4*startpix0;
       unsigned int onring = (pix-startpix) % northRing;

       unsigned int matId = onring + startpix0;
       v = &cap[matId*numMatrixElements];
       noise = noisecap[matId];
      }
    upMatrix<false>(v, C, numNeighbours);
  }

  template<typename T,typename T2, int s>
  xcomplex<T> QuickPolInterpolateMap<T,T2,s>::interpolate(const Healpix_Map<xcomplex<T> >& map,
							const pointing& ptgs,
							int *npix, T *var)
    throw (PlanckError)
  {
    T var0;
    int npix0;
    xcomplex<T> result0;

    interpolateMany(map, &ptgs, &npix0, &var0, &result0, 1);

    if (npix)
      *npix = npix0;
    if (var)
      *var = var0;

    return result0;
  }

  template<typename T,typename T2, int s>
  void QuickPolInterpolateMap<T,T2,s>::interpolateMany(const Healpix_Map<xcomplex<T> >& map,
						     const pointing *ptgs,
						     T *var, xcomplex<T> *result,
						     int numDirections)
    throw (PlanckError)
   {
     interpolateManyBase<false,true>(map, ptgs, 0, var, result, numDirections);
   }

  template<typename T,typename T2, int s>
  void QuickPolInterpolateMap<T,T2,s>::interpolateMany(const Healpix_Map<xcomplex<T> >& map,
                                              const pointing *ptgs,
                                              int *npix, xcomplex<T> *result,
                                              int numDirections)
    throw (PlanckError)
   {
     interpolateManyBase<true,false>(map, ptgs, npix, 0, result, numDirections);
   }

  template<typename T,typename T2, int s>
  void QuickPolInterpolateMap<T,T2,s>::interpolateMany(const Healpix_Map<xcomplex<T> >& map,
						     const pointing *ptgs,
						     int *npix, T *var, xcomplex<T> *result,
						     int numDirections)
    throw (PlanckError)
   {
     interpolateManyBase<true,true>(map, ptgs, npix, var, result, numDirections);
   }

  template<typename T,typename T2, int s>
  void QuickPolInterpolateMap<T,T2,s>::interpolateMany(const Healpix_Map<xcomplex<T> >& map,
                                              const pointing *ptgs,
                                              xcomplex<T> *result,
                                              int numDirections)
    throw (PlanckError)                         
   {                                              
     interpolateManyBase<false,false>(map, ptgs, 0, 0, result, numDirections);
   }
  
  template<typename T,typename T2, int s> template<bool doNpix,bool doVar>
  void QuickPolInterpolateMap<T,T2,s>::interpolateManyBase(const Healpix_Map<xcomplex<T> >& map,
						  const pointing *ptgs, 
						  int *npix, T *var, xcomplex<T> *result,
						  int numDirections)
    throw (PlanckError)
  {
    arr<int> pixel_list(numNeighbours);
    arr<xcomplex<T2> > workSpace(numNeighbours*numNeighbours+numNeighbours*2);
    xcomplex<T2> *CorrelationNgb = &workSpace[0];
    xcomplex<T2> *localCorrelation = &workSpace[numNeighbours*numNeighbours];
    xcomplex<T2> *Correlation_vector = &workSpace[numNeighbours*numNeighbours+numNeighbours];
    xcomplex<T2> geomVariance;
    
    for (int i = 0 ; i < numNeighbours; i++)
      localCorrelation[i] = 0;
    for (int q = 0 ; q < numDirections; q++) {
      pointing vInterpolate = ptgs[q];
      int pix = map.ang2pix(ptgs[q]);
      xcomplex<T> accum_value = 0;
      T2 noise;

      // We discover the neighbours
      discoverNeighbours(pix, pixel_list);
      
      // First we extract the geometry matrix from the cache
      getMatrix(CorrelationNgb, noise, pix, pixel_list);
      
      // Setup the correlation between the direction to interpolate 
      // and the direction of the pixels of the mesh.
      for (int i = 0; i < numNeighbours; i++)
	{
          int pixcur = pixel_list[i];
          if (pixcur < 0) 
            continue;

          double alpha, beta, gamma;
	  pointing& dir = directions[pixcur];
	  computeRotation(vInterpolate.theta,
			  vInterpolate.phi,
			  dir.theta,
			  dir.phi,
			  alpha, beta, gamma);
	  // Still looking at spin +s fields
	  double epsilon = -s*(alpha+gamma);

          localCorrelation[i] = computeCorrelation(beta)*xcomplex<T2>(cos(epsilon),sin(epsilon));
	}
// 12.15
      
      xcomplex<T2> regulT=xcomplex<T2>(noise,0);
      CMB_Interpolation::compute_line<xcomplex<T2>, doVar>(CorrelationNgb, localCorrelation, 
						Correlation_vector, geomVariance, 
						numNeighbours, regulT);
// 16.74
      
      // Compute the scalar product
      for (int i = 0; i < numNeighbours; i++)
	accum_value += conj(Correlation_vector[i])*map[pixel_list[i]];
      if (geomVariance.real() < 0)
	{
	  unsigned int ring = lastLevel.pix2ring(pix);
	  int startpix, ringpix;
	  double costheta, sintheta;
	  bool shifted;

	  lastLevel.get_ring_info(ring, startpix, ringpix, costheta, sintheta, shifted);
	  
	  if (geomVariance.real() < 0) {
//	  std::cerr << "geomVariance is negative (value is " << geomVariance.real() << "). This cannot happen. The coordinate of the affected pixel is:" << std::endl
//		    << "Ring=" << ring << " OnRing=" << pix-startpix << " pix=" << pix << std::endl
//		    << "backupRings=" << backupRings << " Nside=" << Nside << std::endl;
//	  	  abort();
                if (DO_DIAGNOSIS) {
		  std::ofstream f("statneg.txt", std::ios::app);
		  f << geomVariance.real() << std::endl;
               }
          }
	  geomVariance = 0;
	}
//15.52
      
      if (doVar)
        var[q] = sqrt(variance*geomVariance.real());
      result[q] = accum_value;
      if (doNpix) {
        int n = 0;
        for (int i = 0 ; i < numNeighbours; i++)
          n += (pixel_list[i] >= 0) ? 1 : 0;
        npix[q] = n;
      }

      assert(!std::isnan(accum_value.real()));
    }
  }



 template<typename T,typename T2, int s> template<bool doNpix,bool doVar>
  void QuickPolInterpolateMap<T,T2,s>::interpolateManyTransposedBase(const xcomplex<T> * restrict_flints mapValue,
								     Healpix_Map<xcomplex<T> >& outMap,
								     const pointing * restrict_flints ptgs, 
								     int *npix, T *var,
								     int numDirections)
    throw (PlanckError)
  {
    arr<int> pixel_list(numNeighbours);
    arr<xcomplex<T2> > workSpace(numNeighbours*numNeighbours+numNeighbours*2);
    xcomplex<T2> *CorrelationNgb = &workSpace[0];
    xcomplex<T2> *localCorrelation = &workSpace[numNeighbours*numNeighbours];
    xcomplex<T2> *Correlation_vector = &workSpace[numNeighbours*numNeighbours+numNeighbours];
    xcomplex<T2> geomVariance;
    
    for (int i = 0 ; i < numNeighbours; i++)
      localCorrelation[i] = 0;
    for (int q = 0 ; q < numDirections; q++) {
      pointing vInterpolate = ptgs[q];
      int pix = lastLevel.ang2pix(ptgs[q]);
      T2 nregul;

      // We discover the neighbours
      discoverNeighbours(pix, pixel_list);
      
      // First we extract the geometry matrix from the cache
      getMatrix(CorrelationNgb, nregul, pix, pixel_list);
      
      // Setup the correlation between the direction to interpolate 
      // and the direction of the pixels of the mesh.
      for (int i = 0; i < numNeighbours; i++)
	{
          int pixcur = pixel_list[i];
          if (pixcur < 0) 
            continue;

          double alpha, beta, gamma;
	  pointing& dir = directions[pixcur];
	  computeRotation(vInterpolate.theta,
			  vInterpolate.phi,
			  dir.theta,
			  dir.phi,
			  alpha, beta, gamma);
	  double epsilon = -s*(alpha+gamma);

          localCorrelation[i] = computeCorrelation(beta)*xcomplex<T2>(cos(epsilon),sin(epsilon));
	}
// 12.15
      
      xcomplex<T2> regulT=xcomplex<T2>(nregul, 0);
      CMB_Interpolation::compute_line<xcomplex<T2>, doVar>(CorrelationNgb, localCorrelation, 
						Correlation_vector, geomVariance, 
						numNeighbours, regulT);
// 16.74
      
      xcomplex<T> localVal = mapValue[q];
      // Compute the scalar product
      for (int i = 0; i < numNeighbours; i++)
	{
	  long pix = pixel_list[i];
	  if (pix < 0)
	    continue;

	  outMap[pix] += Correlation_vector[i]*localVal;
	}

      if (geomVariance.real() < 0)
	{
	  unsigned int ring = lastLevel.pix2ring(pix);
	  int startpix, ringpix;
	  double costheta, sintheta;
	  bool shifted;

	  lastLevel.get_ring_info(ring, startpix, ringpix, costheta, sintheta, shifted);
	  
	  if (geomVariance.real() < 0) {
//	  std::cerr << "geomVariance is negative (value is " << geomVariance.real() << "). This cannot happen. The coordinate of the affected pixel is:" << std::endl
//		    << "Ring=" << ring << " OnRing=" << pix-startpix << " pix=" << pix << std::endl
//		    << "backupRings=" << backupRings << " Nside=" << Nside << std::endl;
//	  	  abort();
                if (DO_DIAGNOSIS) {
		  std::ofstream f("statneg.txt", std::ios::app);
		  f << geomVariance.real() << std::endl;
               }
          }
	  geomVariance = 0;
	}
//15.52
      
      if (doVar)
        var[q] = sqrt(variance*geomVariance.real());
      if (doNpix) {
        int n = 0;
        for (int i = 0 ; i < numNeighbours; i++)
          n += (pixel_list[i] >= 0) ? 1 : 0;
        npix[q] = n;
      }
    }
  }

  
  template<typename T, typename T2, int s>
  void QuickPolInterpolateMap<T,T2,s>::discoverNeighbours(long pix, arr<int>& pixels)
  {
    // If we are at the base level, no other 
    // computations are necessary. Grab the neighbours.
    if (interpolateLevel == 0)
      {
	fix_arr<int,8> subpix;
	lastLevel.neighbors(pix, subpix);
	pixels[0] = pix;
	memcpy(&pixels[1], &subpix[0], sizeof(int)*8);
	return;
      }
    
    int extraPixels = 1 << (2*interpolateLevel);
    int basepix = (lastLevel.ring2nest(pix) >> (2*interpolateLevel));
    fix_arr<int,8> sub_basepix;
    baseLevel.neighbors(basepix, sub_basepix);
    
    for (int j = 0; j < extraPixels; j++)
      pixels[j] = lastLevel.nest2ring(basepix*extraPixels + j);
    
    // Generate the pixel number in RING mode
    for (int i = 0; i < 8; i++)
      {
	if (sub_basepix[i] == -1)
	  {
	    for (int j = 0; j<extraPixels; j++)
	      pixels[extraPixels*(i+1)+j] = -1;
	    continue;
	  }
	
	int bpix = sub_basepix[i]*extraPixels;
	for (int j = 0; j < extraPixels; j++)
	  pixels[extraPixels*(i+1)+j] = lastLevel.nest2ring(bpix + j);
      }
  }
  
  static void printMatrix(const char *base_fname, xcomplex<double> *M, int N)
  {
    std::string fr(base_fname), fi(base_fname);

    fr += "real";
    fi += "imag";
    std::ofstream f1(fr.c_str());
    std::ofstream f2(fi.c_str());

    for (int i = 0; i < N; i++)
      {
	for (int j = 0; j < N; j++)
	  {
	    f1 << M[i*N+j].real() << std::endl;
	    f2 << M[i*N+j].imag() << std::endl;
	  }
      }
  }

  template<typename T, typename T2, int s>
  void QuickPolInterpolateMap<T,T2,s>::computeMatrix(int pix,
						   arr<int>& pixels,
						   arr<pointing>& all_vec,
						   arr<xcomplex<T2> >& cho,
						   arr<xcomplex<T2> >& M,
						   xcomplex<T2> *out, T2* nregul)
  {
    discoverNeighbours(pix, pixels);

    for (int j = 0; j < numNeighbours; j++)
      if (pixels[j] >= 0)
	all_vec[j] = directions[pixels[j]];

    int err = -1;
    double nr = noiseRegulation;
    noiseRegulation = 1e-5;
    while (err < 0) {
      setupMatrix(all_vec, cho, pixels);
      err = do_c_cholesky_decomposition(cho, numNeighbours);
      if (err < 0)
        noiseRegulation *= 2;
    }
    *nregul = noiseRegulation;
    noiseRegulation = nr;
    do_c_cholesky_inversion(cho, numNeighbours);
    do_c_cholesky_multiplication(cho, M, numNeighbours);

    upMatrix<true>(&M[0], out, numNeighbours);
  }

  
  template<typename T, typename T2, int s>
  void QuickPolInterpolateMap<T,T2,s>::precomputeGeometry()
    throw (PlanckError)
  {
    arr<int> pixels;
    arr<pointing> all_vec;
    arr<xcomplex<T2> > M, cho;

    northCap.alloc(numMatrixElements*capPixels);
    southCap.alloc(numMatrixElements*capPixels);
    equatorialRings.alloc(numMatrixElements*(2*(Nside-backupRings)+1));
    extraRingsN.alloc(numMatrixElements*sizeExtra);
    extraRingsS.alloc(numMatrixElements*sizeExtra);
    noiseNorth.alloc(capPixels);
    noiseSouth.alloc(capPixels);
    noiseEquatorialRings.alloc(2*(Nside-backupRings)+1);
    noiseExtraN.alloc(sizeExtra);
    noiseExtraS.alloc(sizeExtra);

    pixels.alloc(numNeighbours);
    all_vec.alloc(numNeighbours);
    M.alloc(numNeighbours*numNeighbours);
    cho.alloc(numNeighbours*numNeighbours);

    std::cout << "Computing pixelization statistical properties..." << std::endl;

    try {
      if (USE_CACHE) {
        int locNumNeighbours, locNumMatElements, locNside, locBackupRings;

        std::ifstream f("cache_pol_geometry.dat");
        if (!f)
          throw PolInterpolateCacheError();

        f.read((char *)&locNumNeighbours, sizeof(int));
	f.read((char *)&locBackupRings, sizeof(int));
        f.read((char *)&locNumMatElements, sizeof(int));
        f.read((char *)&locNside, sizeof(int));
        if (locNside != lastLevel.Nside() || locNumNeighbours != numNeighbours ||
            locNumMatElements != numMatrixElements || locBackupRings != backupRings)
          throw PolInterpolateCacheError();
	f.read((char *)&equatorialRings[0], sizeof(xcomplex<T2>)*numMatrixElements*(2*(Nside-backupRings)+1));
	f.read((char *)&northCap[0], sizeof(xcomplex<T2>)*numMatrixElements*capPixels);
	f.read((char *)&southCap[0], sizeof(xcomplex<T2>)*numMatrixElements*capPixels);
        f.read((char *)&extraRingsN[0], sizeof(xcomplex<T2>)*numMatrixElements*sizeExtra);
        f.read((char *)&extraRingsS[0], sizeof(xcomplex<T2>)*numMatrixElements*sizeExtra);
	if (!f)
	  throw PolInterpolateCacheError();
        return;
      }
    } catch (const PolInterpolateCacheError & e) 
       {
          std::cerr << "Error loading cache. Rebuilding." << std::endl;
       }

    int lastpct = -1;
    for (int ring = 0; ring <= 2*(Nside-backupRings); ring++)
      {
        int startpix, ringpix;
        double costheta, sintheta;
        bool shifted;

        assert(ring < (2*(Nside-backupRings)+1));
        lastLevel.get_ring_info(ring+Nside+backupRings, startpix, ringpix, costheta, sintheta, shifted);
        computeMatrix(startpix, pixels, all_vec, cho, M, &equatorialRings[ring*numMatrixElements], &noiseEquatorialRings[ring]);
      }


    // There is a pi/4 periodicity in the coefficient. We store only one fourth of each ring.
    // First ring is 1
    // We also store the south cap at the moment. We need to investigate the use of the north-south
    // symmetry with complex correlation.
    for (int r = 1; r < Nside; r++)
      {
        double costheta, sintheta;
        bool shifted;
        int startpixN = 2*r*(r-1);
        int ringpix = 4*r;
        int southRing = 4*Nside-r;
        int startpixS = lastLevel.Npix()-startpixN-ringpix;
        int startpix0 = startpixN/4;
        for (int i = 0; i < r; i++)
          {
            assert(startpix0+i < capPixels);
            computeMatrix(startpixN+i, pixels, all_vec, cho, M,
                          &northCap[(startpix0+i)*numMatrixElements], &noiseNorth[startpix0+i]);
            computeMatrix(startpixS+i, pixels, all_vec, cho, M, 
                          &southCap[(startpix0+i)*numMatrixElements], &noiseSouth[startpix0+i]);
          }
      }

   for (int r = 0; r < backupRings; r++)
      {
        int startpixN = 2*Nside*(Nside-1) + r*4*Nside;
        int ringpix = 4*Nside;
        int startpixS = lastLevel.Npix()-startpixN - ringpix;
        for (int i = 0; i < Nside; i++)
          {
            assert(Nside*r+i < sizeExtra);
            computeMatrix(startpixN+i, pixels, all_vec, cho, M, &extraRingsN[(Nside*r+i)*numMatrixElements], &noiseExtraN[Nside*r+i]);
            computeMatrix(startpixS+i, pixels, all_vec, cho, M, &extraRingsS[(Nside*r+i)*numMatrixElements], &noiseExtraS[Nside*r+i]);
          }
        }

    if (USE_CACHE) {
      std::ofstream f("cache_pol_geometry.dat");
      if (!f)
         return;
      f.write((char *)&numNeighbours, sizeof(int));
      f.write((char *)&backupRings, sizeof(int));
      f.write((char *)&numMatrixElements, sizeof(int));
      f.write((char *)&Nside, sizeof(int));
      f.write((char *)&equatorialRings[0], sizeof(xcomplex<T2>)*numMatrixElements*(2*(Nside-backupRings)+1));
      f.write((char *)&northCap[0], sizeof(xcomplex<T2>)*numMatrixElements*capPixels);
      f.write((char *)&southCap[0], sizeof(xcomplex<T2>)*numMatrixElements*capPixels);
      f.write((char *)&extraRingsN[0], sizeof(xcomplex<T2>)*numMatrixElements*sizeExtra);
      f.write((char *)&extraRingsS[0], sizeof(xcomplex<T2>)*numMatrixElements*sizeExtra);
    }
  }

};
