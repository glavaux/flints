/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/polinterpol_ecp.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/


#ifndef __BAYES_QPOLINTERPOLATION_ECP_HPP
#define __BAYES_QPOLINTERPOLATION_ECP_HPP

#include <cmath>
#include <iostream>
#include <fstream>
#include <cstring>
#include <cassert>
#include <vector>
#include <vec3.h>
#include <arr.h>
#include "ecp_map.hpp"
#include "compiler_defs.hpp"
#include "extra_map_tools.hpp"
#include "cubic.hpp"
#include "c_cholesky.hpp"
#include "eigenvals.hpp"
#include "packMatrix.hpp"
#include "qinverse.hpp"
#include <xcomplex.h>
#include "numerics.hpp"

namespace CMB
{
  /** 
   * This is the polarization interpolation object. Actually it interpolates any spin-s field. 
   * The third template argument (here defaulting to 2) gives the 
   */
  template<typename T,typename T2 = T, int s = 2>
  class QuickPolInterpolateMap_ECP
  {
  private:
    static const int VERBOSE = 1;    
    static const bool USE_CACHE = false;
    static const bool DO_DIAGNOSIS = false;
    arr<T2> cls;
    T2 variance;
    int lmax;
    double delta, minAngle;
    arr<T2> precomputed_correlations;
    CubicSpline<T2> cubic;
    arr<pointing> directions;
    arr<xcomplex<T2> > equatorialRings; 
    arr<T2> noiseRegul;
    arr<bool> bad_matrix;
    ECP_Map_Base baseLevel;
    int Nrings, Nphi;
    int numNeighbours,numMatrixElements;
    int sizeExtra;
    T2 noiseRegulation;
    int interpolateLevel;
    std::map<T2,T2> correl_cache;
  public:
 
    /**
      * This is the constructor of the spin-s field interpolator. 
      */    
    QuickPolInterpolateMap_ECP(const arr<T2>& spectrum, 
                               int Nrings, int Nphi, double phi0 = 0,
                               double minAngle = -1, T2 regulation = 0)
      throw(PlanckError)
      : cls(spectrum), lmax(spectrum.size()-1),
        baseLevel(Nrings, Nphi, phi0)
    {
      if (minAngle < 0)
	minAngle = baseLevel.max_pixrad()*0.1;

      this->Nrings = Nrings;
      this->Nphi = Nphi;
      this->minAngle = minAngle;
      this->numNeighbours = 9;
      this->noiseRegulation = regulation;
      noiseRegulation = TYPE_INFO<T2>::one_sqrt_precision*10;
#ifdef MEMORY_DIET_PACK_MATRIX
      numMatrixElements = ((numNeighbours+1)/2+1)*numNeighbours;
#else
      numMatrixElements = numNeighbours*numNeighbours;
#endif
      precomputeCorrelations();
      precomputeDirections();
      precomputeGeometry();
   }
    
    ~QuickPolInterpolateMap_ECP() throw()
    {
    }
    
    /**
     * Do the real job of computing the value of the correlation function. 
     */
    void doRealComputation()
      throw (PlanckError);
    
    T2 computeOrExtract(T2 x);

    /** 
     * This function computes the Euler angle of the rotation to bring the direction
     * (theta,phi) to (thetap,phip). The angles are alpha,beta,gamma.
     */
    void computeRotation(double theta, double phi,
			 double thetap, double phip,
			 double& alpha, double& beta, double& gamma);
    
    /**
     * This function precomputes the pixelization statistical properties given the input spectrum.
     * It is called by the constructor.
     */
    void precomputeGeometry()
      throw (PlanckError);

    /** This function precomputes the 3d unit vector 
     * for each pixel. This allows us to do a lookup
     * instead of a serie of costly trigonometric operation.
     * It is called by the constructor.
     */
    void precomputeDirections()
    {
      directions.alloc(baseLevel.Npix());
      for (int i = 0; i < baseLevel.Npix(); i++)
	directions[i] = baseLevel.pix2ang(i);
    }

    /**
     * This function precomputes a cubic interpolation
     * of wigner functions polynomials. This will speed up
     * the interpolation algorithm while keeping
     * much of the precision (in practice nearly all).
     * It is called by the constructor.
     */
    void  precomputeCorrelations()
      throw(PlanckError);
    
    /**
     * This function computes the correlation function
     * using the cubic interpolation. This concerns
     * only the beta rotation of the correlation.
     */
    T2 computeCorrelation(double beta)
    {
      return cubic.computeRegular(beta);
    }

    /** 
     * This function computes the correlation matrix
     * between the given directions and the direction of interpolation.
     * The array must be preallocated and specified in \a M.
     */
    void setupMatrix(const arr<pointing>& all_vec, 
		     arr<xcomplex<T2> >& M, const arr<int>& valid_pix, T2 noiseReg)
      throw(PlanckError);

    bool getMatrix(xcomplex<T2>* C, T2 * nregul, int pix, arr<int>& pixel_list);

    bool computeMatrix(int pix, arr<int>& pixels, arr<pointing>& all_vec,
		       arr<xcomplex<T2> >& cho, arr<xcomplex<T2> >& M, xcomplex<T2> *geom, T2* nregul);

    /**
      * This function discover the list of neighbors of a given pixel.
      * The length depends on Nlevel, defined in the constructor.
      */
    void discoverNeighbours(long pix, arr<int>& pixels);
    
    /** 
     */     
    xcomplex<T> interpolate(const ECP_Map<xcomplex<T> >& map,
                            const pointing& ptg, int *npix, T *var)
      throw (PlanckError);
    
    template<bool doNpix, bool doVar>
    void interpolateManyBase(const ECP_Map<xcomplex<T> >& map, const pointing *ptgs, int *npix, T *var, xcomplex<T> *result, int numDirections)
       throw (PlanckError);

    /**
     * Function.
     */
    void interpolateMany(const ECP_Map<xcomplex<T> >& map, const pointing *ptgs, xcomplex<T> *result, int numDirections)
      throw (PlanckError);

    /**
     */
    void interpolateMany(const ECP_Map<xcomplex<T> >& map, const pointing *ptgs, T *var, xcomplex<T> *result, int numDirections)
      throw (PlanckError);

    /**
     */
    void interpolateMany(const ECP_Map<xcomplex<T> >& map, const pointing *ptgs, int *npix, xcomplex<T> *result, int numDirections)
      throw (PlanckError);
    void interpolateMany(const ECP_Map<xcomplex<T> >& map, const pointing *ptgs, int *npix, T *var, xcomplex<T> *result, int numDirections)
      throw (PlanckError);


    template<bool doNpix, bool doVar>
    void interpolateManyTransposedBase(const xcomplex<T>* restrict_flints mapValue, ECP_Map<xcomplex<T> >& outmap, const pointing *ptgs, int *npix, T *var, int numDirections)
       throw (PlanckError);


    void interpolateManyTransposed(const xcomplex<T> * restrict_flints mapValue, ECP_Map<xcomplex<T> >& outmap, const pointing * restrict_flints ptgs, int numDirections) 
      throw (PlanckError) { interpolateManyTransposedBase<false,false>(mapValue, outmap, ptgs, 0, 0, numDirections); }

    void matrixError(const arr<xcomplex<T2> >& Correlation, int n);

    template<bool doVar>
    void do_basic_interpolation(const ECP_Map<xcomplex<T> >& map,
                                const pointing& ptg, xcomplex<T>& val, 
                                double& err);

    template<bool doVar>
    void do_basic_interpolation_transposed(const xcomplex<T>& map_value,
                                           const pointing& ptg, 
                                           ECP_Map<xcomplex<T> >& val, double& err);
  };    
};

#include "polinterpol_ecp.tcc"

#endif
