/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/ecp_fitsio.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#ifndef __ECP_FITSIO_HPP
#define __ECP_FITSIO_HPP

#include <fitshandle.h>
#include <datatypes.h>
#include "ecp_map.hpp"

template<typename T>
void write_ecp_map(fitshandle& h, const ECP_Map<T>& m, PDT datatype);
template<typename T>
void write_ecp_map(fitshandle& h, const ECP_Map<T>& m, const ECP_Map<T>& mQ, const ECP_Map<T>& mU, PDT datatype);


#endif
