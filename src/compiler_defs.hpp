/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/compiler_defs.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#ifndef FLINTS_CDEFS
#define FLINTS_CDEFS

#ifdef __GNUC__
/* Enable the use of restrict_flints keyword for further optimizations */
#define restrict_flints __restrict
#else
#define restrict_flints
#endif

#endif
