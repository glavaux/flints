/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/multi_healpix.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#ifndef __FLINTS_MULTI_HEALPIX_HPP
#define __FLINTS_MULTI_HEALPIX_HPP

#include "flints_healpix_version.hpp"
#include "datatypes.h"

#if defined(BEFORE_HEALPIX_2_20)
#include <message_error.h>
class PlanckError: public Message_error
{
public:
  PlanckError(const std::string& msg)
    : Message_error(msg) {}
};

typedef int PDT;
typedef long tsize;

template<typename T> PDT planckType()
{
  return typehelper<T>::id;
}

static int FLINTS_convert_to_fitsio(PDT t)
{
  return type2ftc(t);
}

#else
#include <error_handling.h>

static inline PDT FLINTS_convert_to_fitsio(PDT t)
{
  return t;
}

#endif

#endif
