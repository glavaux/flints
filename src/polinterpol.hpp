/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/polinterpol.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/


#ifndef __BAYES_QPOLINTERPOLATION_HPP
#define __BAYES_QPOLINTERPOLATION_HPP

#include <cmath>
#include <iostream>
#include <fstream>
#include <cstring>
#include <cassert>
#include <vector>
#include <vec3.h>
#include <arr.h>
#include <healpix_map.h>
#include "multi_healpix.hpp"
#include "compiler_defs.hpp"
#include "extra_map_tools.hpp"
#include "cubic.hpp"
#include "c_cholesky.hpp"
#include "eigenvals.hpp"
#include "packMatrix.hpp"
#include "qinverse.hpp"
#include <xcomplex.h>

namespace CMB
{
  /** 
   * This is the polarization interpolation object. Actually it interpolates any spin-s field. 
   * The third template argument (here defaulting to 2) gives the 
   */
  template<typename T,typename T2 = T, int s = 2>
  class QuickPolInterpolateMap
  {
  private:
    static const int VERBOSE = 1;    
    static const bool USE_CACHE = false;
    static const bool DO_DIAGNOSIS = false;
    arr<T2> cls;
    T2 variance;
    int lmax;
    double delta, minAngle;
    arr<T2> precomputed_correlations;
    CubicSpline<T2> cubic;
    arr<pointing> directions;
    arr<xcomplex<T2> > equatorialRings, northCap, southCap, extraRingsN, extraRingsS; 
    arr<T2> noiseEquatorialRings, noiseNorth, noiseSouth, noiseExtraN, noiseExtraS;
    Healpix_Base baseLevel, lastLevel;
    const int Nside;
    const int sizeNorthCap;
    int numNeighbours,numMatrixElements;
    int backupRings, capPixels;
    int sizeExtra;
    T2 noiseRegulation;
    int interpolateLevel;
  public:
 
    /**
      * This is the constructor of the spin-s field interpolator. 
      * It takes the average spectrum (in the argument \p spectrum) of the fluctuations of the complex field
      * on the sphere as an argument. This  function may take a significant amount of time to run
      * as the covariance matrices of the field on the pixelized sphere are computed for each pixel of the \p HEALPix
      * grid. The complexity of the interpolation may be tuned using the \p nLevel argument. \p nLevel specifies 
      * implicitly the number of neighbors to take to achieve the interpolation in some direction. This number is 
      * given by the formula:
      * \f[
      *     N = 9 \times 4^{\mathrm{nLevel}-1}.
      * \f]
      * It represents a maximum as for some pixels it is more complicated to find correct neighbors, those directions
      * are just ignored. The minimum number for each pixel is obtained by replacing 9 by 5.
      * 
      * Additionally, the functions requires the resolution at which the input map, to be interpolated, is sampled.
      * This is determined by the argument \p Nside.
      * 
      * Optionally it is possible to specify two other parameters to the interpolation: \p minAngle and \p regulation.
      * 
      * \p minAngle sets the sampling resolution of the correlation function for tabulation.
      * This in principle should be smaller than
      * the typical pixel radius. By default, \p minAngle is set to the maximum pixel radius of the \p HEALpix grid. 
      * The correlation function computer actually subdivides this into 4. It is nonetheless possible to use a finer
      * (or a coarser) grid by adjusting this parameter.
      * 
      * The second optional parameter, \p regulation, allows the user to set an homogeneous regulation term for 
      * decreasing numerical instabilities in the inversion of covariance matrices. Its impact is to reduce the
      * the correlations, which degrades the quality of the weights, but allow a more stable inversion when
      * the maps become degenerate. Please note that if the resolution of the input map is too high compared 
      * to the requirement given by the spectrum it is advisable to use the naive interpolator which will correctly
      * behave. 
      *
      * After the constructor has built the interpolator successfully, i.e. no exception has been thrown, it
      * is possible to use directly the #interpolate member function, and all the other related functions.
      *
      * \params spectrum the polarization power spectrum
      * \params nLevel the resolution of interpolation in terms of number of neighbors
      * \params nSide the resolution of the input map
      * \params minAngle the minimum angle for tabulating the correlation function.
      * \params regulation an adimensional regulatory term to promote numerical stabilities in the inversion of covariance matrices.
      * 
      * \sa interpolate, interpolateMany, interpolateManyTransposed
      */    
    QuickPolInterpolateMap(const arr<T2>& spectrum, 
			   int nLevel, int Nside, double minAngle = -1, T2 regulation = 0)
      throw(PlanckError)
      : cls(spectrum), lmax(spectrum.size()-1),
	numNeighbours(9*(1<<(2*(nLevel-1)))),
	baseLevel(Nside/nLevel, RING, SET_NSIDE),
	interpolateLevel(nLevel-1),
	lastLevel(Nside, RING, SET_NSIDE),
	sizeNorthCap(2*Nside*(Nside-1)),
	Nside(Nside)
    {    
      if (minAngle < 0)
	minAngle = lastLevel.max_pixrad();

      this->minAngle = minAngle;
      this->numNeighbours = numNeighbours;
      this->noiseRegulation = regulation;
      // WARNING Heuristic tuning
      backupRings = 4+(1<<(2*nLevel-1));
       // END WARNING
      sizeExtra = Nside*backupRings;
      capPixels = Nside*(Nside-1)/2;
#ifdef MEMORY_DIET_PACK_MATRIX
      numMatrixElements = ((numNeighbours+1)/2+1)*numNeighbours;
#else
      numMatrixElements = numNeighbours*numNeighbours;
#endif
      precomputeCorrelations();
      precomputeDirections();
      precomputeGeometry();
   }
    
    ~QuickPolInterpolateMap() throw()
    {
    }
    
    /**
     * Do the real job of computing the value of the correlation function. 
     */
    void doRealComputation()
      throw (PlanckError);
    
    /** 
     * This function computes the Euler angle of the rotation to bring the direction
     * (theta,phi) to (thetap,phip). The angles are alpha,beta,gamma.
     */
    void computeRotation(double theta, double phi,
			 double thetap, double phip,
			 double& alpha, double& beta, double& gamma);
    
    /**
     * This function precomputes the pixelization statistical properties given the input spectrum.
     * It is called by the constructor.
     */
    void precomputeGeometry()
      throw (PlanckError);

    /** This function precomputes the 3d unit vector 
     * for each pixel. This allows us to do a lookup
     * instead of a serie of costly trigonometric operation.
     * It is called by the constructor.
     */
    void precomputeDirections()
    {
      Healpix_Base map(Nside, RING, SET_NSIDE);

      directions.alloc(map.Npix());
      for (int i = 0; i < map.Npix(); i++)
	directions[i] = map.pix2ang(i);
    }

    /**
     * This function precomputes a cubic interpolation
     * of wigner functions polynomials. This will speed up
     * the interpolation algorithm while keeping
     * much of the precision (in practice nearly all).
     * It is called by the constructor.
     */
    void  precomputeCorrelations()
      throw(PlanckError);
    
    /**
     * This function computes the correlation function
     * using the cubic interpolation. This concerns
     * only the beta rotation of the correlation.
     */
    T2 computeCorrelation(double beta)
    {
      return cubic.computeRegular(beta);
    }

    /** 
     * This function computes the correlation matrix
     * between the given directions and the direction of interpolation.
     * The array must be preallocated and specified in \a M.
     */
    void setupMatrix(const arr<pointing>& all_vec, 
		     arr<xcomplex<T2> >& M, const arr<int>& valid_pix)
      throw(PlanckError);

    void getMatrix(xcomplex<T2>* C, T2& nregul, int pix, arr<int>& pixel_list);

    void computeMatrix(int pix, arr<int>& pixels, arr<pointing>& all_vec,
		       arr<xcomplex<T2> >& cho, arr<xcomplex<T2> >& M, xcomplex<T2> *geom, T2 *nregul);

    /**
      * This function discover the list of neighbors of a given pixel.
      * The length depends on Nlevel, defined in the constructor.
      */
    void discoverNeighbours(long pix, arr<int>& pixels);
    
    /** 
     * This is the base function for interpolating polarization maps. It requires an input complex
     * \p HEALPix map, \p map, and a direction of interpolation, \p ptg. It returns the  interpolated complex value.
     * Optionally, the user can request the number of pixels used in the procedure, in \p npix, and also the
     * expected standard deviation due to the interpolation, assuming the field follow locally
     * a Gaussian statistic, in \p var. Note that if you need a large number of interpolated directions
     * it is advised to use one of the \p interpolateMany member function.
     *
     * \params map the input map to interpolate. This map must be of the complex type.
     * \params ptg a direction expressed using a \p pointing object.
     * \params npix the number of pixels used in the interpolation. It is possible to set this to the null pointer. In that case, no number will be returned.
     * \params var the standard deviation, due to the interpolation procedure, on the interpolated direction.
     *
     * \sa #interpolateMany(const Healpix_Map<xcomplex<T> >& map, const pointing *ptgs, xcomplex<T> *result, int numDirections)
     * \sa #interpolateMany(const Healpix_Map<xcomplex<T> >& map, const pointing *ptgs, T *var, xcomplex<T> *result, int numDirections)
     */     
    xcomplex<T> interpolate(const Healpix_Map<xcomplex<T> >& map,
		  const pointing& ptg, int *npix, T *var)
      throw (PlanckError);
    
    template<bool doNpix, bool doVar>
    void interpolateManyBase(const Healpix_Map<xcomplex<T> >& map, const pointing *ptgs, int *npix, T *var, xcomplex<T> *result, int numDirections)
       throw (PlanckError);

    /**
     * Function.
     */
    void interpolateMany(const Healpix_Map<xcomplex<T> >& map, const pointing *ptgs, xcomplex<T> *result, int numDirections)
      throw (PlanckError);

    /**
     */
    void interpolateMany(const Healpix_Map<xcomplex<T> >& map, const pointing *ptgs, T *var, xcomplex<T> *result, int numDirections)
      throw (PlanckError);

    /**
     */
    void interpolateMany(const Healpix_Map<xcomplex<T> >& map, const pointing *ptgs, int *npix, xcomplex<T> *result, int numDirections)
      throw (PlanckError);
    void interpolateMany(const Healpix_Map<xcomplex<T> >& map, const pointing *ptgs, int *npix, T *var, xcomplex<T> *result, int numDirections)
      throw (PlanckError);


    template<bool doNpix, bool doVar>
    void interpolateManyTransposedBase(const xcomplex<T>* restrict_flints mapValue, Healpix_Map<xcomplex<T> >& outmap, const pointing *ptgs, int *npix, T *var, int numDirections)
       throw (PlanckError);


    void interpolateManyTransposed(const xcomplex<T> * restrict_flints mapValue, Healpix_Map<xcomplex<T> >& outmap, const pointing * restrict_flints ptgs, int numDirections) 
      throw (PlanckError) { interpolateManyTransposedBase<false,false>(mapValue, outmap, ptgs, 0, 0, numDirections); }

    void matrixError(const arr<xcomplex<T2> >& Correlation, int n);
  };    
};

#include "polinterpol.tcc"

#endif
