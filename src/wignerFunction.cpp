/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/wignerFunction.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/



#include "wignerFunction.hpp"
#include "fftpack_support.h"
#include <iostream>
#include <cmath>
#include <fstream>

using namespace std;


// We assume m1, m2 > 0 here. deltas1 and deltas2 should have
// a size equal to (l+1)
static
void computeDeltas(int l, int m1, int m2, double *deltas1, double *deltas2)
{
  int lcur;
  // Deltas should be an array of length l.

  deltas1[0] = 1;
  // First increase the order l->l+1
  lcur = 0;
  while (lcur < l)
    {
      for (int q = lcur; q >= 0; q--)
	{
	  double coef;
	  
	  coef = 0.5*(lcur+1)*(2*lcur+1)/((lcur+q+2)*(lcur+q+1));
	  deltas1[q+1] = sqrt(coef) * deltas1[q];
	}
      deltas1[0] = -(sqrt(1-1.0/(2*lcur+2)))*deltas1[0];
      lcur++;
    }

  // Now we reached lcur=l. deltas hold Delta^l_{l,m2} for 0<=m2<=l
  // We now need all Delta^l_{u,m2} and Delta^l_{u,m1} for -l<=u<=l at fixed (m1,m2).

  deltas2[l] = deltas1[m2];
  deltas1[l] = deltas1[m1];

  deltas2[l-1] = 2*m2/sqrt(2.0*l) * deltas2[l];
  deltas1[l-1] = 2*m1/sqrt(2.0*l) * deltas1[l];

  int mcur = l-2;
  while (mcur >= 0)
    {
      double coef1, coef1_2, coef2;

      coef1 = 2.0/sqrt((l-double(mcur))*(l+mcur+1.));
      coef2 = sqrt((l-mcur-1.)*(l+mcur+2.)/((l-mcur)*(l+mcur+1.)));

      deltas1[mcur] = m1*coef1 * deltas1[mcur+1] - coef2 * deltas1[mcur+2];
      deltas2[mcur] = m2*coef1 * deltas2[mcur+1] - coef2 * deltas2[mcur+2];
      mcur--;
    }
}

void CMB::computeWigner_d_Fourier(int l, int m1, int m2, double *B)
{
  int Bsign = 1, B2sign = 1;
  int Bimag;
  double *deltas1 = new double[l+1];
  double *deltas2 = new double[l+1];

  int idelta = (m2-m1);
  if (idelta < 0)
    {
      idelta = -idelta;
      Bsign *= 1-2*(idelta&1);
    }
  Bsign *= 1-(idelta&2);
  Bimag = idelta&1;

  if (m1 < 0)
    {
      B2sign *= -1;
      Bsign *= 1 - 2*(l&1);
      m1 = -m1;
    }

  if (m2 < 0)
    {    
      B2sign *= -1;
      Bsign *= 1 - 2*(l&1);
      m2 = -m2;
    }

//  if (m1 >= m2)
//    {
//      Bsign *= 1-(int(m1-m2)&2);
//      Bimag = (m1-m2)&1;
//    }
//  else
//    {
//      Bsign *= 1-(int(m2-m1)&2);
//      Bimag = (m2-m1)&1;
//    }
  
  computeDeltas(l, m1, m2, deltas1, deltas2);
  
  if (!Bimag)
    B[0] = Bsign*deltas1[0]*deltas2[0];
  B = &B[Bimag+1];
  int s = B2sign;
  for (int u = 1; u <= l; u++,B+=2)
   {
     *B = s*Bsign*deltas1[u]*deltas2[u];
     s *= B2sign;
   }

  delete[] deltas1;
  delete[] deltas2;
}

void CMB::computeWigner_D(int l, int m1, int m2, int N, double *data)
{
  rfft plan;

  plan.Set(N);
  
  for (int i = 0; i < N; i++)
    data[i] = 0;
  computeWigner_d_Fourier(l, m1, m2, data);

  plan.backward_fftpack(data);
}

#if 0
int main(int argc, char **argv)
{
  int N = 100000;
  double *data = new double[N];
  int l = 5;
  int m1 = 4, m2 = 2;

  computeWigner_D(l, m1, m2, N, data);

  for (int i = 0 ; i < N; i++)
    {
      cout << 2*M_PI/N*i << " " << data[i] << endl;
    }

  delete[] data;
}
#endif
#if 0
int main(int argc, char **argv)
{
  const int l = 5;
  int m1 = 4, m2 = 2;
  double deltas1[l+1], deltas2[l+1];
  
  computeDeltas(l, m1, m2, deltas1, deltas2);


  //  cout << deltas1[0] << " " << deltas2[0] << endl;
#if 0
  for (int i = 0; i <= l; i++)
    {
      cout << i << " " << deltas1[i] << "   " << deltas2[i] << endl;
    }
#endif
  

#if 0
  for (int u = 0; u <= l; u++)
    {
      cout << "B^{" << l << "}_{" << m1 << "," << m2 << "," << u << "} =  ";
      if (Bimag)
	cout << " i ";
      cout << Bsign*deltas1[u]*deltas2[u] << endl;
    }
  for (int u = 0; u <= l; u++)
    {
      cout << "d^{" << l << "}_{" << u << "," << m1 << "} = " << deltas1[u] << endl;
      cout << "d^{" << l << "}_{" << u << "," << m2 << "} = " << deltas2[u] << endl;
    }
  cout << endl << endl;
  
  cout << "d^{" << l << "}_{" << m1 << "," << m2 << "} (beta) =" << endl;
  cout << "\t" << Bsign*deltas1[0]*deltas2[0] << endl;
  for (int u = 1; u <= l; u++)
    {
      cout << "\t" << (Bimag ? "+i " : "+ ") << Bsign*deltas1[u]*deltas2[u] << " exp(i " << u << " beta)" << endl;
    }
  cout << "\t + c.c." << endl;
#endif

  ofstream f("toto.txt");
  for (int i = 0; i <= 1000; i++)
    {
      double mysum = 0;
      double beta = 2*M_PI*i/1000 - M_PI;

      for (int u = 1; u <= l; u++)
	{
	  double Br = (1-Bimag) * deltas1[u] * deltas2[u] * cos(u*beta);
	  double Bim = Bimag * deltas1[u] * deltas2[u] * sin(u*beta);

	  mysum += Br + Bim;
	}
      mysum *= 2;
      mysum += (1-Bimag) * deltas1[0] * deltas2[0];
      mysum *= Bsign;

      f << beta << " " << mysum << endl;
    }

  return 0;
}
#endif
