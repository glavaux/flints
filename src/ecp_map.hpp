/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/ecp_map.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#ifndef __ECP_MAP_HPP
#define __ECP_MAP_HPP

#include <assert.h>
#include <functional>
#include <arr.h>
#include "pointing.h"
#include "clean_sum.hpp"
#include <xcomplex.h>
#include "extra_complex.h"

template<typename T>
T element_product(const T&a, const T& b)
{
  return conj(a)*b;
} 

class ECP_Map_Base
{
protected:
  double Phi0;
  int nphi, nrings;
  long npix;
public:
  ECP_Map_Base(double phi0)
  {
    this->Phi0 = phi0;
    nphi = nrings = npix = 0;
  }


  ECP_Map_Base(int Nrings, int Nphi, double phi0 = 0)
  {
    this->Phi0 = phi0;
    Set(Nrings, Nphi);
  }
  
  virtual void Set(int Nrings, int Nphi)
  {
    this->nrings = Nrings;
    this->nphi = Nphi;
    this->npix = long(Nphi)*Nrings;    
  }

  virtual ~ECP_Map_Base()
  {
  }

  double max_pixrad() const
  {
    return sqrt(4*M_PI*M_PI/nphi/nphi + 4*M_PI*M_PI/nrings/nrings);
  }

  long Npix() const
  {
    return npix;
  }

  int Nrings() const
  {
    return nrings;
  }

  int Nphi() const
  {
    return nphi;
  }

  pointing pix2ang(int pix) const
  {
    double theta, phi;

    pix2ang(pix, theta, phi);
    return pointing(theta, phi);
  }

  vec3 pix2vec(int pix) const
  {
    return vec3(pix2ang(pix));
  }

  void pix2ang(int pix, double& theta, double& phi) const
  {
    int ring = pix / nphi;
    int i_phi = pix - ring*nphi;

    theta = (ring+0.5)*M_PI/nrings;
    phi = i_phi*2*M_PI/nphi + Phi0;
  }

  int pix2ring(int pix) const
  {
    return pix / nphi;
  }

  int rp2pix(int ring, int phi) const
  {
    return long(ring)*nphi + phi;
  }

  int ang2pix(double theta, double phi) const
  {
    int ring, i_phi;

    phi -= Phi0;
    while (phi < 0)
      phi += 2*M_PI;
    while (phi >= 2*M_PI)
      phi -= 2*M_PI;

    ring = (int)round(theta*nrings/M_PI-0.5);
    i_phi = (int)round(nphi/(2*M_PI)*phi) % nphi;

    if (ring >= nrings)
      ring = nrings-1;
    if (ring < 0)
      ring = 0;

    return i_phi + ring*nphi;
  }

  int ang2pix(const pointing& p) const
  {
    return ang2pix(p.theta, p.phi);
  }

  int vec2pix(const vec3& v) const
  {
    return ang2pix(pointing(v));
  }

  void get_interpol(const pointing& p, fix_arr<int, 4>& npix, fix_arr<double, 4>& wgt) const
  {
    int ring, i_phi;
    double theta = p.theta;
    double phi = p.phi;

    phi -= Phi0;
    while (phi < 0)
      phi += 2*M_PI;
    while (phi > 2*M_PI)
      phi -= 2*M_PI;

    ring = (int)floor(theta*nrings/M_PI-0.5);
    i_phi = (int)floor(nphi/(2*M_PI)*phi);
    assert(i_phi >= 0);
    assert(i_phi < nphi);

    if (ring >= nrings)
      ring = nrings-1;
    
    if (ring < 0 || ring == (nrings-1))
      {
        int pix;
        
        ring = (ring < 0) ? 0 : ring;
        pix = i_phi + ring*nphi;

        npix[0]=npix[1]=npix[2]=npix[3] = pix;
        wgt[0]=wgt[1]=wgt[2]=wgt[3] = 0.25;
        return;
      }
    
    double DeltaTheta = M_PI/nrings, DeltaPhi = 2*M_PI/nphi;
    int i_phi_plus = (i_phi+1);
    double phi_plus;
    double phi_minus = i_phi * DeltaPhi + Phi0;
    int ring_plus = ring+1;
    double tpix[4], ppix[4];

    phi_plus = i_phi_plus*DeltaPhi + Phi0;
    if (i_phi_plus == nphi)
     i_phi_plus = 0;

    npix[0] = i_phi + ring*nphi;
    npix[1] = i_phi_plus + ring*nphi;
    npix[2] = i_phi  + ring_plus*nphi;
    npix[3] = i_phi_plus  + ring_plus*nphi;

    ppix[0]=ppix[2] = phi_minus;
    ppix[1]=ppix[3] = phi_plus;
    tpix[0]=tpix[1] = (ring+0.5)*DeltaTheta;
    tpix[2]=tpix[3] = (ring_plus+0.5)*DeltaTheta;


    for (int q = 0; q < 4; q++)
      {
        double delta_theta = std::max(double(0), std::min(double(1), std::abs((theta - tpix[q])/DeltaTheta)));
        double delta_phi = std::max(double(0),std::min(double(1),std::abs((phi-ppix[q])/DeltaPhi)));
        
        wgt[q] = (1-delta_phi)*(1-delta_theta);
      }
  }

  void neighbors(int pix, fix_arr<int, 8>& n) const
  {
    int ring = pix / nphi;
    int i_phi = pix - ring*nphi;
   
    if (ring == 0)
      {
	int i0_phi = (nphi/2 + i_phi) % nphi;
	int i0m_phi = (i0_phi-1+nphi) % nphi;
	int i0p_phi = (i0_phi+1) % nphi;
	int im = (i_phi-1+nphi) % nphi;
	int ip = (i_phi+1) % nphi;

	n[0] = i0m_phi;
	n[1] = i0_phi;
	n[2] = i0p_phi;
	n[3] = im;
	n[4] = ip;
	n[5] = im + nphi;
	n[6] = i_phi + nphi;
	n[7] = ip + nphi;
      }
    else if (ring == nrings-1)
      {
	int i0_phi = (nphi/2 + i_phi) % nphi;
	int i0m_phi = (i0_phi-1+nphi) % nphi;
	int i0p_phi = (i0_phi+1) % nphi;
	int k = (nrings-1)*nphi;
	int im = (i_phi-1+nphi) % nphi;
	int ip = (i_phi+1) % nphi;

	n[0] = k + i0m_phi;
	n[1] = k + i0_phi;
	n[2] = k + i0p_phi;
	n[3] = k + i_phi-1;
	n[4] = k + i_phi+1;
	n[5] = im + k-nphi;
	n[6] = pix - nphi;
	n[7] = ip + k-nphi;
      }
    else
      {
	int km = (ring-1)*nphi;
	int kp = (ring+1)*nphi;
	int k0 = ring*nphi;
	int im = (i_phi-1+nphi) % nphi;
	int ip = (i_phi+1) % nphi;

	n[0] = km + im;
	n[1] = km + i_phi;
	n[2] = km + ip;
	n[3] = k0 + im;
	n[4] = k0 + ip;
	n[5] = kp + im;
	n[6] = kp + i_phi;
	n[7] = kp + ip;
      }    
  }
};

template<typename T>
class ECP_Map: public ECP_Map_Base
{
protected:
  arr<T> map;
public:
  ECP_Map(double phi0 = 0)
    : ECP_Map_Base(phi0)
  {
  }
  
  ECP_Map(int Nrings, int Nphi, double phi0 = 0)
    : ECP_Map_Base(Nrings, Nphi, phi0)
  {
    map.alloc(Nrings*Nphi);
  }

  virtual void Set(int Nrings, int Nphi)
  {
    ECP_Map_Base::Set(Nrings, Nphi);
    map.alloc(Nrings*Nphi);
  }

  ~ECP_Map()
  {
  }

  arr<T>& Map()
  {
    return map;
  }

  const arr<T>& Map() const
  {
    return map;
  }

  T& operator[](int pix)
  {
    return map[pix];
  }

  const T& operator[](int pix) const
  {
    return map[pix];
  }

  T dot(const ECP_Map<T>& other) const
  {
    arr<T> target_sum(map.size());

    assert(map.size() == other.map.size());

    CMB::clean_sum_op2(target_sum, map, other.map, map.size(), std::ptr_fun<const T&,const T&,T>(element_product<T>));
    return target_sum[0];
  }

};

#endif

