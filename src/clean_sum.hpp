/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/clean_sum.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#ifndef __CMB_CLEAN_SUM_HPP
#define __CMB_CLEAN_SUM_HPP
#include <cassert>

namespace CMB
{

  template<typename ArrT>
  void clean_sum(ArrT& a, long cur_len)
  {
    while (cur_len > 1)
      {       
        if ((cur_len % 2) == 0)
          cur_len /= 2;
        else
          {
              // Add the odd numbered one and reduce.
            a[cur_len-2] += a[cur_len-1];
            cur_len = (cur_len-1)/2;
          }
        for (long p = 0; p < cur_len; p++)
          {
            assert((p+cur_len) < a.size());
            a[p] += a[p+cur_len];    
          }
      }
  }

  template<typename ArrT, typename Op>
  void clean_sum_op2(ArrT& target, const ArrT& a, const ArrT& b, long cur_len, Op op)
  {
    assert(cur_len > 0);

    if ((cur_len % 2) == 0)
     {
       cur_len /= 2;
#pragma omp parallel for schedule(static)
       for (long p = 0; p < cur_len; p++)
          target[p] = op(a[p],b[p]) + op(a[p+cur_len],b[p+cur_len]);
     }
    else
     {
       if (cur_len == 1)
        {
          target[0] = op(a[0],b[0]);
          return;
        }
       cur_len /= 2;
#pragma omp parallel for schedule(static)
       for (long p = 0; p < cur_len; p++)
          target[p] = op(a[p],b[p]) + op(a[p+cur_len],b[p+cur_len]);
       target[cur_len-1] += op(a[cur_len*2],b[cur_len*2]);
     }

    while (cur_len > 1)
      {
        if ((cur_len % 2) == 0)
          cur_len /= 2;
        else
          {
              // Add the odd numbered one and reduce.
            target[cur_len-2] += target[cur_len-1];
            cur_len = (cur_len-1)/2;
          }
#pragma omp parallel for schedule(static)
        for (long p = 0; p < cur_len; p++)
          {
            assert((p+cur_len) < a.size());
            target[p] += target[p+cur_len];
          }
      }
  }

  template<typename ArrT, typename Op>
  void clean_sum_op(ArrT& a, const ArrT& b, long cur_len, Op op)
  {
    clean_sum_op2(a, a, b, cur_len, op);
  }

};

#endif
