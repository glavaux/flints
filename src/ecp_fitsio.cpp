/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/ecp_fitsio.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <string>
#include <vector>
#include "fitshandle.h"
#include "ecp_map.hpp"
#include "ecp_fitsio.hpp"
#include "datatypes.h"

using namespace std;

static unsigned int ecp_repcount (tsize npix)
{
  if (npix<1024) return 1;
  if ((npix%1024)==0) return 1024;
  return isqrt (npix);
}


template<typename T>
void prepare_ecp_fitsmap
(fitshandle &out, const ECP_Map<T> &base, PDT datatype,
 const arr<string> &colname)
{
  vector<fitscolumn> cols;
  int repcount = ecp_repcount(base.Npix());
  
  for (tsize m=0; m<colname.size(); ++m)
    cols.push_back (fitscolumn (colname[m],"unknown",repcount, datatype));
  out.insert_bintab(cols);
  out.set_key ("PIXTYPE",string("ECP"),"ECP pixelisation");
  out.set_key ("NRINGS",base.Nrings(),"Number of rings");
  out.set_key ("NPHI",base.Nphi(),"Number of elements per ring");
  out.set_key ("FIRSTPIX",0,"First pixel # (0 based)");
  out.set_key ("LASTPIX",base.Npix()-1,"Last pixel # (0 based)");  
}

template<typename T>
void write_ecp_map(fitshandle& h, const ECP_Map<T>& m, PDT datatype)
{
  arr<string> colname(1);
  colname[0] = "signal";
  prepare_ecp_fitsmap (h, m, datatype, colname);
  h.write_column(1,m.Map());
}

template<typename T>
void write_ecp_map(fitshandle& h, const ECP_Map<T>& m, const ECP_Map<T>& mQ, const ECP_Map<T>& mU, PDT datatype)
{
  arr<string> colname(3);
  colname[0] = "signal";
  colname[1] = "Q-pol";
  colname[2] = "U-pol";
  prepare_ecp_fitsmap (h, m, datatype, colname);
  h.write_column(1,m.Map());
  h.write_column(2,mQ.Map());
  h.write_column(3,mU.Map());
}


#define FORCE(T) template void write_ecp_map(fitshandle& h, const ECP_Map<T>& m, PDT datatype)
FORCE(double);
FORCE(float);
#undef FORCE

#define FORCE(T) template void write_ecp_map(fitshandle& h, const ECP_Map<T>& m, const ECP_Map<T>& mQ, const ECP_Map<T>& mU, PDT datatype)
FORCE(double);
FORCE(float);
#undef FORCE

