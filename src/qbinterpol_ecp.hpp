/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/qbinterpol_ecp.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/


#ifndef __BAYES_QINTERPOLATION_ECP_HPP
#define __BAYES_QINTERPOLATION_ECP_HPP

#include <cmath>
#include <iostream>
#include <map>
#include <fstream>
#include <cstring>
#include <cassert>
#include <vector>
#include <vec3.h>
#include <arr.h>
#include "ecp_map.hpp"
#include "extra_map_tools.hpp"
#include "cubic.hpp"
#include "cholesky.hpp"
#include "eigenvals.hpp"
#include "packMatrix.hpp"
#include "qinverse.hpp"
#include "numerics.hpp"
#include "compiler_defs.hpp"
#include "eskow.hpp"

namespace CMB
{
  template<typename T,typename T2 = T>
  class QuickInterpolateMap_ECP
  {
  private:
    /**
      * This is a compile option. The code becomes a little more
      * verbose if \p VERBOSE is \p true.
      */
    static const bool VERBOSE = true;    
    /**
      * If USE_CACHE is true, then the code generates an on-disk cache of the pixelization
      * statistical property, and/or use this cache for further construction of this object.
      */
    static const bool USE_CACHE = false;
    /**
     * Do extra sanity checking in the code if true.
     */
    static const bool DO_EXTRA_CHECKING = true;
    static const bool DO_DIAGNOSIS = true;
    arr<T2> cls;  
    T2 variance;
    int lmax;
    double delta, maxAngle;
    arr<T2> precomputed_correlations;
    CubicSpline<T2> cubic;
    arr<T2> equatorialRings;
    const int Nrings, Nphi;
    int numNeighbours,numMatrixElements;
    T2 noiseRegulation;
    ECP_Map_Base geom;
    arr<vec3> directions;
    arr<bool> bad_matrix;
    std::map<T2, T2> correl_cache;
  public:
    
    static void compute_Pl_array(T2 x, arr<T2>& result);

    double getVariance() const  { return variance; }

    /**
     * This is the constructor.
     * It takes the average spectrum of the fluctuation of the field on the sphere as an argument
     * alongside the number of neighbors (through the nLevel argument), Nside (for the input map to
     * be interpolated) and optionally a regularization term (regulation).     
     */
    QuickInterpolateMap_ECP(const arr<T2>& spectrum, 
			    int nrings, int nphi, double phi0 = 0, T2 regulation = 0)
      throw(PlanckError)
      : cls(spectrum), lmax(spectrum.size()-1),
	numNeighbours(9),
	Nrings(nrings), Nphi(nphi),
	geom(nrings, nphi, phi0)
    {    
      this->maxAngle = std::min(M_PI, 2*std::max(2*M_PI/Nphi, M_PI/Nrings));
      this->numNeighbours = numNeighbours;
      this->noiseRegulation = regulation;
      bad_matrix.alloc(Nrings);
#ifdef MEMORY_DIET_PACK_MATRIX
      numMatrixElements = ((numNeighbours+1)/2+1)*numNeighbours;
#else
      numMatrixElements = numNeighbours*numNeighbours;
#endif
      noiseRegulation = 0;//TYPE_INFO<T>::one_sqrt_precision*2;
      std::cout << "Regulation = " << noiseRegulation << std::endl;
      precomputeVariance();
      precomputeDirections();
      precomputeLegendre(262144);
      precomputeGeometry();
   }
    
    ~QuickInterpolateMap_ECP() throw()
    {
    }

    void precomputeDirections()
      throw (PlanckError);

    void precomputeGeometry()
      throw (PlanckError);

    /**
     * We precompute the variance of a pixel here.
     */
    void precomputeVariance()
      throw (PlanckError)
    {
      variance = 1.0;
      variance = fullComputation(1.0);
    }

    /**
     * This function do the full compute of the 
     * the correlation function for an angular separation 
     * \f$\theta$ and \f$x = \cos(\theta)$.
     */
    T2 fullComputation(T2 x)
      throw (PlanckError);
    
    /**
     * This function precomputes a cubic interpolation
     * of Legendre polynomials. This will speed up
     * the interpolation algorithm while keeping
     * much of the precision (in practice nearly all).
     */
    void  precomputeLegendre(int numPoints)
      throw(PlanckError);
    
    /**
     * This function computes the correlation function
     * using the cubic interpolation.
     */
    T2 computeCorrelation(T2 x)
    {
      return cubic.computeRegular(((T2)1)-x);
    }

    T2 computeOrExtract(T2 x);

    /** 
     * This function computes the correlation matrix
     * between the given directions and the direction of interpolation.
     * The array must be preallocated and specified in \a M.
     */
    template<typename A>
    void setupMatrix(const arr<vec3>& all_vec, 
		     arr<T2>& M, const A& valid_pix)
      throw(PlanckError);

    template<typename A>
    bool getMatrix(T2* restrict_flints C, int pix, A& pixel_list);

    bool computeMatrix(int ring, arr<vec3>& all_vec,
		       arr<T2>& cho, arr<T2>& M, T2 *geom);

    template<typename A>
    void discoverNeighbours(long pix, A& pixels);

   /** 
     * This function evaluates the interpolated pixel using the 9 neighbours
     * of the given direction \a ptg. The number of pixels actually used
     * is returned in \a npix, the variance of the interpolation is in \a var.
     */     
    T interpolate(const ECP_Map<T>& map,
		  const pointing& ptg, int *npix, T *var)
      throw (PlanckError);

    /** This function is most useful for the lensing case. It computes the
     * conjugate transpose operator of the interpolation procedur.e
     * As the operation is actually accumulative,  one has first to clear up
     * the output map before using this function at all.
     */
    template<bool doNpix>
    void interpolateManyTransposedBase(const T *restrict_flints mapValue,
				       ECP_Map<T>& outmap,
				       const pointing * restrict_flints ptg,
				       int numDirections,
				       int *npix)
      throw (PlanckError);

    void interpolateManyTransposed(const T * restrict_flints mapValue, ECP_Map<T>& outmap, const pointing * restrict_flints ptgs, int numDirections) 
      throw (PlanckError) { interpolateManyTransposedBase<false>(mapValue, outmap, ptgs, numDirections, 0); }

    void interpolateTransposed(const T& mapValue, ECP_Map<T>& outmap, const pointing& ptg)
      throw (PlanckError) { interpolateManyTransposedBase<false>(&mapValue, outmap, &ptg, 1, 0); }
    
    template<bool doNpix, bool doVar>
    void interpolateManyBase(const ECP_Map<T>& map, const pointing * restrict_flints ptgs, int *npix, T *var, T *result, int numDirections)
       throw (PlanckError);

    void interpolateMany(const ECP_Map<T>& map, const pointing * restrict_flints ptgs, T *result, int numDirections)
      throw (PlanckError);
    void interpolateMany(const ECP_Map<T>& map, const pointing * restrict_flints ptgs, T *var, T *result, int numDirections)
      throw (PlanckError);
    void interpolateMany(const ECP_Map<T>& map, const pointing * restrict_flints ptgs, int *npix, T *result, int numDirections)
      throw (PlanckError);
    void interpolateMany(const ECP_Map<T>& map, const pointing * restrict_flints ptgs, int *npix, T *var, T *result, int numDirections)
      throw (PlanckError);

    void matrixError(const arr<T2>& Correlation, int n, bool crash = true);
    void vectorError(const T2* Cor, int n);

    template<bool doVar>
    void do_basic_interpolation(const ECP_Map<T>& map,
				const pointing& ptg, T& val, double& err);

    template<bool doVar>
    void do_basic_interpolation_transposed(const T& map_value,
					   const pointing& ptg, ECP_Map<T>& val, double& err);
  };    
};

#include "qbinterpol_ecp.tcc"

#endif
