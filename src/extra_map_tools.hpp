/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/extra_map_tools.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/


#ifndef __EXTRA_MAP_TOOLS_HPP
#define __EXTRA_MAP_TOOLS_HPP

#include <xcomplex.h>
#include <cmath>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <healpix_map.h>
#include <functional>
#include "clean_sum.hpp"
#include "extra_complex.h"

namespace CMB
{
  
  template<typename OutMap>
  void makeRandomMap(gsl_rng *r, OutMap& map)
  {
    long N = map.Npix();

    for (long i = 0; i < N; i++)
      map[i] = gsl_ran_gaussian_ziggurat(r, 1.0);
  }

  template<typename OutMap, typename T>
  void fillMap(OutMap& map, const T& val)
  {
    long N = map.Npix();
    for (long i = 0; i < N; i++)
      map[i] = val;
  }

  template<typename T>
  const Healpix_Map<T>& operator+=(Healpix_Map<T>& m1, const Healpix_Map<T>& m2)
  {
    long N = m1.Npix();
    planck_assert(m1.Npix() == m2.Npix(), "The two maps must be conformable");
    for (long i = 0; i < N; i++)
      m1[i] += m2[i];
    return m1;
  }

  template<typename T>
  const Healpix_Map<T>& operator-=(Healpix_Map<T>& m1, const Healpix_Map<T>& m2)
  {
    long N = m1.Npix();

    planck_assert(m1.Npix() == m2.Npix(), "The two maps must be conformable");
    for (long i = 0; i < N; i++)
      m1[i] -= m2[i];
    return m1;
  }

  template<typename T,typename T2>
  const Healpix_Map<T>& operator*=(Healpix_Map<T>& m1, const T2& val)
  {
    long N = m1.Npix();

    for (long i = 0; i < N; i++)
      m1[i] *= val;
    return m1;
  }
 

  template<typename T>
  T hpx_element_product(const T&a, const T& b)
  {
    return conj(a)*b;
  } 

  template<typename T>
  T dot_product(const Healpix_Map<T>& map1, const Healpix_Map<T>& map2)
  {
    T scalar = 0;
    
    planck_assert(map1.Nside() == map2.Nside(), "map1 and map2 does not have the same Nside");
    planck_assert(map1.Scheme() == map2.Scheme(), "map1 and map2 are not in the same ordering");

    arr<T> tmp(map1.Npix());

    clean_sum_op2(tmp, map1.Map(), map2.Map(), map1.Npix(), std::ptr_fun<const T&, const T&, T>(&hpx_element_product<T>));
    
    return tmp[0];
  }   

  double realpart(double r) { return r; }

  template<typename T>  T realpart(const xcomplex<T>& c) { return c.real(); }

  template<typename T,typename T2>
  Healpix_Map<T2> operator*(T val, const Healpix_Map<T2>& m)
  {
    Healpix_Map<T2> outmap(m.Nside(), m.Scheme(), SET_NSIDE);
    long N = m.Npix();

    for (long i = 0; i < N; i++)
      outmap[i] = m[i]*realpart(val);
    return outmap;
   
  }
  
  template<typename T>
  Healpix_Map<T> operator-(const Healpix_Map<T>& m1, const Healpix_Map<T>& m2)
  {
    planck_assert(m1.Nside() == m2.Nside(), 
		  "Map1 and map2 should have the same number of pixels");
    planck_assert(m1.Scheme() == m2.Scheme(),
		  "Map1 and map2 should have the same scheme");
    Healpix_Map<T> m(m1.Nside(), m1.Scheme(), SET_NSIDE);
    
    for (long i = 0; i < m.Npix(); i++)
      m[i] = m1[i] - m2[i];

    return m;
  }

  template<typename T>
  Healpix_Map<T> operator+(const Healpix_Map<T>& m1, const Healpix_Map<T>& m2)
  {
    planck_assert(m1.Nside() == m2.Nside(), 
		  "Map1 and map2 should have the same number of pixels");
    planck_assert(m1.Scheme() == m2.Scheme(),
		  "Map1 and map2 should have the same scheme");
    Healpix_Map<T> m(m1.Nside(), m1.Scheme(), SET_NSIDE);
    
    for (long i = 0; i < m.Npix(); i++)
      m[i] = m1[i] + m2[i];

    return m;
  }

  template<typename T>
  Healpix_Map<T> abs(const Healpix_Map<T>& m1)
  {
    Healpix_Map<T> m(m1.Order(), m1.Scheme());
    
    for (long i = 0; i < m.Npix(); i++)
      m[i] = std::abs(m1[i]);

    return m;
  }
   
  template<typename T>
  Healpix_Map<T> operator/(const Healpix_Map<T>& m1, const Healpix_Map<T>& m2)
  {
    Healpix_Map<T> m(m1.Nside(), m1.Scheme(), SET_NSIDE);
    
    for (long i = 0; i < m.Npix(); i++)
      m[i] = m1[i]/m2[i];
    return m;
  }

  template<typename T>
  T norm_L2(const Healpix_Map<T>& map)
  {
    return dot_product(map,map).real();
  }

  template<typename T>
  const Healpix_Map<T>& clear(Healpix_Map<T>& m)
  {
    for (long i = 0; i < m.Npix(); i++)
      m[i] = 0;
    return m;
  }
  
};


#endif
