/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/cholesky_svd.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#ifndef __CHOLESKY_SVD_HPP
#define __CHOLESKY_SVD_HPP

#include <cmath>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_matrix_float.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include <stdlib.h>
#include "cholesky.hpp"

namespace CMB
{

  template<typename T>
  void do_svd_regularisation(arr<T>& d, int N)
  {
    abort();
  }

  template<>
  void do_svd_regularisation(arr<double>& d, int N)
  {
    gsl_eigen_symmv_workspace *work =  gsl_eigen_symmv_alloc (N);
    gsl_matrix_view M = gsl_matrix_view_array(&d[0], N, N);
    gsl_matrix *V = gsl_matrix_alloc(N,N);
    gsl_matrix *U = gsl_matrix_alloc(N,N);
    gsl_vector *S = gsl_vector_alloc(N);
    
    gsl_eigen_symmv (&M.matrix, S, U, work);

    gsl_eigen_symmv_sort (S, U, GSL_EIGEN_SORT_VAL_DESC);

    gsl_matrix_memcpy(V, U);
    double v0 = 1/gsl_vector_get(S, 0);
    for (int i = 0; i < N; i++)
      {
	double v = gsl_vector_get(S, i), v2;
	if (v < 0 || v*v0 < 100*TYPE_INFO<double>::one_sqrt_precision)
	  v2 = 0;
	else
	  v2 = 1/(v);

	gsl_vector_view ci = gsl_matrix_column (V, i);
	gsl_vector_scale(&ci.vector, v2);
      }

    gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1, V, U, 0, &M.matrix);

    gsl_vector_free(S);
    gsl_eigen_symmv_free(work);
    gsl_matrix_free(V);
    gsl_matrix_free(U);
  }

};

#endif
