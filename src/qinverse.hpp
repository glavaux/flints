/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/qinverse.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/


#ifndef __QINVERSE_HPP
#define __QINVERSE_HPP

#include <cmath>
#include <cassert>
#include <xcomplex.h>
#include "compiler_defs.hpp"

namespace CMB_Interpolation
{
  static inline
  double conj(double x) { return x; }

  static inline
  long double conj(long double x) { return  x;}

  static inline
  float conj(float x) { return x;}

/** This is quick inversion procedure for retrieving
 * the first line of an inverse matrix whose
 * we already know some other pieces. 
 * I took inspiration from chapter 2-7 of Numerical 
 * Recipes.
 */

  template<typename T>
  void vecmatprod(const T*restrict_flints Matrix, const T*restrict_flints vector, T*restrict_flints output, int N)
  {
    for (int i = 0; i < N; i++)
      {
	output[i] = 0;
	for (int k = 0; k < N; k++)
	  output[i] += Matrix[i*N+k]*vector[k];
      }
  }

  template<typename T>
  void vecdotprod(const T*restrict_flints v1, const T* restrict_flints v2, T& sum, int N)
  {
    sum = 0;
    for (int i = 0; i < N; i++)
      {
	T prod = conj(v1[i]) * v2[i];
	sum += prod;
      }
  }
  

  template<typename T, bool needVariance>
  void compute_line(const T* restrict_flints S_inverse, const T* restrict_flints correlations,
		    T*restrict_flints covariance, T& variance, int N, T regul = 0)
  {
    //  assert(work.size() == correlations.size());
    vecmatprod(S_inverse, correlations, covariance, N);
    if (needVariance) {    
      vecdotprod(covariance, correlations, variance, N);
      variance = (((T)1)+regul-variance);  
    }
  }

};

#endif
