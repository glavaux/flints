/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/qbinterpol_ecp.tcc
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/


#include <Eigen/Core>
#include <Eigen/Cholesky>
#include <exception>
#include "eigenvals.hpp"
#include <iomanip>
#include <boost/format.hpp>
#include "cholesky.hpp"
#include "cholesky_svd.hpp"

namespace CMB
{

  template<typename T>
  class ArrMatrix_adaptor
  {
  public:
    arr<T> *a;
    int N;

    T& operator()(int i, int j)
    {
      return (*a)[i*N+j];
    }

    const T& operator()(int i, int j) const
    {
      return (*a)[i*N+j];
    }
  };

  template<typename T,typename T2>
  void QuickInterpolateMap_ECP<T,T2>::compute_Pl_array(T2 x, arr<T2>& Pl) 
  {
    int k = 2;
    int n = Pl.size();
    
    if (n == 0)
      return;
    
    if (n >= 1)
      Pl[0] = 1;
    if (n >= 2)
      Pl[1] = x;
    
    for (int k = 2; k < n; k++)
      {
	Pl[k] = ((2*k-1)*x*Pl[k-1] - (k-1)*Pl[k-2])/k;
      }
  }

  template<typename T, typename T2>
  void QuickInterpolateMap_ECP<T,T2>::vectorError(const T2* Cor, int npix)
  {
    static int vector_id = 0;
    std::string fname = boost::str(boost::format("vector_%d.txt") % vector_id);
    std::ofstream mfile(fname.c_str());
    for (int i = 0; i < npix; i++)
      {
	mfile << std::setprecision(30) << Cor[i] << std::endl;
      }
    vector_id++;
  }

  template<typename T,typename T2>
  void QuickInterpolateMap_ECP<T,T2>::matrixError(const arr<T2>& Correlation, int npix, bool crash)
  {
    static int matrix_id = 0;
    std::string fname = boost::str(boost::format("matrix_%d.txt") % matrix_id);
    std::ofstream mfile(fname.c_str());
    for (int i = 0; i < npix; i++)
      {
	for (int j = 0; j < npix; j++)
	  {
	    if (j >= i)
	      mfile << std::setprecision(30) << Correlation[i*npix+j] << std::endl;
	    else
	      mfile << std::setprecision(30) << Correlation[j*npix+i] << std::endl;
	  }
      }
    if (crash)
      {
	std::cout
	  << "Error in covariance matrix decomposition. "
	  << boost::format("Matrix dumped to '%s'") % fname << std::endl;
	abort();
      }
    else
      matrix_id++;
  }
  
  template<typename T,typename T2>
  T2 QuickInterpolateMap_ECP<T,T2>::fullComputation(T2 x)
    throw (PlanckError)
  {
    arr<T2> result(lmax+1);

    compute_Pl_array(x, result);
    
    T2 accum = 0;
    assert(lmax == result.size()-1);
    assert(lmax == cls.size()-1);
    for (int l = 1; l <= lmax; l++)
      accum += (2*l+1)*result[l]*cls[l];
    accum += result[0]*cls[0];
    accum /= (4*M_PI);
    return accum/variance;
  }

  template<typename T,typename T2>
  void QuickInterpolateMap_ECP<T,T2>::precomputeDirections()
    throw (PlanckError)
  {
    directions.alloc(geom.Npix());
    for (long p = 0; p < geom.Npix(); p++)
      {
	directions[p] = geom.pix2vec(p);
      }
  }

  class InterpolateCacheError_ECP: virtual std::exception {};
  
  template<typename T, typename T2>
  T2 QuickInterpolateMap_ECP<T,T2>::computeOrExtract(T2 x)
  {
    typename std::map<T2,T2>::iterator iter = correl_cache.find(x);
    if (iter == correl_cache.end())
      return (correl_cache[x]  = fullComputation(x));
    return iter->second;
  }

  template<typename T,typename T2>
  void QuickInterpolateMap_ECP<T,T2>::precomputeLegendre(int numPoints)
    throw(PlanckError)
  {
    if (VERBOSE)
      std::cout << "Precomputing correlation function" << std::endl;
    
    delta = (1-cos(maxAngle))/(numPoints);
    precomputed_correlations.alloc(numPoints);

    try
      {
	if (USE_CACHE)
	  {
	    std::ifstream fcache("cache_legendre.dat");
	    if (!fcache)
	      throw InterpolateCacheError_ECP();

	    double d;
	    int n;
	    double maxA;

	    std::cout << "Cache detected" << std::endl;

	    fcache.read((char*)&d, sizeof(double));
	    fcache.read((char *)&n, sizeof(int));
	    fcache.read((char *)&maxA, sizeof(double));
	    if (d != delta || n != numPoints || maxA != maxAngle)
	      throw InterpolateCacheError_ECP();
	    arr<T2> xs;
	    xs.alloc(numPoints);
	    fcache.read((char *)&xs[0], sizeof(T2)*numPoints);
	    fcache.read((char *)&precomputed_correlations[0], sizeof(T2)*numPoints);
	    if (fcache.eof())
	      throw InterpolateCacheError_ECP();
	    
	    std::cout << "Cache loaded successfully" << std::endl;
	    std::cout << "Building cubic splines.." << std::endl;
            cubic = CubicSpline<T2>(xs,precomputed_correlations);
	    return;
	  }
      }
    catch (InterpolateCacheError_ECP e)
      {
	std::cout << "Error while opening cache. rebuilding it." << std::endl;
      }

    arr<T2> xs;
    xs.alloc(numPoints);
#pragma omp parallel for schedule(static)
    for (int i = 0; i < numPoints; i++)
      {
	xs[i] = delta*i;
	precomputed_correlations[i] = fullComputation(1-xs[i]);
      }
    if (USE_CACHE)
      {
	std::ofstream fcache("cache_legendre.dat");

	fcache.write((char *)&delta, sizeof(double));
	fcache.write((char *)&numPoints, sizeof(int));
	fcache.write((char *)&maxAngle, sizeof(double));
	fcache.write((char *)&xs[0], sizeof(double)*numPoints);
	fcache.write((char *)&precomputed_correlations[0], sizeof(double)*numPoints);
      }
    cubic = CubicSpline<T2>(xs,precomputed_correlations);
  }

  template<typename T,typename T2>
  template<typename A>
  void QuickInterpolateMap_ECP<T,T2>::setupMatrix(const arr<vec3>& all_vec, 
						  arr<T2>& M, const A& pixels)
    throw(PlanckError)
  {
    int npix = all_vec.size();

    for (int i = 0; i < npix; i++)
      {
	M[i*npix+i] = ((T2)1) + noiseRegulation;
	
	for (int j = i+1; j < npix; j++)
	  {
	    double cos_theta = dotprod(all_vec[i], all_vec[j]);
	    //planck_assert(cos_theta*cos_theta <= 1, "Error, |cos(theta)| > 1");
	    M[i*npix + j] = M[j*npix+i]=computeOrExtract(cos_theta);
	  }
      }
  }

  template<typename T,typename T2>
  template<typename A>
  bool QuickInterpolateMap_ECP<T,T2>::getMatrix(T2* restrict_flints C, int pix, A& pixel_list)
  {
    int r = geom.pix2ring(pix);
    T2 * restrict_flints v;
    
    if (bad_matrix[r])
      return false;

    v = &equatorialRings[r*numMatrixElements];
    upMatrix<false>(v, C, numNeighbours);
    return true;
  }

  template<typename T,typename T2>
  T QuickInterpolateMap_ECP<T,T2>::interpolate(const ECP_Map<T>& map,
					   const pointing& ptgs,
					   int *npix, T *var)
    throw (PlanckError)
  {
    T var0;
    int npix0;
    T result0;

    interpolateMany(map, &ptgs, &npix0, &var0, &result0, 1);

    if (npix)
      *npix = npix0;
    if (var)
      *var = var0;

    return result0;
  }

  template<typename T,typename T2>
  void QuickInterpolateMap_ECP<T,T2>::interpolateMany(const ECP_Map<T>& map,
						  const pointing * ptgs,
						  T *var, T *result,
						  int numDirections)
    throw (PlanckError)
   {
     interpolateManyBase<false,true>(map, ptgs, 0, var, result, numDirections);
   }

  template<typename T,typename T2>
  void QuickInterpolateMap_ECP<T,T2>::interpolateMany(const ECP_Map<T>& map,
                                              const pointing *ptgs,
                                              int *npix, T *result,
                                              int numDirections)
    throw (PlanckError)
   {
     interpolateManyBase<true,false>(map, ptgs, npix, 0, result, numDirections);
   }

  template<typename T,typename T2>
  void QuickInterpolateMap_ECP<T,T2>::interpolateMany(const ECP_Map<T>& map,
                                              const pointing *ptgs,
                                              int *npix, T *var, T *result,
                                              int numDirections)
    throw (PlanckError)
   {
     interpolateManyBase<true,true>(map, ptgs, npix, var, result, numDirections);
   }

  template<typename T,typename T2>
  void QuickInterpolateMap_ECP<T,T2>::interpolateMany(const ECP_Map<T>& map,
                                              const pointing *ptgs,
                                              T *result,
                                              int numDirections)
    throw (PlanckError)                         
   {                                              
     interpolateManyBase<false,false>(map, ptgs, 0, 0, result, numDirections);
   }

  template<typename T,typename T2> template<bool doNpix>
  void QuickInterpolateMap_ECP<T,T2>::interpolateManyTransposedBase(const T * restrict_flints mapValue,
								    ECP_Map<T>& outMap,
								    const pointing * restrict_flints ptgs,
								    int numDirections,
								    int *npix)
    throw  (PlanckError)
  {
    if (DO_EXTRA_CHECKING)
      {
        if (outMap.Nrings() != geom.Nrings() || outMap.Nphi() != geom.Nphi())
          throw PlanckError("Incompatible geometry used for outMap");
      }
    fix_arr<int,9> pixel_list;
    arr<T2> workSpace(numNeighbours*numNeighbours+numNeighbours*2);
    T2 * restrict_flints CorrelationNgb = &workSpace[0];
    T2 * restrict_flints localCorrelation = &workSpace[numNeighbours*numNeighbours];
    T2 * restrict_flints Correlation_vector = &workSpace[numNeighbours*numNeighbours+numNeighbours];
    T2 geomVariance;
 
    for (int i = 0 ; i < numNeighbours; i++)
      localCorrelation[i] = 0;
    
    for (long q = 0; q < numDirections; q++) {
      vec3 vInterpolate = ptgs[q];
      int pix = geom.vec2pix(ptgs[q]);

      // We discover the neighbours      
      discoverNeighbours(pix, pixel_list);
      
      // First we extract the geometry matrix from the cache
      if (!getMatrix(CorrelationNgb, pix, pixel_list))
        {
          double loc_var;

          do_basic_interpolation_transposed<false>(mapValue[q], ptgs[q], outMap, loc_var);
          if (doNpix)
            npix[q] = 4;
          continue;
        }
      
      // Setup the correlation between the direction to interpolate
      // and the direction of the pixels of the mesh.                       
      for (int i = 0; i < numNeighbours; i++)
        {
          int pix = pixel_list[i];
          T2 cos_theta = dotprod(vInterpolate, directions[pixel_list[i]]);
          localCorrelation[i] = computeCorrelation(cos_theta);
        }
      CMB_Interpolation::compute_line<T2,true>(CorrelationNgb, localCorrelation,
					       Correlation_vector, geomVariance,
					       numNeighbours, noiseRegulation);
      if (geomVariance < 0)
        {
          double loc_var;

          do_basic_interpolation_transposed<false>(mapValue[q], ptgs[q], outMap, loc_var);
	  
          if (doNpix)
            npix[q] = 4;
          continue;
        }

      // Compute the scalar product                        
      T localVal = mapValue[q];
      for (int i = 0; i < numNeighbours; i++)
        {
          long pix = pixel_list[i];
          outMap[pix]  += Correlation_vector[i]*localVal;
        }
      if (doNpix) {
        npix[q] = numNeighbours;
      }
    }
  }
   
  template<typename T, typename T2> template<bool doVar>
  void QuickInterpolateMap_ECP<T,T2>::do_basic_interpolation(const ECP_Map<T>& map,
							     const pointing& ptg, T& val, double& err)
  {
    fix_arr<int, 4> npix;
    fix_arr<double, 4> wgt;
    // Something went wrong. The matrix is too ill-conditioned
    // to be reliable. Just to a basic interpolation
    map.get_interpol(ptg, npix, wgt);
    val = 0;
    for (int i = 0; i < 4; i++)
      {
        val += wgt[i] * map[npix[i]];
      }
    if (doVar)
      {
        err = 0;
        for (int i = 0; i < 4; i++)
          {	    
            double delta = (val-map[npix[i]]);
            err += delta*delta;
          }
        err = std::sqrt(err/3.);
      }
  }


  template<typename T, typename T2> template<bool doVar>
  void QuickInterpolateMap_ECP<T,T2>::do_basic_interpolation_transposed(const T& map_value,
									const pointing& ptg, ECP_Map<T>& val, double& err)
  {
    fix_arr<int, 4> npix;
    fix_arr<double, 4> wgt;
    // Something went wrong. The matrix is too ill-conditioned
    // to be reliable. Just to a basic interpolation
    val.get_interpol(ptg, npix, wgt);
    for (int i = 0; i < 4; i++)
      {
        val[npix[i]] += wgt[i] * map_value;
      }
    if (doVar)
      err = 0;
  }


  template<typename T,typename T2> template<bool doNpix,bool doVar>
  void QuickInterpolateMap_ECP<T,T2>::interpolateManyBase(const ECP_Map<T>& map,
						      const pointing * restrict_flints ptgs, 
						      int *npix, T *var, T *result,
						      int numDirections)
    throw (PlanckError)
  {
#pragma omp parallel
    {
      fix_arr<int,9> pixel_list;
      arr<T2> workSpace(numNeighbours*numNeighbours+numNeighbours*2);
      T2 * restrict_flints CorrelationNgb = &workSpace[0];
      T2 * restrict_flints localCorrelation = &workSpace[numNeighbours*numNeighbours];
      T2 * restrict_flints Correlation_vector = &workSpace[numNeighbours*numNeighbours+numNeighbours];
      T2 geomVariance;
      
      for (int i = 0 ; i < numNeighbours; i++)
        localCorrelation[i] = 0;
#pragma omp for schedule(static)
      for (int q = 0 ; q < numDirections; q++) {
        vec3 vInterpolate = ptgs[q];
        int pix = map.ang2pix(ptgs[q]);
        T accum_value = 0;

        // We discover the neighbours
        discoverNeighbours(pix, pixel_list);
        
        // First we extract the geometry matrix from the cache
        if (!getMatrix(CorrelationNgb, pix, pixel_list))
          {
            double loc_var;

            do_basic_interpolation<doVar>(map, vInterpolate, result[q], loc_var);
            if (doVar)
              var[q] = -loc_var;
            if (doNpix)
              npix[q] = 4;
            continue;
          }

        // Setup the correlation between the direction to interpolate 
        // and the direction of the pixels of the mesh.
        for (int i = 0; i < numNeighbours; i++)
          {
            int pix = pixel_list[i];
            T2 cos_theta = dotprod(vInterpolate, directions[pix]);
            localCorrelation[i] = computeCorrelation(cos_theta);
          }
        
        CMB_Interpolation::compute_line<T2,true>(CorrelationNgb, localCorrelation, 
					         Correlation_vector, geomVariance, 
					         numNeighbours, noiseRegulation);
        
        if (geomVariance < 0)
          {
            double loc_var;

            do_basic_interpolation<doVar>(map, vInterpolate, result[q], loc_var);
            if (doVar)
              var[q] = -loc_var;
            if (doNpix)
              npix[q] = 4;
          }
        else
          {
            // Compute the scalar product
            for (int i = 0; i < numNeighbours; i++)
              {
                int pix = pixel_list[i];
                accum_value += Correlation_vector[i]*map[pix];
              }
            if (doVar)
              var[q] = sqrt(variance*geomVariance);	  
            result[q] = accum_value;
            if (doNpix) {
              npix[q] = numNeighbours;
            }
          }

        assert(!std::isnan(accum_value));
      }
    }
  }

  
  template<typename T, typename T2>
  template<typename A>
  void QuickInterpolateMap_ECP<T,T2>::discoverNeighbours(long pix, A& pixels)
  {
    fix_arr<int,8> subpix;
    geom.neighbors(pix, subpix);
    pixels[0] = pix;
    memcpy(&pixels[1], &subpix[0], sizeof(int)*8);
  }

  template<typename T, typename T2>
  bool QuickInterpolateMap_ECP<T,T2>::computeMatrix(int ring,
						    arr<vec3>& all_vec,
						    arr<T2>& cho,
						    arr<T2>& M,
						    T2 *out)
  {
    int pix = geom.rp2pix(ring, 0);
    fix_arr<int,9> pixels;
    ArrMatrix_adaptor<double> adapt;

    adapt.a = &cho;
    adapt.N = numNeighbours;

    discoverNeighbours(pix, pixels);

    for (int j = 0; j < numNeighbours; j++)
      all_vec[j] = directions[pixels[j]];

    setupMatrix(all_vec, cho, pixels);

    int err;

    Eigen::Map<Eigen::MatrixXd> Mcho(&cho[0], numNeighbours, numNeighbours);
    Eigen::LDLT<Eigen::MatrixXd> ldlt = Mcho.ldlt();
    Eigen::MatrixXd Inverse = Eigen::MatrixXd::Identity(numNeighbours, numNeighbours);
    Eigen::MatrixXd Dmat;

    Inverse = ldlt.matrixL().solve(Inverse);
    Dmat = ldlt.vectorD();
    std::ofstream f("D_stat.txt", std::ios::app);
    f << ring << " " << Dmat.array().maxCoeff() << " " << Dmat.array().minCoeff() << std::endl;
    for (int i = 0; i < numNeighbours; i++)
      if (Dmat(i,0) < 0)
        Dmat(i,0) = 0;
      else
        Dmat(i,0) = 1/Dmat(i,0);
    Inverse = ldlt.matrixU().solve(Eigen::DiagonalMatrix<double,Eigen::Dynamic,Eigen::Dynamic>(Dmat)*Inverse);

    for (int i = 0; i < numNeighbours; i++)
     for (int j = i; j < numNeighbours; j++)
       M[j*numNeighbours+i] = M[i*numNeighbours+j] = Inverse(i,j);
         

#if 0
    err = do_cholesky_decomposition<T2, arr<T2> >(cho, numNeighbours);
    if (err < 0)
      return true;

    do_cholesky_inversion(cho, numNeighbours);
    do_cholesky_multiplication(cho, M, numNeighbours);
    
    for (int i = 0; i < numNeighbours; i++)
      {
        for (int j = i+1; j < numNeighbours; j++)
          {
	    //	    M[i*numNeighbours+j] /= (((T2)1) + noiseRegulation);
	    M[j*numNeighbours+i] = conj(M[i*numNeighbours+j]);
          }
      }
#endif

    upMatrix<true>(&M[0], out, numNeighbours);
    return false;
  }

  
  template<typename T, typename T2>
  void QuickInterpolateMap_ECP<T,T2>::precomputeGeometry()
    throw (PlanckError)
  {
    arr<int> pixels;
    arr<vec3> all_vec;
    arr<T2> M, cho;

    equatorialRings.alloc(numMatrixElements*Nrings);

    pixels.alloc(numNeighbours);
    all_vec.alloc(numNeighbours);
    M.alloc(numNeighbours*numNeighbours);
    cho.alloc(numNeighbours*numNeighbours);

    std::cout << "Computing pixelization statistical properties..." << std::endl;

    try {
      if (USE_CACHE) {
        int locNumNeighbours, locNumMatElements, locNrings, locBackupRings, locNphi;

        std::ifstream f("cache_geometry_ecp_scalar.dat");
        if (!f)
          throw InterpolateCacheError_ECP();

        f.read((char *)&locNumNeighbours, sizeof(int));
        f.read((char *)&locNumMatElements, sizeof(int));
        f.read((char *)&locNrings, sizeof(int));
	f.read((char *)&locNphi, sizeof(int));
        if (locNrings != Nrings || locNphi != Nphi ||
	    locNumNeighbours != numNeighbours ||
            locNumMatElements != numMatrixElements)
          throw InterpolateCacheError_ECP();
 	f.read((char *)&equatorialRings[0], sizeof(T2)*numMatrixElements*Nrings);
	f.read((char *)&bad_matrix[0], sizeof(bool)*Nrings);
	if (!f)
	  throw InterpolateCacheError_ECP();
        return;
      }
    } catch (const InterpolateCacheError_ECP & e) 
       {
          std::cerr << "Error loading cache. Rebuilding." << std::endl;
       }

    // Two steps for precomputing geometry
    // First we compute the pixelization on each ring in north cap (south cap is
    // just a mirror.
    // We need only one matrix for each ring in the equatorial plane

    bad_matrix[0] = true;
    bad_matrix[Nrings-1] = true;
    for (int ring = 1; ring < Nrings-1; ring++)
      {
	bad_matrix[ring] = computeMatrix(ring, all_vec, cho, M, &equatorialRings[ring*numMatrixElements]);
      }

    if (USE_CACHE) {
      std::ofstream f("cache_geometry_ecp_scalar.dat");
      if (!f)
         return;
      f.write((char *)&numNeighbours, sizeof(int));
      f.write((char *)&numMatrixElements, sizeof(int));
      f.write((char *)&Nrings, sizeof(int));
      f.write((char *)&Nphi, sizeof(int));
      f.write((char *)&equatorialRings[0], sizeof(T2)*numMatrixElements*Nrings);
      f.write((char *)&bad_matrix[0], sizeof(bool)*Nrings);
    }
  }

};
