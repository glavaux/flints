/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/mach.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#ifndef __COSMO_MACHINE_TEST_HPP
#define __COSMO_MACHINE_TEST_HPP

#include <iostream>

template<typename T>
T mach_epsilon()
{
  T eps = (T)1;

  do
    {
      eps /= 2;
    }
  while ((T)(1 + (eps/2)) != (T)1);

  return eps;
}

#endif
