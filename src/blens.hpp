/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/blens.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/


#ifndef __LENSED_DIRECTION
#define __LENSED_DIRECTION

#include <cassert>

namespace CMB
{
  static double sq(double x) { return x*x; }

  static inline void computeMovedDirectionClassic(double d_theta,
						  double d_phi,
						  const pointing& n, pointing& new_n)
  {
    double amplitude = sqrt(d_theta*d_theta + d_phi*d_phi);
    double cos_alpha = d_theta/amplitude;
    double sin_alpha = d_phi/amplitude;


    if (amplitude == 0)
      {
        new_n = n;
        return;
      }
    double cos_d = cos(amplitude);
    double sin_d = sin(amplitude);

    double cos_theta = cos(n.theta);
    double sin_theta = sin(n.theta);

    double cos_phi = cos(n.phi);
    double sin_phi = sin(n.phi);

    double cos_thetap = cos_d * cos_theta - sin_d * sin_theta * cos_alpha;
    double sin_thetap = sqrt(sq(cos_d * sin_theta + cos_alpha * sin_d * cos_theta) + sq(sin_alpha * sin_d));
    assert(sin_thetap >= 0);
    assert(sin_thetap <= 1);
    assert(cos_thetap <= 1);
    assert(cos_thetap >= -1);
    double sin_deltaphi = sin_alpha * sin_d / sin_thetap;
    assert(sin_deltaphi < 1);
    assert(sin_deltaphi > -1);

    double delta_phi = asin(sin_deltaphi);

    new_n.theta = acos(cos_thetap);
    new_n.phi = n.phi + delta_phi;
  }

  static inline void computeMovedDirectionNew(double d_theta,
					      double d_phi,
					      const pointing& n, pointing& new_n)
  {
    double amplitude = sqrt(d_theta*d_theta + d_phi*d_phi);
    double cos_alpha = d_theta/amplitude;
    double sin_alpha = d_phi/amplitude;

    
    if (amplitude == 0)
      {
	new_n = n;
	return;
      }
    double cos_d = cos(amplitude);
    double sin_d = sin(amplitude);
    
    double cos_theta = cos(n.theta);
    double sin_theta = sin(n.theta);

    double cos_phi = cos(n.phi);
    double sin_phi = sin(n.phi);

    double n_xyz;
    double basis[3][3] = {
        { cos_phi * sin_theta, sin_phi * sin_theta, cos_theta },
        { cos_phi * cos_theta, sin_phi * cos_theta, -sin_theta },
        { -sin_phi, cos_phi, 0 }
    };
    double np0[3] = { cos_d, cos_alpha*sin_d, sin_alpha*sin_d };
    double np[3];
    double dnp;

    np[0] = basis[0][0] * np0[0] + basis[1][0] * np0[1] + basis[2][0] * np0[2];
    np[1] = basis[0][1] * np0[0] + basis[1][1] * np0[1] + basis[2][1] * np0[2];
    np[2] = basis[0][2] * np0[0] + basis[1][2] * np0[1] + basis[2][2] * np0[2];
    dnp = sqrt(np[0]*np[0] + np[1]*np[1] + np[2]*np[2]);

    new_n.theta = acos(np[2]/dnp);
    new_n.phi = atan2(np[1], np[0]);
    while (new_n.phi < 0) new_n.phi += 2*M_PI;
  }

  static inline void computeMovedDirectionNew2(double d_theta,
                                           double d_phi,
                                           const pointing& n, pointing& new_n)
  {
     double grad_len = std::sqrt(d_theta*d_theta + d_phi*d_phi);
     if (grad_len == 0)
       {
         new_n = n;
         return;
       }
     double cth0 = std::cos(n.theta), sth0 = std::sin(n.theta);
     int topbottom = n.theta < (0.5*M_PI)  ? 1 : -1;
     double sinc_grad_len = std::sin(grad_len)/grad_len;
     double cth = topbottom*std::cos(grad_len) * cth0 - sinc_grad_len*sth0*d_theta;
     double sth = std::max(1e-10,std::sqrt((1.0-cth)*(1.0+cth)));

     new_n.phi = n.phi + std::asin(d_phi*sinc_grad_len/ sth );
     new_n.theta = std::acos(cth);
  }


  static inline void computeMovedDirection(double d_theta,
					   double d_phi,
					   const pointing& n, pointing& new_n)
  {
    computeMovedDirectionNew(d_theta, d_phi, n, new_n);
  }

};

#endif
