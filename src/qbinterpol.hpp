/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/qbinterpol.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/


#ifndef __BAYES_QINTERPOLATION_HPP
#define __BAYES_QINTERPOLATION_HPP

#include <cmath>
#include <iostream>
#include <fstream>
#include <cstring>
#include <cassert>
#include <vector>
#include <vec3.h>
#include <arr.h>
#include <healpix_map.h>
#include "multi_healpix.hpp"
#include "extra_map_tools.hpp"
#include "cubic.hpp"
#include "cholesky.hpp"
#include "eigenvals.hpp"
#include "packMatrix.hpp"
#include "qinverse.hpp"
#include "numerics.hpp"
#include "compiler_defs.hpp"

namespace CMB
{
  template<typename T,typename T2 = T>
  class QuickInterpolateMap
  {
  private:
    /**
      * This is a compile option. The code becomes a little more
      * verbose if \p VERBOSE is \p true.
      */
    static const bool VERBOSE = true;    
    /**
      * If USE_CACHE is true, then the code generates an on-disk cache of the pixelization
      * statistical property, and/or use this cache for further construction of this object.
      */
    static const bool USE_CACHE = false;
    /**
     * Do extra sanity checking in the code if true.
     */
    static const bool DO_EXTRA_CHECKING = true;
    static const bool DO_DIAGNOSIS = true;
    arr<T2> cls;  
    T2 variance;
    int lmax;
    double delta, maxAngle;
    arr<T2> precomputed_correlations;
    CubicSpline<T2> cubic;
    arr<vec3> directions;
    arr<T2> equatorialRings, northCap, southCap, extraRingsN, extraRingsS; 
    Healpix_Base baseLevel, lastLevel;
    const int Nside;
    const int sizeNorthCap;
    int numNeighbours,numMatrixElements;
    int backupRings, capPixels;
    int sizeExtra;
    int interpolateLevel;
    T2 noiseRegulation;
    arr<bool> regulatedEq, regulatedN, regulatedS, regulatedExtraN, regulatedExtraS;
  public:
    
    static void compute_Pl_array(T2 x, arr<T2>& result);

    double getVariance() const  { return variance; }

    /**
     * This is the constructor.
     * It takes the average spectrum of the fluctuation of the field on the sphere as an argument
     * alongside the number of neighbors (through the nLevel argument), Nside (for the input map to
     * be interpolated) and optionally a regularization term (regulation).     
     */
    QuickInterpolateMap(const arr<T2>& spectrum, 
			int nLevel, int Nside, T2 regulation = 0)
      throw(PlanckError)
      : cls(spectrum), lmax(spectrum.size()-1),
	interpolateLevel(nLevel-1),
	numNeighbours(9*(1<<(2*(nLevel-1)))),
	baseLevel(Nside/nLevel, NEST, SET_NSIDE),
	lastLevel(Nside, RING, SET_NSIDE),
	sizeNorthCap(2*Nside*(Nside-1)),
	Nside(Nside)
    {    
      this->maxAngle = lastLevel.max_pixrad()*20;
      this->numNeighbours = numNeighbours;
      this->noiseRegulation = regulation;
      // WARNING Heuristic tuning
      backupRings = 2+(1<<(2*nLevel-1));
      // END WARNING
      sizeExtra = Nside*backupRings;
      capPixels = (Nside)*(Nside-1)/2;
#ifdef MEMORY_DIET_PACK_MATRIX
      numMatrixElements = ((numNeighbours+1)/2+1)*numNeighbours;
#else
      numMatrixElements = numNeighbours*numNeighbours;
#endif
      precomputeVariance();
      precomputeLegendre(200000);
      precomputeDirections();
      precomputeGeometry();
   }
    
    ~QuickInterpolateMap() throw()
    {
    }

    void precomputeGeometry()
      throw (PlanckError);

    /** This function precomputes the 3d unit vector 
     * for each pixel. This allows us to do a lookup
     * instead of a serie of costly trigonometric operation.
     */
    void precomputeDirections()
    {
      Healpix_Base map(Nside, RING, SET_NSIDE);

      directions.alloc(map.Npix());
      for (int i = 0; i < map.Npix(); i++)
	directions[i] = map.pix2ang(i);
    }

    /**
     * We precompute the variance of a pixel here.
     */
    void precomputeVariance()
      throw (PlanckError)
    {
      variance = 1.0;
      variance = fullComputation(1.0);
    }

    /**
     * This function do the full compute of the 
     * the correlation function for an angular separation 
     * \f$\theta$ and \f$x = \cos(\theta)$.
     */
    T2 fullComputation(T2 x)
      throw (PlanckError);
    
    /**
     * This function precomputes a cubic interpolation
     * of Legendre polynomials. This will speed up
     * the interpolation algorithm while keeping
     * much of the precision (in practice nearly all).
     */
    void  precomputeLegendre(int numPoints)
      throw(PlanckError);
    
    /**
     * This function computes the correlation function
     * using the cubic interpolation.
     */
    T2 computeCorrelation(T2 x)
    {
      return cubic.computeRegular(((T2)1)-x);
    }

    /** 
     * This function computes the correlation matrix
     * between the given directions and the direction of interpolation.
     * The array must be preallocated and specified in \a M.
     */
    void setupMatrix(const arr<vec3>& all_vec, 
		     arr<T2>& M, const arr<int>& valid_pix)
      throw(PlanckError);

    bool getMatrix(T2* restrict_flints C, int pix, arr<int>& pixel_list);

    bool computeMatrix(int pix, arr<int>& pixels, arr<vec3>& all_vec,
		       arr<T2>& cho, arr<T2>& M, T2 *geom);

    void discoverNeighbours(long pix, arr<int>& pixels);

   /** 
     * This function evaluates the interpolated pixel using the 9 neighbours
     * of the given direction \a ptg. The number of pixels actually used
     * is returned in \a npix, the variance of the interpolation is in \a var.
     */     
    T interpolate(const Healpix_Map<T>& map,
		  const pointing& ptg, int *npix, T *var)
      throw (PlanckError);

    /** This function is most useful for the lensing case. It computes the
     * conjugate transpose operator of the interpolation procedure.
     * As the operation is actually accumulative,  one has first to clear up
     * the output map before using this function at all.
     */
    template<bool doNpix>
    void interpolateManyTransposedBase(const T *restrict_flints mapValue,
				       Healpix_Map<T>& outmap,
				       const pointing * restrict_flints ptg,
				       int numDirections,
				       int *npix)
      throw (PlanckError);

    void interpolateManyTransposed(const T * restrict_flints mapValue, Healpix_Map<T>& outmap, const pointing * restrict_flints ptgs, int numDirections) 
      throw (PlanckError) { interpolateManyTransposedBase<false>(mapValue, outmap, ptgs, numDirections, 0); }

    void interpolateTransposed(const T& mapValue, Healpix_Map<T>& outmap, const pointing& ptg)
      throw (PlanckError) { interpolateManyTransposedBase<false>(&mapValue, outmap, &ptg, 1, 0); }
    
    template<bool doNpix, bool doVar>
    void interpolateManyBase(const Healpix_Map<T>& map, const pointing * restrict_flints ptgs, int *npix, T *var, T *result, int numDirections)
       throw (PlanckError);

    void interpolateMany(const Healpix_Map<T>& map, const pointing * restrict_flints ptgs, T *result, int numDirections)
      throw (PlanckError);
    void interpolateMany(const Healpix_Map<T>& map, const pointing * restrict_flints ptgs, T *var, T *result, int numDirections)
      throw (PlanckError);
    void interpolateMany(const Healpix_Map<T>& map, const pointing * restrict_flints ptgs, int *npix, T *result, int numDirections)
      throw (PlanckError);
    void interpolateMany(const Healpix_Map<T>& map, const pointing * restrict_flints ptgs, int *npix, T *var, T *result, int numDirections)
      throw (PlanckError);

    void matrixError(const arr<T2>& Correlation, int n);
  };    
};

#include "qbinterpol.tcc"

#endif
