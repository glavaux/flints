/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/numerics.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

/**
 * This file is part of FLINTS, for interpolating random fields on the sphere.
 * Copyright (C) 2010-2011 Guilhem Lavaux
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __CMB_NUMERICS_HPP
#define __CMB_NUMERICS_HPP

namespace CMB
{
  template<typename T> 
  class TYPE_INFO {
  public:
    static const T one_precision = 10000;
    static const T one_sqrt_precision = 10000;
  };
    

  template<>
  class TYPE_INFO<double> {
  public:
    static const double one_precision = 2.23e-16;
    static const double one_sqrt_precision = 1.49e-8;
  };

  template<>
  class TYPE_INFO<float>  {
  public:
    static const float one_precision = 1.19e-7;
    static const float one_sqrt_precision = 3.45e-4;
  };

  template<>
  class TYPE_INFO<long double> {
  public:
//    static const long double one_precision = 1.93e-34;
//    static const long double one_sqrt_precision = 1.38e-17;
    static const long double one_precision = 2.23e-16;
    static const long double one_sqrt_precision = 2.33e-10;
  };
};


#endif
