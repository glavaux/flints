/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/packMatrix.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/



#ifndef __PACK_MATRIX
#define __PACK_MATRIX

#include "extra_complex.h"

//#define MEMORY_DIET_PACK_MATRIX

namespace CMB
{

#ifndef MEMORY_DIET_PACK_MATRIX
  template<bool pack, typename T>
  void upMatrix(const T* A, T* B, int N)
  {
     memcpy(&B[0], &A[0], sizeof(T)* N*N);
  }

#else
  /**
   * This function packs a symetric matrix into a smaller array.
   * The gain in size is rougly a factor 2. The unpacked symmetric matrix
   * must be represented that way
   *    A_{i,j} = A[i*N + j]
   * i being the i-th line, j being the j-th column.
   * 
   * The packed version occupies an array of size ((N+1)/2+1)*N.
   * If the template argument "pack" is set to true then 
   * A is the input unpacked matrix and B is the output
   * Otherwise, A is the packed matrix and B the unpacked matrix.
   */
  template<bool pack, typename T,typename T2>
  void upMatrix(const T* A, T2* B, int N)
  {
    int Ntilde = (N+1)>>1;
        
    for (int i = 0; i < Ntilde; i++)
      {
	for (int j = i+1; j < N; j++)
	  {
	    B[i*N+j] = A[i*N+j];
	    if (!pack)
	      B[j*N+i] = conj(A[i*N+j]);
	  }
      }
    for (int i = Ntilde; i < N; i++)
      {
	int k = N-i-1;
	for (int j = i+1; j < N; j++)
	  {
	    int l = N-j-1;
	    
	    if (pack)
	      B[k*N+l] = A[i*N+j];
	    else
	      {
		B[i*N+j] = A[k*N+l];
		B[j*N+i] = conj(A[k*N+l]);
	      }
	  }
      }
    
    for (int i = 0; i < N; i++)
      {
	if (pack)
	  B[Ntilde*N+i] = A[i*N+i];
	else
	  B[i*N+i] = A[Ntilde*N+i];
      }
  }

  /**
   * This function packs a symetric matrix into a smaller array.
   * The gain in size is rougly a factor 2. The unpacked symmetric matrix
   * must be represented that way
   *    A_{i,j} = A[i*N + j]
   * i being the i-th line, j being the j-th column.
   * 
   * The packed version occupies an array of size ((N+1)/2+1)*N.
   * If the template argument "pack" is set to true then 
   * A is the input unpacked matrix and B is the output
   * Otherwise, A is the packed matrix and B the unpacked matrix.
   *
   *
   * This is a specific version that supports a permutation of 
   * columns/rows at the time of packing/unpacking. It is most useful
   * at the moment of unpacking where the meaning of rows/columns
   * may have changed.
   */
  template<bool pack, typename T,typename T2>
  void upMatrix(const T* A, T2* B, const int *permutation, int N)
  {
    __builtin_prefetch(A);
   int Ntilde = (N+1)>>1;
        
    for (int i = 0; i < Ntilde; i++)
      {
	int k = permutation[i];
	for (int j = i+1; j < N; j++)
	  {
	    int l = permutation[j];

	    B[k*N+l] = A[i*N+j];
	    if (!pack)
	      B[l*N+k] = A[i*N+j];
	  }
      }
    for (int i = Ntilde; i < N; i++)
      {
	int k = N-i-1;
	int q = permutation[i];
	for (int j = i+1; j < N; j++)
	  {
	    int l = N-j-1;
	    int r = permutation[j];
	    
	    if (pack)
	      B[k*N+l] = A[q*N+r];
	    else
	      {
		B[q*N+r] = A[k*N+l];
		B[r*N+q] = A[k*N+l];
	      }
	  }
      }
      
    for (int i = 0; i < N; i++)
      {
	int k = permutation[i];

	if (pack)
	  B[Ntilde*N+i] = A[k*N+k];
	else
	  B[k*N+k] = A[Ntilde*N+i];
      }
  }
#endif
  
};

#endif
