/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/cholesky_nolapack.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/


#ifndef __CHOLESKY_SOLVE_HPP
#define __CHOLESKY_SOLVE_HPP

#include <cmath>
#include <cassert>
#include <arr.h>
#include "numerics.hpp"

namespace CMB {

  template<typename T>
  T square_cholesky(T a) { return a*a;}

  template<typename T>
  int do_cholesky_decomposition(arr<T>& M, int N, T precision = 0)
  {
    int invalid = 0;
    for (int i = 0; i < N; i++)
      {
	T diag;

	{
	  // The diagonal part
	  T result;
	  T sum0 = 0;
	  for (int k = i-1; k>=0; k--)
	    sum0 += square_cholesky(M[i*N+k]);
          result = M[i*N+i]-sum0;

	  // Instead of having to branch
	  // we do simple arithmetic. Branching
	  // is delayed to the end of the function.
	  invalid += (result < TYPE_INFO<T>::one_sqrt_precision);
//	  invalid += std::signbit(result);

	  diag = M[i*N+i] = std::max(TYPE_INFO<T>::one_sqrt_precision,std::sqrt(std::max((T)0,result)));
	}

	for (int j = i+1; j < N; j++)
	  {
	    T result = M[i*N+j];
	    T sum0 = 0;
	    for (int k = i-1; k>=0; k--)
	      sum0 += M[i*N+k]*M[j*N+k];
	    result -= sum0;

	    M[j*N+i] = result/diag;
	  }
      }
    return invalid ? -1 : 0;
  }

  template<typename T>
  int do_cholesky_decomposition_plus(arr<T>& M, arr<T>& D, int N, T precision = 0)
  {
    int invalid = 0;
    for (int i = 0; i < N; i++)
      {
        T diag;

        {
          // The diagonal part
          T result;
          T sum0 = 0;
          for (int k = i-1; k>=0; k--)
            sum0 += square_cholesky(M[i*N+k])*D[k];
          result = M[i*N+i]-sum0;

          // Instead of having to branch
          // we do simple arithmetic. Branching
          // is delayed to the end of the function.
          invalid += (result < 100*TYPE_INFO<T>::one_precision);
//std::signbit(result);

	  diag = D[i] = result;

          M[i*N+i] = 1.0;
        }

        for (int j = i+1; j < N; j++)
          {
            T result = M[i*N+j];
            T sum0 = 0;
            for (int k = i-1; k>=0; k--)
              sum0 += M[i*N+k]*M[j*N+k]*D[k];
            result -= sum0;

            M[j*N+i] = result/diag;
          }
      }
    return invalid ? -1 : 0;
  }



  /**
   * This function does the inversion of L (as decomposed by do_cholesky)
   */
  template<typename T>
  void do_cholesky_inversion(arr<T>& cho, int N)
  {
    for (int i = 0; i < N; i++)
      {
	cho[N*i+i] = ((T)1)/cho[i*N+i];
	for (int j = (i+1); j < N; j++)
	  {
	    T sum = 0;
	    for (int k = i; k < j; k++)
	      sum -= cho[j*N+k]*cho[k*N+i];
	    cho[j*N+i] = sum/cho[j*N+j];
	  }
      }    
  }

  /**
   * This function does the multiplication L^{t} * L
   */
  template<typename T>
  void do_cholesky_multiplication(const arr<T>& cho, arr<T>& M, int N)
  {
    for (int i = 0; i < N; i++)
      {
	for (int j = i; j < N; j++)
	  {
	    T sum = 0;
	    for (int k = j; k < N; k++)
	      {
		sum += cho[k*N+i]*cho[k*N+j];
	      }
	    M[i*N+j] = M[j*N+i] = sum;
	  }
      }
  }

  template<typename T>
  void do_cholesky_rev_multiplication(const arr<T>& cho, arr<T>& M, int N)
  {
    for (int i = 0; i < N; i++)
      {
	for (int j = i; j < N; j++)
	  {
	    T sum = 0;
	    for (int k = 0; k <= i; k++)
	      {
		sum += cho[i*N+k]*cho[j*N+k];
	      }
	    M[i*N+j] = M[j*N+i] = sum;
	  }
      }
  }

  template<typename T>
  void cholesky_solve_0(const arr<T>& cho, arr<T>& col0)
  {
    int N = col0.size();

    assert(cho.size() == N*N);

    col0[0] = 1./cho[0];

    // Solve for L y = (1 0 0...)
    for (int i = 1; i < N; i++)
      {
	T result = 0;
	const T *ChoLine = &cho[i*N];
	T *c0 = &col0[0];

	for (int k = i-1; k >=0; k--)
	  {
	    result -= (*ChoLine)*(*c0);
	    ChoLine++;
	    c0++;
	  }
	*c0 = result/(*ChoLine);
      }

    // Solve for L^t x = y
    for (int i = N-1; i >= 0; i--)
      {
	__builtin_prefetch(&cho[(i-1)*N+i]);
	T result = col0[i];
	T diag = cho[i*N+i];
	const T *ChoLine = &cho[(i+1)*N + i];
	T *c0 = &col0[i+1];

	for (int k = i+1; k<N; k++)
	  {
	    result -= (*c0)*(*ChoLine);
	    c0++;
	    ChoLine+=N;
	  }

	col0[i] = result/diag;
      }
  }

};

#endif
