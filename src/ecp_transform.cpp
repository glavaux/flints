/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/ecp_transform.cpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/

#include <cstdlib>
#include <cmath>
#include <alm.h>
#include <xcomplex.h>
#include "ecp_map.hpp"
#include "psht_cxx.h"
#include "ecp_transform.hpp"

template<typename T>
void ecp_alm2map(const Alm<xcomplex<T> >& alms, ECP_Map<T>& m)
{
  psht_joblist<T> p;
  
  p.set_ECP_geometry(m.Nrings(), m.Nphi());
  p.set_triangular_alm_info(alms.Lmax(), alms.Mmax());
  p.add_alm2map(&alms.Alms()[0], &m[0], false);
  p.execute(); 
}

#define FORCE(T) template void ecp_alm2map(const Alm<xcomplex<T> >& alms, ECP_Map<T>& m)

FORCE(double);
FORCE(float);

#undef FORCE


template<typename T>
void ecp_alm2map_der1(const Alm<xcomplex<T> >& alms, ECP_Map<T>& m_dtheta, ECP_Map<T>& m_dphi)
{
  psht_joblist<T> p;
  
  p.set_ECP_geometry(m_dtheta.Nrings(), m_dtheta.Nphi());
  p.set_triangular_alm_info(alms.Lmax(), alms.Mmax());
  p.add_alm2map_der1(&alms.Alms()[0], &m_dtheta[0], &m_dphi[0], false);
  p.execute(); 
}

#define FORCE(T) template void ecp_alm2map_der1(const Alm<xcomplex<T> >& alms, ECP_Map<T>& m_dtheta, ECP_Map<T>& m_dphi)

FORCE(double);
FORCE(float);

#undef FORCE


template<typename T>
void ecp_alm2map_pol(const Alm<xcomplex<T> >& almsG, const Alm<xcomplex<T> >& almsC,  ECP_Map<T>& mQ, ECP_Map<T>& mU)
{
  psht_joblist<T> p;
  
  p.set_ECP_geometry(mQ.Nrings(), mQ.Nphi());
  p.set_triangular_alm_info(almsG.Lmax(), almsG.Mmax());
  p.add_alm2map_spin(&almsG.Alms()[0], &almsC.Alms()[0], &mQ[0], &mU[0], 2, false);
  p.execute(); 
}

#define FORCE(T) template void ecp_alm2map_pol(const Alm<xcomplex<T> >& almsG, const Alm<xcomplex<T> >& almsC, ECP_Map<T>& mQ, ECP_Map<T>& mU)

FORCE(double);
FORCE(float);

#undef FORCE


template<typename T>
void ecp_alm2map_pol(const Alm<xcomplex<T> >& almsT, const Alm<xcomplex<T> >& almsG, const Alm<xcomplex<T> >& almsC, ECP_Map<T>& mT, ECP_Map<T>& mQ, ECP_Map<T>& mU)
{
  psht_joblist<T> p;
  
  p.set_ECP_geometry(mQ.Nrings(), mQ.Nphi());
  p.set_triangular_alm_info(almsG.Lmax(), almsG.Mmax());
  p.add_alm2map_pol(&almsT.Alms()[0], &almsG.Alms()[0], &almsC.Alms()[0], &mT[0], &mQ[0], &mU[0], false);
  p.execute(); 
}

#define FORCE(T) template void ecp_alm2map_pol(const Alm<xcomplex<T> >& almsT, const Alm<xcomplex<T> >& almsG, const Alm<xcomplex<T> >& almsC, ECP_Map<T>& mT, ECP_Map<T>& mQ, ECP_Map<T>& mU)

FORCE(double);
FORCE(float);

#undef FORCE
