/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/eigenvals.hpp
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/


#ifndef __EIGENVALS_HPP
#define __EIGENVALS_HPP

#include <gsl/gsl_eigen.h>

namespace CMB
{

#define SMALL_NUMBER 1e-6

  static inline
  void matrixEigenValues(arr<double>& Mat, int N, arr<double>& vecs, arr<double>& vals)
  {
    gsl_matrix_view MatView = gsl_matrix_view_array(&Mat[0], N, N);
    gsl_matrix_view VecView = gsl_matrix_view_array(&vecs[0], N, N);
    gsl_vector_view ValsView = gsl_vector_view_array(&vals[0], N);

    gsl_eigen_symmv_workspace * work = gsl_eigen_symmv_alloc(N);

    for (int i = 0; i < N; i++)
      {
	for (int j = i+1; j < N; j++)
	  {
	    Mat[j*N+i] = Mat[i*N+j];
	  }
      }

    gsl_eigen_symmv(&MatView.matrix, &ValsView.vector, &VecView.matrix, work);
    
    gsl_eigen_symmv_sort(&ValsView.vector, &VecView.matrix, GSL_EIGEN_SORT_VAL_DESC);

    gsl_eigen_symmv_free(work);
  }

  static inline
  void matrixEigenValues(arr<xcomplex<double> >& Mat, int N, arr<xcomplex<double> >& vecs, arr<double>& vals)
  {
    gsl_matrix_complex *MatC = gsl_matrix_complex_alloc(N, N);
    gsl_matrix_complex *VecC = gsl_matrix_complex_alloc(N, N);
    gsl_vector_view ValsView = gsl_vector_view_array(&vals[0], N);

    gsl_eigen_hermv_workspace *work = gsl_eigen_hermv_alloc(N);

    for (int i = 0; i < N; i++)
     {
	for (int j = i+1; j < N; j++)
	  {
             gsl_complex c = { Mat[i*N+j].re, Mat[i*N+j].im };
             gsl_complex c2 = { Mat[i*N+j].re, -Mat[i*N+j].im };

             gsl_matrix_complex_set(MatC, i, j, c);
             gsl_matrix_complex_set(MatC, j, i, c2);
          }
        gsl_complex c = {Mat[i*N+i].re, 0};
        gsl_matrix_complex_set(MatC, i, i, c);
     }
     gsl_eigen_hermv(MatC, &ValsView.vector, VecC, work);
     gsl_eigen_hermv_sort(&ValsView.vector, VecC, GSL_EIGEN_SORT_VAL_DESC);
     for (int i = 0; i < N; i++)
      {
	for (int j = 0; j < N; j++)
	  {
             gsl_complex c = gsl_matrix_complex_get(VecC, i, j);
             
             vecs[i*N+j].re = GSL_REAL(c);
             vecs[i*N+j].im = GSL_IMAG(c);
          }
      }
      gsl_matrix_complex_free(VecC);
      gsl_matrix_complex_free(MatC);
      gsl_eigen_hermv_free(work);
  }

  static inline
 void matrixEigenValues(arr<float>& Mat, int N, arr<float>& vecs, arr<float>& vals)
  {
    gsl_matrix_float_view MatView = gsl_matrix_float_view_array(&Mat[0], N, N);
    gsl_matrix_float_view VecView = gsl_matrix_float_view_array(&vecs[0], N, N);
    gsl_vector_float_view ValsView = gsl_vector_float_view_array(&vals[0], N);

	abort();
  }

  template<typename T>
  void do_eigen_matrix(const arr<T>& vecs, const arr<T>& vals, arr<T>& M, int N)
  {
    for (int i = 0; i < N; i++)
      for (int j = 0; j < N; j++)
        {
          double a = 0;
          for (int k = 0; k < N; k++)
            a += vecs[N*i+k]*vals[k]*vecs[N*j+k];
          M[N*i+j] = a;
        }
  }

  template<typename T> T regularLambda(T a, T ref)
  {
    if (fabs(a/ref) < SMALL_NUMBER)
      return 0;
    return 1/a;
  }

  template<typename T>
  void matrixEigenFirstRow(arr<T>& vecs, arr<T>& vals, arr<T>& row )
  {
    int N = row.size();

    arr<T> tmp_row(N);
    for (int i = 0; i < N; i++)
      tmp_row[i] = vecs[0*N + i] * regularLambda(vals[i], vals[0]);
    
    for (int i = 0; i < N; i++)
      {
	row[i] = 0;
	for (int j = 0; j < N; j++)
	  {
	    row[i] += vecs[i*N + j] * tmp_row[j];
	  }
      }
  }

};

#endif
