/*+
    FLINTS -- Fast and Light Interpolation oN The Sphere -- ./src/extra_complex.h
    Copyright (C) 2009-2013 Guilhem Lavaux

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/


#ifndef __CMB_EXTRA_COMPLEX_H
#define __CMB_EXTRA_COMPLEX_H

#include <iostream>
#include <xcomplex.h>


  static inline
  double conj(double x) { return x; }

  template<typename T>
  std::ostream& printComplex(std::ostream& s, const xcomplex<T>& c)
  {
    s << c.real();
    if (c.imag() <0)
      s << "-i*" << -c.imag();
    else
      s << "+i*" << c.imag();

    return s;
  }


#endif
